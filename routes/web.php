<?php

/* echo "<pre>";
for($i=40;$i<=400;$i++){
		echo "(".(18800+$i).",'DELUXE','80',".$i."),\n";
}
for($i=40;$i<=400;$i++){
		//echo "(".(18400+$i).",'GOFRE','80',".$i."),\n";
}
die; */

Route::get("test",function(){
	return view("print.test");
});
Route::get("login","Auth\LoginController@showLoginForm")->name("login");
Route::post("login","Auth\LoginController@login");
Route::post("logout","Auth\LoginController@logout")->name("logout");

Route::group(['guard' => 'web'], function () {
	Route::get('/', 'TumController@index')->name("home");
	Route::get('/onay', 'OnayController@index');
	Route::get('/hazirlik', 'HazirlikController@index');
	Route::get('/uretimde', 'UretimdeController@index'); //1
	Route::get('/transfer', 'TransferdeController@index');
	Route::get('/transfer/panel', 'TransferdeController@panel');
	Route::get('/uretilen', 'UretilenController@index');
	Route::get('/cikislar', 'CikislarController@index');
	Route::get('/cikislar/panel', 'CikislarController@panel');
	Route::get('/dagitimda', 'DagitimdaController@index');
	Route::get('/tum', 'TumController@index');
	Route::post('/tum/excel', 'TumController@excel');
	Route::get('/fatura', 'FaturaController@index');
	Route::get('/hatali', 'HataliController@index');
	Route::get('/kurye', 'KuryeController@index');
	Route::get('/bayi_giris', 'BayiGirisController@index');
	Route::get('/cari', 'CariController@index');
//	Route::get("/logo/{tarih}","LogoController@show");
	Route::get("order/create","OrderController@create");
	Route::get("urun","UrunController@index");
	Route::get("urun/list","UrunController@list");
	Route::get("toplar","TopController@index");
	Route::get("analiz","AnalizController@index");
});

Route::prefix('jobs')->group(function () {
	//Route::queueMonitor();
});
Route::group(['guard' => 'kurye'], function () {
	Route::get('/gonderi', 'GonderiController@index')->name("gonderi");
});
Route::group(['guard' => 'bayi'], function () {
	Route::get('bayi', 'BayiController@index')->name("bayi");
	Route::get('bayi/create', 'BayiController@create');
	Route::get("bayi/urun","BayiUrunController@index");
	Route::post('bayi/baski', 'UploadController@baski');
	Route::post("/bayi/baski/delete","UploadController@delete");
});

Route::group(["prefix"=>"api/v1"],function(){
	Route::put("gonderi/teslimet","GonderiController@teslimet");

	Route::get("irsaliye/{id}","IrsaliyeController@show_json");
	Route::put("irsaliye/save","IrsaliyeController@store_json");
	Route::put("irsaliye/op_save","IrsaliyeController@opstore_json");
	Route::put("irsaliye/op_new","IrsaliyeController@opnew_json");
	Route::put("irsaliye/op_sil","IrsaliyeController@opsil_json");
	Route::put("irsaliye/coupon_update","IrsaliyeController@coupon_update");

	Route::post("onay","OnayController@store");
	Route::put("onayla","OnayController@onaylandi");
	Route::put("sil","OnayController@sil_json");

	Route::post("hazirlik","HazirlikController@store");
	Route::put("hazirlandi","HazirlikController@hazirlandi");

	Route::post("uretimde","UretimdeController@store");
	Route::post("uretilen","UretilenController@store");
	Route::post("transferde","TransferdeController@store");
	Route::post("dagitimda","DagitimdaController@store");
	Route::post("cikista","CikislarController@store");
	Route::post("tum","TumController@store");
	Route::post("hatali","HataliController@store");
	Route::post("fatura","FaturaController@store");
	Route::post("search","TumController@search");

	Route::put("transfer","TransferdeController@transfer");
	Route::put("uretildi","UretimdeController@uretildi");
	Route::put("transfere","TransferdeController@transfere");
	Route::put("top_transfere","TransferdeController@top_transfere");
	Route::put("cikisa","DagitimdaController@cikisa");
	Route::put("dagitima","DagitimdaController@dagitima");
	Route::put("teslimet","DagitimdaController@teslimet");
	Route::put("iptalet","IrsaliyeController@iptalet");
	Route::put("cikmadi","IrsaliyeController@cikmadi");
	Route::put("hatali","IrsaliyeController@hatali");
	Route::put("uretimegeri","IrsaliyeController@uretimegeri");
	Route::put("basilanageri","IrsaliyeController@basilanageri");
	Route::put("top_degistir","IrsaliyeController@top_degistir");

	Route::post("takip","TakipController@store");
	Route::get("print/irsaliye/{id}","IrsaliyeController@printle");
    Route::post("print/mesaj","IrsaliyeController@mesaj_yaz");
    Route::post("print/barkod","IrsaliyeController@barkod_yaz");
    Route::post("print/etiket","IrsaliyeController@etiket_yaz");
    Route::get("print/barkod","IrsaliyeController@barkod_yaz");
    Route::post("print/cikis","IrsaliyeController@cikis_yaz");
    Route::get("print/fatura/{id}","FaturaController@print");
    Route::get("rawprint/fatura/{id}","FaturaController@rawprint");
    Route::get("xml/fatura/{id}","EfaturaController@store");

    Route::post("kurye","KuryeController@get");
    Route::put("kurye","KuryeController@store");
	Route::patch("kurye/{id}","KuryeController@update");

	Route::post("cari","CariController@get");
    Route::put("cari","CariController@store");
	Route::patch("cari/{id}","CariController@update");

	Route::post("urun","UrunController@get");
    Route::put("urun","UrunController@store");
    Route::post("urun/upload/{id}","UrunController@upload");
	Route::patch("urun/{id}","UrunController@update");
	Route::delete("urun/{id}","UrunController@delete");

	Route::post("top_urunleri","TransferdeController@urunler");
	Route::post("top_transfer","CikislarController@urunler");

	Route::post("top","TopController@get");
    Route::put("top","TopController@store");
    Route::post("top/upload/{id}","TopController@upload");
	Route::patch("top/{id}","TopController@update");
	Route::delete("top/{id}","TopController@delete");

	Route::post("bayi","BayiGirisController@get");
    Route::put("bayi","BayiGirisController@store");
	Route::patch("bayi/{id}","BayiGirisController@update");

    Route::post("gonderi","GonderiController@store");
    Route::get("irsaliye_item/{id}","GonderiController@irsaliye_item");

    Route::get("order/{id}","OrderController@show");
	Route::put("order","OrderController@store");
	Route::put("order/save","OrderController@update");
	Route::post("order/upload","OrderController@upload");

	Route::put("order/op_save","OrderController@opstore_json");
	Route::put("order/op_new","OrderController@opnew_json");
	Route::put("order/op_sil","OrderController@opsil_json");
	Route::put("order/coupon_update","OrderController@coupon_update");

	Route::get("products","ProductController@get");
	Route::get("subcustomer/{id}","CustomerController@subcustomer");

	Route::post("oku/barkod","IrsaliyeController@barkod_oku");
	Route::post("analiz","AnalizController@store");
});

Route::group(["prefix"=>"api/v1/bayi"],function(){
	Route::get("order/{id}","BayiController@show");
	Route::put("order","BayiController@store");
	Route::put("order/save","BayiController@update");
	Route::post("order/upload","BayiController@upload");
	Route::post("tum","BayiController@tum");
	Route::post("search","BayiController@search");
	Route::get("irsaliye/{id}","BayiController@show_json");
	Route::put("irsaliyeproduct/save","BayiController@op_save");
	Route::get("irsaliyeproduct/{id}","BayiController@op_json");
	Route::get("print/mesaj/{id}","BayiController@mesaj_yaz");
	Route::post("takip","TakipController@bayi");

	Route::post("urun","BayiUrunController@get");
    Route::put("urun","BayiUrunController@store");
	Route::post("urun/upload/{id}","BayiUrunController@upload");

	Route::patch("urun/{id}","BayiUrunController@update");
	Route::delete("urun/{id}","BayiUrunController@delete");
	Route::put("iptalet","BayiUrunController@iptalet");
	Route::post("print/cikis","BayiController@cikis_yaz");
});

Route::group(["prefix"=>"rapor"],function(){
//	Route::get("/","RaporController@index");
//    Route::post("/","RaporController@store");
//    Route::get("uruntoplam/{tarih1?}/{tarih2?}","RaporController@uruntoplam");
//    Route::get("uruntoplamFiyat/{tarih1?}/{tarih2?}","RaporController@uruntoplamFiyat");
//    Route::get("urunbazinda/{tarih1?}/{tarih2?}","RaporController@urunbazinda");
//    Route::get("urunkategori/{yil?}","RaporController@urunkategori");
//    Route::get("semturun/{tarih1?}/{tarih2?}","RaporController@semturun");
//    Route::get("kategori/{tarih1?}/{tarih2?}","RaporController@kategori");
//    Route::get("urunsatis/{tarih1?}/{tarih2?}/{sube_id?}","RaporController@urunsatis");
//    Route::get("sube_mutabakat/{tarih1?}/{tarih2?}/{sube_id?}","RaporController@sube_mutabakat");
//    Route::get("sube_urunler/{tarih1?}/{tarih2?}/{sube_id?}","RaporController@sube_urunler");
//    Route::get("orders/{sube_id?}/{ay?}","RaporController@orders");
//    Route::get("kuponlar/{tarih1}/{tarih2}","RaporController@kuponlar");
//    Route::get("excel/{tarih1}/{tarih2}/{sube_id?}","RaporController@excel");
});
