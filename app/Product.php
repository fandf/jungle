<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	//use UploadableRelationship;

	protected $guarded = array("id");

	protected $validationRules = array(
		'name' => 'required',
		'slug' => 'required',
	);

	public function categories()
	{
		return $this->belongsToMany('App\Category')->withPivot('sort');
	}

	public function maincategory()
	{
		return $this->belongsTo('App\Category','category_id');
	}

	public function productimages()
	{
		return $this->hasMany('App\Productimage');
	}

	public function options()
	{
		return $this->hasMany('App\Option');
	}

	public function subes()
	{
		return $this->belongsToMany("App\Sube");
	}

}
