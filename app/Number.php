<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
	protected $guarded = ["id"];

    protected $fillable = [
    
    	"sube_id",
    	"type",
    	"no"
    ];
}