<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adres extends Model
{
	protected $table = 'adres';

	public function customer()
	{
		return $this->belongsTo("App\Customer");
	}
}