<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kayit extends Model
{
    protected $table = 'kayit';

    protected $fillable = [
    	"order_id",
    	"user_id",
    	"table",
    	"ilk",
    	"degisen"
    ];
}