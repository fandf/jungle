<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sube extends Model {

	protected $table = 'subeler';
	protected $guarded = ["id"];
	public static $rules = [
		'subeadi' => 'required',
	];

	public function products()
	{
		return $this->belongsToMany("\App\Stock");
	}

}
