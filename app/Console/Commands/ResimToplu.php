<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResimToplu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resim:toplu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Toplu Resim';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//         INSERT INTO `productimages` ( `product_id`, `customer_id`, `name`, `filename`, `alt`, `status`, `created_at`, `updated_at`)
// VALUES
         ini_set('memory_limit', '2048MB');
         foreach(\File::allFiles("/Users/nevfelermumcu/Documents/jungle-program/YENİ TAKIM") as $key=>$file)
         {
            $filename = str_replace("-","",$file->getFilename());
            $name = str_replace('.jpg','',$filename);
            $filename = str_slug($name)."-".rand(1000,9999).".jpg";

            \Image::make($file)->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(storage_path("app/public/images/products/org/".$filename));
            \Image::make($file)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path("app/public/images/products/".$filename));

            /* \DB::table("productimages")->insert([
                "product_id" => 1,
                "customer_id" => 1,
                "name" => 'BEBİŞİM-'.str_replace('.jpg','',$filename),
                "filename" => "/images/products/".$filename,
                "alt" => "",
                "status" => 1,
                "created_at" => date("Y.m.d H:i:s"),
                "updated_at" => date("Y.m.d H:i:s"),
            ]); */
            echo "( 1, 376, '".$name."', '/images/products/".$filename."', '', 1, NOW(),NOW()),
";
            //echo ($key+1)." ".$filename."\n";
        }
    }
}
