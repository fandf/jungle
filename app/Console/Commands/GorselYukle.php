<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class GorselYukle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gorsel:yukle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Baskı görseli yükleme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $disk = Storage::disk('dropbox');
        $path = "/Users/nevfelermumcu/Documents/BoyutHalı/mockups/curtain.psd";
        $result = $disk->put("curtain.psd", fopen($path, 'r+'));
        dd($result);
    }
}
