<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResimPdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resim:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pdf resimleri kesme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '2048MB');
        $file = "/Users/nevfelermumcu/Downloads/EFŞAN/078.jpg";
        $names = [
            'Adel 01',
            'Adel 02',
            'Adel 03',
            'Adel 04',
            'Adel 05',
            'Adel 06',
            'Adel 07',
            'Adel 08',
            'Adel 09',
        ];
        \Image::make($file)
            ->crop(500,778, 218, 452)
            ->save(storage_path("app/public/images/products/org/".$names[0].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[0].".jpg"));


        \Image::make($file)
            ->crop(500,778, 768, 452)
            ->save(storage_path("app/public/images/products/org/".$names[1].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[1].".jpg"));


        \Image::make($file)
            ->crop(500,778, 1315, 452)
            ->save(storage_path("app/public/images/products/org/".$names[2].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[2].".jpg"));


        \Image::make($file)
            ->crop(500,778, 218, 1257)
            ->save(storage_path("app/public/images/products/org/".$names[3].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[3].".jpg"));


        \Image::make($file)
            ->crop(500,778, 768, 1257)
            ->save(storage_path("app/public/images/products/org/".$names[4].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[4].".jpg"));


        \Image::make($file)
            ->crop(500,778, 1315, 1257)
            ->save(storage_path("app/public/images/products/org/".$names[5].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[5].".jpg"));


        \Image::make($file)
            ->crop(500,778, 218, 2055)
            ->save(storage_path("app/public/images/products/org/".$names[6].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[6].".jpg"));


        \Image::make($file)
            ->crop(500,778, 768, 2055)
            ->save(storage_path("app/public/images/products/org/".$names[7].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[7].".jpg"));


        \Image::make($file)
            ->crop(500,778, 1315, 2055)
            ->save(storage_path("app/public/images/products/org/".$names[8].".jpg"))
            ->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path("app/public/images/products/".$names[8].".jpg"));


//         INSERT INTO `productimages` ( `product_id`, `customer_id`, `name`, `filename`, `alt`, `status`, `created_at`, `updated_at`)
// VALUES
        /* for($x=$y;$x<($y+9);$x++){

            echo "( 1, 1, 'Adel ".$x."', '/images/products/".$x."', '', 1, NOW(),NOW()),
";
        } */
            //echo ($key+1)." ".$filename."\n";

    }
}
