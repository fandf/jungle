<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Customer;
use DB;

class EfsanCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ekle:customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Efsan Customer Sync';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $firmakodlar = Customer::pluck("firmakodu")->all();
        foreach(
        DB::connection("efsan")
            ->table("F0102TBLCARI")
            ->selectRaw("ADRESFATURA,FIRMAKODU,CAST(FIRMAADI AS TEXT) AS FIRMAADI,YETKILI,VERGIDAIRESI,VERGINO,TELEFON1,TELEFON3,EMAIL,UNVAN,FIRMATIPI,FAKS,URL")
            ->whereIn("FIRMATIPI",[1,3])
            ->whereNotIn("FIRMAKODU",$firmakodlar)
            ->get() as $cari
        ){
            $c = new Customer;
            $c->name = $cari->YETKILI ?: ($cari->FIRMAADI?:$cari->FIRMAKODU);
            $c->company_name = $cari->FIRMAADI;
            $c->unvan = $cari->UNVAN;
            $c->group = $cari->FIRMATIPI;
            $c->fax = $cari->FAKS;
            $c->work_phone = $cari->TELEFON1?:"";
            $c->mobile_phone = $cari->TELEFON3?:"";
            $c->email = $cari->EMAIL?:"";
            $c->web_address = $cari->URL;
            $c->address = $cari->ADRESFATURA;
            $c->city = "";
            $c->semt = "";
            $c->vergi_dairesi = $cari->VERGIDAIRESI;
            $c->vergi_no = $cari->VERGINO;
            $c->firmakodu = $cari->FIRMAKODU;
            $c->save();
            echo $c->name."\n";
        }
    }
}
