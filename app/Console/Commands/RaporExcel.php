<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPExcel;
use PHPExcel_Worksheet;
use PHPExcel_IOFactory;
use PHPExcel_Style;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet_PageSetup;
use PHPExcel_Cell_DataType;
use App\Productimage;

class RaporExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapor:excel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Excel stok raporu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Productimage::all();
        $this->excel($products);
        echo "ok\n";
    }

    private function excel($products)
    {

        $objPHPExcel = new PHPExcel();
		// Set document properties
        $objPHPExcel->getProperties()
            ->setCreator("Nevfel Ermumcu")
            ->setLastModifiedBy("Nevfel Ermumcu")
            ->setTitle("Jungle ".date("d.m.Y")." tarihli satışlar")
            ->setSubject("Jungle Günlük Satışlar")
            ->setDescription("Jungle Günlük Satış Tutarları.")
            ->setCategory("Satış");


        $borderStyle = new PHPExcel_Style();

		$borderStyle->applyFromArray(
			[
				'borders' => [
				  	'bottom'	=> ['style' => PHPExcel_Style_Border::BORDER_THIN],
				  	'right'		=> ['style' => PHPExcel_Style_Border::BORDER_THIN],
				  	'top'	=> ['style' => PHPExcel_Style_Border::BORDER_THIN],
				  	'left'		=> ['style' => PHPExcel_Style_Border::BORDER_THIN]
				]
			]
        );


        $objPHPExcel->setActiveSheetIndex(0);
        $i = 1;
        $ilk = 0;
        foreach($products as $product){
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $product->name ,  PHPExcel_Cell_DataType::TYPE_STRING);

            $i++;
        }


        $objPHPExcel->getActiveSheet()->getStyle("B3:D".$i)->getNumberFormat()->setFormatCode('#,##0.00');
        $objPHPExcel->getActiveSheet()->setSharedStyle($borderStyle, "A1:A".$i);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $filename = storage_path().'/app/public/ÜRÜNLER.xlsx';
        return $objWriter->save($filename);
    }
}
