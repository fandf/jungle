<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = "web")
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }
        if (Auth::guard("kurye")->check()) {
            return redirect('/gonderi');
        }
        if (Auth::guard("bayi")->check()) {
            return redirect('/bayi');
        }

        return $next($request);
    }
}
