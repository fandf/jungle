<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Top;

class DagitimdaController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware('auth');
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $top_names = Top::where(function($query){
            $query->where("created_at",">",date("Y-m-d",strtotime("-3 months")));
            $query->orWhere("aktif",1);
        })
        ->pluck("name","id")->all();

        return view('dagitimda',compact("top_names"));
    }

    public function store()
    {
        $user = Auth::user();
        return $this->repo->getDagitimda($user);
    }

    public function cikisa(Request $request)
    {
        $user = Auth::user();
        foreach($request->input("checkedIrs") as $order_id)
        {
            $this->repo->setCikista($user,$order_id,0);
        }
        return 1;
    }

    public function dagitima(Request $request)
    {
        $user = Auth::user();
        foreach($request->input("checkedIrs") as $order_id)
        {
            $this->repo->setDagitimda($user,$order_id,0);
        }
        return 1;
    }

    public function teslimet(Request $request)
    {
        $user = Auth::user();
        foreach($request->input("checkedIrs") as $order_id)
        {
            $this->repo->setTeslimEdildi($user,$order_id,$request->input("kurye_id"));
        }
        return 1;
    }

}
