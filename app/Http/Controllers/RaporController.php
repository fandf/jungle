<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Orderproduct;
use App\Sube;
use App\Product;
use App\User;
use Excel;
use DB;

class RaporController extends Controller
{
    public function __construct()
    {
    	$this->middleware("auth");
    	$this->middleware("can:superuser");
    }
    public function index()
    {
        $subes = [""=>"Hepsi.."] + Sube::pluck("subeadi","id")->all();

        $raporlar = [

            "urunsatis"=>"urunsatis",
            "uruntoplam"=>"uruntoplam",
            "uruntoplamFiyat"=>"uruntoplamFiyat",
            "urunbazinda"=>"urunbazinda",
            "semturun"=>"semturun",
            "kategori"=>"kategori",
            "sube_mutabakat"=>"sube_mutabakat",
            "sube_urunler"=>"sube_urunler",
            "excel" => "excel"
        ];
        return view("rapor.index",compact("subes","raporlar"));
    }

    public function store()
    {
        $link = "";

        if(request()->has("komisyon"))
        {
            $link = '?komisyon='.request()->input("komisyon");
        }

        return redirect("/rapor/".request()->input("rapor")."/".request()->input('tarih1')."/".request()->input('tarih2')."/".request()->input("uretim_yeri").$link);

    }
	/**

		Nefis Demet şube ürün satış

	*/
    public function urunsatis($tarih1=0,$tarih2=0,$sube_id=0)
    {
        $tarih1 = $tarih1 ? $tarih1 : date("Y-m-d");
        $tarih2 = $tarih2 ? $tarih2 : date("Y-m-d");

        $tarih1 = date("Y-m-d",strtotime($tarih1));
        $tarih2 = date("Y-m-d",strtotime($tarih2));


        $orders = Irsaliye::with("orderproducts")

        	->where(function($query)use($sube_id){
        		if($sube_id)
        		{
        			$query->where("uretim_yeri",$sube_id);
        		}
        	})

        	->whereHas("orderproducts",function($query){
        		$query->where("status",4);
        	})

            ->whereBetween('siptar',array($tarih1,$tarih2))

            ->get();

        return view("rapor.urunsatis",compact("orders"));

    }

    public function uruntoplam($tarih1=0,$tarih2=0,$sube_id=0)
    {
        $tarih1 = $tarih1 ? $tarih1 : date("Y-m-d");
        $tarih2 = $tarih2 ? $tarih2 : date("Y-m-d");

        $tarih1 = date("Y-m-d",strtotime($tarih1));
        $tarih2 = date("Y-m-d",strtotime($tarih2));


        $order_ids = Irsaliye::whereBetween('siptar',array($tarih1,$tarih2))
            ->whereHas("orderproducts",function($query){
                $query->whereNotIn("status",[0,5,6]);
             })
            ->pluck("id")
            ->all();

        $urunler = Irsaliyeproduct::whereIn("order_id",$order_ids)
            ->where("ek",0)
            ->get();


        $tablo = [];
        $names = [];

        foreach ($urunler as $key => $op)
        {
            $names[$op->product_sku] = $op->product_name;
            if(!isset($tablo[$op->product_sku]))
            {
                $tablo[$op->product_sku] = $op->product_qty;
            }
            else
            {
                $tablo[$op->product_sku] += $op->product_qty;
            }
        }

        arsort($tablo);
        return view("rapor.uruntoplam",compact("tablo","names","tarih1","tarih2"));
    }

    public function uruntoplamFiyat($tarih1=0,$tarih2=0,$sube_id=0)
    {
        $tarih1 = $tarih1 ? $tarih1 : date("Y-m-d");
        $tarih2 = $tarih2 ? $tarih2 : date("Y-m-d");

        $tarih1 = date("Y-m-d",strtotime($tarih1));
        $tarih2 = date("Y-m-d",strtotime($tarih2));


        $order_ids = Irsaliye::whereBetween('siptar',array($tarih1,$tarih2))
        	->whereHas("orderproducts",function($query){
        		$query->whereNotIn("status",[0,5,6]);
       		 })
            ->pluck("id")
            ->all();

        $urunler = Irsaliyeproduct::whereIn("order_id",$order_ids)
            ->where("ek",0)
            ->get();
        $prices = Product::pluck("price","sku")->all();


        $tablo = [];
        $names = [];

        foreach ($urunler as $key => $op)
        {
            $fiyat = round($op->product_price);
            $names[$op->product_sku] = $op->product_name;
            if(!isset($tablo[$op->product_sku][$fiyat]))
            {
                $tablo[$op->product_sku][$fiyat] = $op->product_qty;
            }
            else
            {
                $tablo[$op->product_sku][$fiyat] += $op->product_qty;
            }
        }

        arsort($tablo);
        return view("rapor.uruntoplamFiyat",compact("tablo","names","tarih1","tarih2","prices"));
    }

    public function urunbazinda($tarih1=0,$tarih2=0,$sube_id=0)
    {
        $tarih1 = $tarih1 ? $tarih1 : date("Y-m-d");
        $tarih2 = $tarih2 ? $tarih2 : date("Y-m-d");

        $tarih1 = date("Y-m-d",strtotime($tarih1));
        $tarih2 = date("Y-m-d",strtotime($tarih2));


        $orders = Irsaliye::whereBetween('siptar',array($tarih1,$tarih2))
        	->whereHas("orderproducts",function($query){
        		$query->whereNotIn("status",[0,5,6]);
       		 })
        	->get();

       $subeler=Sube::pluck('subeadi','id')->all();

       $tablo = array();

        foreach($orders as $order)
        {
            $discount_yuzde = 1 - $order->discount_yuzde/100;
            foreach($order->orderproducts as $op)
            {
                if(isset($tablo[$op->uretim_yeri]["product"][$op->product_name]["qty"]))
                {
                    $tablo[$op->uretim_yeri]["product"][$op->product_name]["qty"] += $op->product_qty;
                    $tablo[$op->uretim_yeri]["product"][$op->product_name]["price"] += $op->product_price * $discount_yuzde;
                }
                else
                {
                    $tablo[$op->uretim_yeri]["product"][$op->product_name]["qty"] = $op->product_qty;
                    $tablo[$op->uretim_yeri]["product"][$op->product_name]["price"] = $op->product_price * $discount_yuzde;
                }

                if(isset($tablo[$op->uretim_yeri]["toplam"]))
                {
                    $tablo[$op->uretim_yeri]["toplam"]+= $op->product_qty*$op->product_price  * $discount_yuzde;
                }
                else
                {
                    $tablo[$op->uretim_yeri]["toplam"] = $op->product_qty*$op->product_price * $discount_yuzde;
                }
            }

            if(isset($tablo[$op->uretim_yeri]["total"]))
            {
                $tablo[$op->uretim_yeri]["total"]+=$order->total;
            }
            else
            {
                $tablo[$op->uretim_yeri]["total"] = $order->total;
            }
        }

        return view("rapor.urunbazinda",compact("tablo","subeler","discount_yuzde"));
    }

    public function urunkategori($yil)
    {
        $aylar = config("tarih.aylar");
        $subeler=Sube::pluck('subeadi','id')->all();
        $tablo = array();
        foreach ($aylar as $key => $ay) {
            $tablo[$key]=$this->_urunkategori($yil,$key);
        }

        return view("rapor.urunkategori",compact("aylar","tablo","subeler"));
    }

    private function _urunkategori($yil,$ay)
    {

        $orders = Irsaliye::whereRaw('year(siptar) = ? ',array($yil))
            ->whereRaw('month(siptar) = ? ',array($ay))
            ->whereHas("orderproducts",function($query){
        		$query->whereNotIn("status",[0,5,6]);
       		 })
            //->whereBetween('siptar',array('2013-01-01','2013-09-30'))
            ->with("orderproducts")->get();



       $tablo = array();

        foreach($orders as $order)
        {
            $discount =  1-$order->discount_yuzde/100;
            foreach($order->orderproducts as $op)
            {
                if(isset($tablo[$op->uretim_yeri]["adet"]))
                {
                    $tablo[$op->uretim_yeri]["adet"]+=$op->product_qty;
                }
                else
                {
                    $tablo[$op->uretim_yeri]["adet"] = $op->product_qty;
                }

                if(isset($tablo[$op->uretim_yeri]["toplam"]))
                {
                    $tablo[$op->uretim_yeri]["toplam"]+= $op->product_qty*$op->product_price * $discount;
                }
                else
                {
                    $tablo[$op->uretim_yeri]["toplam"] = $op->product_qty*$op->product_price * $discount;
                }
            }

            if(isset($tablo[$op->uretim_yeri]["total"]))
            {
                $tablo[$op->uretim_yeri]["total"]+= $order->total  - $order->tax;
                $tablo[$op->uretim_yeri]["kdv"]+=  $order->tax;
                $tablo[$op->uretim_yeri]["discount"]+=  $order->coupon_discount;
            }
            else
            {
                $tablo[$op->uretim_yeri]["total"] = $order->total  - $order->tax;
                $tablo[$op->uretim_yeri]["kdv"] =  $order->tax;
                $tablo[$op->uretim_yeri]["discount"] =  $order->coupon_discount;
            }
        }

        return $tablo;
    }

    public function semturun($tarih1,$tarih2,$sube_id=0)
    {
        $tarih1 = date("Y-m-d",strtotime($tarih1));
        $tarih2 = date("Y-m-d",strtotime($tarih2));
        $ops = Irsaliyeproduct::selectRaw('year(cikis_tarihi) as yil,month(cikis_tarihi) as ay,sum(product_price*product_qty) as toplam,count(*) as adet,il_adi,semt_adi')
        	->whereNotIn("status",[0,5,6])
            ->whereBetween("cikis_tarihi",[$tarih1,$tarih2])
            ->groupBy("yil","ay","il_adi","semt_adi")
            ->orderBy("yil","ay")
            //->whereRaw('year(cikis_tarihi) = ? ',array($yil))
            //->whereRaw('month(cikis_tarihi) = ? ',array($ay))
            //->whereBetween('cikis_tarihi',array('2013-01-01','2013-09-30'))
            //->with("orderproducts")
            ->get();

       $subeler=Sube::pluck('subeadi','id')->all();

       $tablo = [];

        foreach($ops as $op)
        {
            if(isset($tablo[$op->il_adi][$op->semt_adi][$op->yil][$op->ay]["toplam"]))
            {
                $tablo[$op->il_adi][$op->semt_adi][$op->yil][$op->ay]["toplam"]+=$op->toplam;
                $tablo[$op->il_adi][$op->semt_adi][$op->yil][$op->ay]["adet"]+=$op->adet;
            }
            else
            {
                $tablo[$op->il_adi][$op->semt_adi][$op->yil][$op->ay]["toplam"] = $op->toplam;
                $tablo[$op->il_adi][$op->semt_adi][$op->yil][$op->ay]["adet"] = $op->adet;
            }
        }

        $aylar = config("tarih.aylar");

        return view("rapor.semturun",compact("tablo","subeler","aylar"));
    }

    public function kategori($tarih1,$tarih2,$sube_id=0)
    {
        $tarih11 = date("Y-m-d",strtotime($tarih1));
        $tarih21 = date("Y-m-d",strtotime($tarih2));

        $kategori = Product::pluck("category_name","id")->all();
        $product = Product::pluck("name","id")->all();

        $orders = Irsaliye::whereHas("orderproducts",function($query){
        		$query->whereNotIn("status",[0,5,6]);
       		 })
            //->whereRaw('year(cikis_tarihi) = ? ',array($yil))
            //->whereRaw('month(cikis_tarihi) = ? ',array($ay))
            //->where("cikis_tarihi","<>","2015-02-14")
            ->whereBetween('siptar',array($tarih11,$tarih21))
            ->with("orderproducts")
            ->orderBy("siptar")
            ->get();

        $p = array();

        foreach($orders as $order)
        {
            foreach ($order->orderproducts as  $op) {

                $kategori[$op->product_id] = isset($kategori[$op->product_id]) ? $kategori[$op->product_id] : "Değişti..";

                if(isset($p[$kategori[$op->product_id]][$op->cikis_tarihi]))
                {
                    $p[$kategori[$op->product_id]][$op->cikis_tarihi] += $op->product_qty;
                }
                else
                {
                        $p[$kategori[$op->product_id]][$op->cikis_tarihi] = $op->product_qty;

                }

            }
        }

        $tarih1 = date("d-m-Y",strtotime($tarih1));
        $tarih2 = date("d-m-Y",strtotime($tarih2));

        $tarihler = $this->dateRange($tarih1,$tarih2,'+1 day','d-m-Y');

        return view("rapor.kategori",compact("p","kategori","product","tarihler"));
    }



    function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {

            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }

    public function sube_urunler(Request $request,$tarih1,$tarih2,$uretim_yeri)
    {
        $komisyon = $request->get("komisyon",10);
        $tarih11 = date("Y-m-d",strtotime($tarih1));
        $tarih21 = date("Y-m-d",strtotime($tarih2));
        $tum_urunler = DB::table("irsaliye as A")
            ->join("irsaliyeproducts as B","A.id","=","B.order_id")
            ->select("A.discount_yuzde","B.product_sku","B.product_name","B.product_price","B.product_tax","B.product_qty")
            ->whereBetween('B.cikis_tarihi',array($tarih11,$tarih21))
            ->where("B.uretim_yeri",$uretim_yeri)
            ->where("B.status",4)
            ->orderBy("B.product_name")
            ->get();

        $adetler = [];
        $names = [];
        $prices = [];
        foreach ($tum_urunler as $item) {
            $adetler[$item->product_sku] = isset($adetler[$item->product_sku]) ? $adetler[$item->product_sku]+$item->product_qty : $item->product_qty;
            $prices[$item->product_sku] = isset($prices[$item->product_sku]) ? $prices[$item->product_sku]+$item->product_price*(100+$item->product_tax)/100 : $item->product_price * (100+$item->product_tax)/100 ;
            $names[$item->product_sku] = $item->product_name;
        }

        return view("rapor.sube_urunler",compact("names","adetler","prices","tarih1","tarih2","komisyon"));
    }

    public function sube_mutabakat($tarih1,$tarih2,$uretim_yeri)
    {
        $komisyon = request()->get("komisyon",10);

        $tarih11 = date("Y-m-d",strtotime($tarih1));
        $tarih21 = date("Y-m-d",strtotime($tarih2));

        $Jungle_satislari = Irsaliye::whereHas("orderproducts",function($query)use($uretim_yeri){
        		$query->whereNotIn("status",[0,5,6]);
        		$query->where("uretim_yeri",$uretim_yeri);
       		 })
            ->whereBetween('siptar',array($tarih11,$tarih21))
            ->whereNotIn("payment",array("nakit","pos","kapida"))
            ->with("orderproducts")
            ->get();


        $sube_satislari = Irsaliye::whereHas("orderproducts",function($query)use($uretim_yeri){
        		$query->whereNotIn("status",[0,5,6]);
        		$query->where("uretim_yeri",$uretim_yeri);
       		})
            ->whereBetween("siptar",array($tarih1,$tarih2))
            ->whereIn("payment",array("nakit","pos","kapida"))
            ->with("orderproducts")
            ->get();

/*
        $baska_subeden = Irsaliye::where("status",4)
                ->whereBetween("cikis_tarihi",array($tarih1,$tarih2))
                ->where("uretim_yeri",$uretim_yeri)
                ->where("sube_kodu",$uretim_yeri)
                ->whereIn("payment",array("nakit","pos","kapida"))
                ->with("orderproducts")
                ->get();
*/

        $baska_subeye = Irsaliye::whereHas("orderproducts",function($query)use($uretim_yeri){
	        		$query->whereNotIn("status",[0,5,6]);
	        		$query->where("uretim_yeri","<>",$uretim_yeri);
	        		$query->where("sube_kodu",$uretim_yeri);
	       		 })
                ->whereBetween("siptar",array($tarih1,$tarih2))
                ->with("orderproducts")
                ->get();

        return view("rapor.sube_mutabakat",compact("Jungle_satislari","sube_satislari","baska_subeye","tarih1","tarih2","komisyon"));

    }
    /*

    public function sube_mutabakat($tarih1,$tarih2,$uretim_yeri)
    {
        $komisyon = request()->get("komisyon",10);

        $tarih11 = date("Y-m-d",strtotime($tarih1));
        $tarih21 = date("Y-m-d",strtotime($tarih2));

        $Jungle_satislari = Irsaliye::where("status",4)

            ->whereBetween('cikis_tarihi',array($tarih11,$tarih21))
            ->where("uretim_yeri",$uretim_yeri)
            ->whereNotIn("payment",array("nakit","pos","kapida"))
            //->where("coupon_discount",0)
            ->with("orderproducts")
            ->get();

        $Jungle_indirimli= Irsaliye::where("status",4)
                ->whereBetween("cikis_tarihi",array($tarih1,$tarih2))
                ->where("uretim_yeri",$uretim_yeri)
                ->whereNotIn("payment",array("nakit","pos","kapida"))
                ->where("coupon_discount",">",0)
                ->with("orderproducts")
                ->get();


        $sube_satislari = Irsaliye::where("status",4)
                ->whereBetween("cikis_tarihi",array($tarih1,$tarih2))
                ->where("uretim_yeri",$uretim_yeri)
                ->whereIn("payment",array("nakit","pos","kapida"))
                ->with("orderproducts")
                ->get();


        $baska_subeden = Irsaliye::where("status",4)
                ->whereBetween("cikis_tarihi",array($tarih1,$tarih2))
                ->where("uretim_yeri",$uretim_yeri)
                ->where("sube_kodu",$uretim_yeri)
                ->whereIn("payment",array("nakit","pos","kapida"))
                ->with("orderproducts")
                ->get();


        $baska_subeye = Irsaliye::where("status",4)
                ->whereBetween("cikis_tarihi",array($tarih1,$tarih2))
                ->where("uretim_yeri",'<>',$uretim_yeri)
                ->where("sube_kodu",$uretim_yeri)
                ->with("orderproducts")
                ->get();

        return view("rapor.sube_mutabakat",compact("Jungle_satislari","sube_satislari","baska_subeye","tarih1","tarih2","komisyon"));

    }*/

    public function orders($sube_id=0,$ay=0)
    {
        $ay = $ay?:date("m");
        $sube = $sube_id ? Sube::findOrFail($sube_id)->subeadi : 'Tüm Şubeler';

        $orders = Irsaliye::
            whereMonth("siptar",$ay)
            ->whereYear("siptar",2017)
            ->whereHas("orderproducts",function($query)use($sube_id){
                $query->whereNotIn("status",[0,5,6]);
                if($sube_id){
                    $query->where("uretim_yeri",$sube_id);
                }
            })
            ->get();

        return view("rapor.orders",compact("orders","sube","sube_id"));
    }
    public function excel($tarih1,$tarih2,$sube_id=0)
    {
        $uretim_yeri = Sube::find($sube_id);
        $tarih1 = date("Y-m-d",strtotime($tarih1));
        $tarih2 = date("Y-m-d",strtotime($tarih2));
        $subeadi = $uretim_yeri ? $uretim_yeri->subeadi : "TümŞubeler";
        $filename = "Jungle-".$subeadi."-".date("d.m.Y",strtotime($tarih1))."-".date("d.m.Y",strtotime($tarih2));

        $ops = IrsaliyeProduct::with("irsaliyesi")
            ->where("ek",0)
            ->whereNotIn("status",[0,5,6])
            ->where(function($query)use($sube_id){
                if($sube_id){
                    $query->where("uretim_yeri",$sube_id);
                }
            })
            ->whereHas("irsaliyesi",function($query)use($tarih1,$tarih2){
                $query->whereBetween("siptar",array($tarih1,$tarih2));
            });

        $subes = Sube::pluck("subeadi","id")->all();
        $payments = config("ayar.payments");
        $tum_siparisler = $this->_orderToArray($ops,$subes,$payments);

        $Jungle_satislari = $ops->whereHas("irsaliyesi",function($query){
                $query->whereIn("payment",["kredi","havale","cari"]);
            })->get();

        $sube_satislari = IrsaliyeProduct::with("irsaliyesi")
            ->where("ek",0)
            ->whereNotIn("status",[0,5,6])
            ->where(function($query)use($sube_id){
                if($sube_id){
                    $query->where("uretim_yeri",$sube_id);
                }
            })
            ->whereHas("irsaliyesi",function($query)use($tarih1,$tarih2){
                $query->whereBetween("siptar",array($tarih1,$tarih2));
                $query->whereIn("payment",["kapida","subede"]);
            })->get();

        Excel::create($filename, function($excel) use($tum_siparisler,$Jungle_satislari,$sube_satislari,$tarih1,$tarih2) {

            $excel->sheet('Tüm Siparişler', function($sheet) use ($tum_siparisler) {

                $sheet->fromArray($tum_siparisler)->setAutoFilter()->removeRow(1,1)->freezeFirstRow();

            });

            $excel->sheet('Mutabakat', function($sheet) use($Jungle_satislari,$sube_satislari,$tarih1,$tarih2)  {

                $sheet->loadView('rapor.sube_mutabakat_excel',compact("Jungle_satislari","sube_satislari","tarih1","tarih2"));

            });
        })->export('xlsx');
    }


    private function _orderToArray($ops,$subes,$payments)
    {
        $durumlar = config("ayar.durumlar");

        $arr[0] = [
            'SİPARİŞ NO',
            'ÜRETİM YERİ',
            'DURUM',
            'SİPARİŞ TARİHİ',
            'ÇIKIŞ TARİHİ',
            'TESLİM TARİHİ',
            'İLÇE',
            'İL',
            'ARATOPLAM',
            'MATRAH%8',
            'MATRAH%18',
            'KDV%8',
            'KDV%18',
            'TUTAR',
            'TUTAR2',
            'İNDİRİM',
            'TESLİMAT',
            'ÜRÜN',
            'TESLİM SAATİ',
            'TESLİM ŞEKLİ',
            'SİPARİŞ ALAN',
            'ÖDENDİ Mİ',
            'ÖDEME',
            'İRSALİYE KESİLDİMİ?',
            'İRSALİYE NO',
            'FATURA KESİLDİMİ?',
            'FATURA NO',
            'KUPON',
        ];

        $i=0;
        foreach($ops->get() as $key=>$op)
        {
            $siparis_kalem = 0;
            $order = $op->irsaliyesi;

            if($order)
            {
                $fatura_var = $order->fatura_id ? 'Evet' : 'Hayır';
                $faturano = $order->faturano;
            }
            else
            {
                die($order->id." ".$order->order_number." irsaliyesi bulunamadı..");
            }

            $iskontooran =$order->discount_yuzde/100;

            $aratoplam = 0;
            $toplamkdv = 0;
            $kdv1=0;
            $kdv8=0;
            $kdv18=0;
            $geneltoplam = 0;
            $matrah8 = 0;
            $matrah18 = 0;

            $product_name = $op->product_name." ";
            $siparis_kalem++;

            $product_price = $op->product_price - $order->coupon_discount;

            $kdvtoplam = $op->product_qty * ($product_price * (100+ $op->product_tax)/100 - $product_price);

            if($op->product_tax==1)
            {
              $kdv1+=$kdvtoplam;
            }
            if($op->product_tax==8)
            {
              $kdv8+= $kdvtoplam;
              $matrah8 += $op->product_qty * $product_price;
            }
            if($op->product_tax==18)
            {
              $kdv18+=$kdvtoplam;
              $matrah18 += $op->product_qty * $product_price;
            }


            if($order->shipping)
            {
                //$kdv18 += $order->shipping-$order->shipping/1.18;
                //$matrah18 += $order->shipping/1.18;
            }

            $arr[$key+1]=array(

                $order->order_number,
                isset($subes[$op->uretim_yeri]) ? $subes[$op->uretim_yeri]: $op->uretim_yeri,
                isset($durumlar[$op->status]) ? $durumlar[$op->status]:$op->status,
                date("d.m.Y",strtotime($order->siptar)),
                date("d.m.Y",strtotime($op->cikis_tarihi)),
                date("d.m.Y",strtotime($op->teslim_tarihi)),
                $op->semt_adi,
                $op->il_adi,
                ($matrah8+$matrah18),
                round($matrah8,2),
                round($matrah18,2),
                round($kdv8,2),
                round($kdv18,2),
                ($matrah18+$matrah8+$kdv8+$kdv18),
                $order->total,
                $order->coupon_discount,
                $op->shipping,
                $product_name,
                $op->teslim_saati,
                $op->ship,
                $order->siparisalan,
                $order->payment_ok ? 'EVET' : 'HAYIR',
                isset($payments[$order->payment])?$payments[$order->payment]:$order->payment,
                $order->irsaliye ? 'EVET' : 'HAYIR',
                $order->irsaliye,
                $fatura_var,
                $faturano,
                $order->coupon_code,
            );
            $i= $key+3;
        }
            $arr[$i] = array(
                        '','','','','','','','','','','','','','','','','', '','','','','','','','','','','','',
                    );
            $arr[$i+1] = array(
                        '','','','','','','','','','','','MATRAH8',"=SUM(J2:J".$i.")",'','','','','','','','','','','','','','','','',
                    );
            $arr[$i+2] = array(
                        '','','','','','','','','','','','MATRAH18',"=SUM(K2:K".$i.")",'','','','','','','','','','','','','','','','',
                    );
            $arr[$i+3] = array(
                        '','','','','','','','','','','','KDV8',"=M".($i+2)."*0.08",'','','','','','','','','','','','','','','','',
                    );
            $arr[$i+4] = array(
                        '','','','','','','','','','','','KDV18',"=M".($i+3)."*0.18",'','','','','','','','','','','','','','','','',
                    );
        return $arr;
    }

}
