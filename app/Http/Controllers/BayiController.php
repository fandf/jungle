<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sube;
use App\Product;
use App\Customer;
use App\Order;
use App\Orderproduct;
use App\Productimage;
use App\Irsaliyetra;
use App\Stock;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Repositories\BayiIrsaliyeRepository;
use Illuminate\Http\File;
use App\Jobs\BaskiTasi;
use DB;

class BayiController extends Controller
{
    protected $repo;

    public function __construct(BayiIrsaliyeRepository $repo)
    {
        $this->middleware('ifnotbayi');
        $this->repo = $repo;
    }

    public function index()
    {
        $user = auth()->guard("bayi")->user();

    	return view("bayi.index",compact("user"));
    }

    public function tum()
    {
        $user = auth()->guard("bayi")->user();
        $tarih1 = request()->has("tarih1") ? date("Y-m-d",strtotime(request()->input("tarih1"))) : date("Y-m-d");
        $tarih2 = request()->has("tarih2") ? date("Y-m-d",strtotime(request()->input("tarih2"))) : date("Y-m-d");
        $tip = request()->input("tip","cikis_tarihi");

        $limit = 10000;
        return [
            "tum"=>$this->repo->getBayiWithFilter($tarih1,$tarih2,$tip,$limit)
        ];
    }

    public function search(Request $request)
    {
        $user = auth()->guard("bayi")->user();
        $tarih1 = (date("Y")-1)."-01-01";
        $tarih2 = date("Y")."-12-31";
        $tip = "";
        $limit = 500;

        return $this->repo->getBayiWithFilter($tarih1,$tarih2,$tip,$limit);
    }

    public function create()
    {
        $user = auth()->guard("bayi")->user();
        $kanallar = config("ayar.kanallar");
        $products = Product::select("sku","name","brand","options","tops")->get();
        $customer = Customer::where("id",$user->customer_id)
            ->select("id","name","company_name","mobile_phone","email","address","city","semt","vergi_dairesi","vergi_no")
            ->firstOrFail();
         $images = Productimage::selectRaw("id,name,filename as src")
        ->where(function($query)use($user){
            $query->where("customer_id",$user->customer_id);
            if($user->katalog){
                $query->orWhere("customer_id",1);
            }
        })
        ->where("name","<>","")
        ->where("status",1)
        ->get();

        return view("bayi.create",compact("user","kanallar","brands","products","customer","images"));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            // "order.gon_adsoyad" =>  "required",
            // "order.gon_cep" =>  "required",
            "order.fatura_unvan" =>  "required",
            "order.fatura_adres" =>  "required",
            // "order.vergi_dairesi" =>  "required",
            // "order.vergi_no" =>  "required",
            "order.siparisalan" =>  "required",
            // "order.alici_firma" =>  "required",
            // "order.alici_adsoyad" =>  "required",
            // "order.alici_telefon" =>  "required",
            // "order.il_adi" =>  "required",
            // "order.semt_adi" =>  "required",
            // "order.alici_adres" =>  "required",
            "orderproducts.*.product_top" =>"required",
            "orderproducts.*.product_sku" =>"required",
            "orderproducts.*.product_width" =>"required",
            "orderproducts.*.product_height" =>"required",
            "orderproducts.*.product_name" =>"required",
            "orderproducts.*.product_qty" =>"required",
            //"orderproducts.*.product_options" =>"required",
            "orderproducts.*.product_image" =>"required",
            // "orderproducts.*.product_price" =>"required",
            // "orderproducts.*.product_tax" =>"required",
        ]);

        $order = new Order;
        $order->order_number = $this->create_order_number();
        $order->user_id = auth()->guard("bayi")->id();
        $order->sube_kodu = 0;
        $order->customer_id = 0;
        $order->tax = 0;
        $order->subtotal = 0;
        $order->total = 0;
        $order->customer_id = $request->input("customer.id");
        $order->gon_adsoyad = $request->input("order.gon_adsoyad");
        $order->gon_cep = $request->input("order.gon_cep");
        $order->fatura_unvan = $request->input("order.fatura_unvan");
        $order->fatura_adres = $request->input("order.fatura_adres");
        $order->vergi_dairesi = $request->input("order.vergi_dairesi");
        $order->vergi_no = $request->input("order.vergi_no");
        $order->siparisalan = $request->input("order.siparisalan");
        $order->bize_mesaj = $request->input("order.bize_mesaj");
        $order->onay = 1;
        $order["siptar"] = request()->input("order.siptar") ? date("Y-m-d",strtotime(request()->input("order.siptar"))) : date("Y-m-d");


        $order->save();

        $adet = 0;
        foreach($request->input("orderproducts") as $key=>$r)
        {
            $op = new Orderproduct();
            $op->order_id = $order->id;
            $op->alici_customer_id = $request->input("subcustomer.id");
            $op->alici_firma = $request->input("order.alici_firma");
            $op->alici_adsoyad = $request->input("order.alici_adsoyad");
            $op->alici_telefon = $request->input("order.alici_telefon");
            $op->il_adi = $request->input("order.il_adi");
            $op->semt_adi = $request->input("order.semt_adi");
            $op->alici_adres = $request->input("order.alici_adres");
            $op->product_sku = $request->input("orderproducts.".$key.".product_sku");
            $op->product_top = $request->input("orderproducts.".$key.".product_top");
            $op->product_width = $request->input("orderproducts.".$key.".product_width");
            $op->product_height = $request->input("orderproducts.".$key.".product_height");
            $op->product_desc = $request->input("orderproducts.".$key.".product_desc");
            $op->product_name = $request->input("orderproducts.".$key.".product_name");
            $op->product_qty = $request->input("orderproducts.".$key.".product_qty");
            $op->product_options = $request->input("orderproducts.".$key.".product_options");
            $op->product_image = $request->input("orderproducts.".$key.".product_image");
            $op->product_price = $request->input("orderproducts.".$key.".product_price") ?:0;
            $op->product_tax = $request->input("orderproducts.".$key.".product_tax") ?:8;
            $op->mevcut_urun = $request->input("orderproducts.".$key.".mevcut_urun") ? $request->input("orderproducts.".$key.".mevcut_urun"): "";
            $op->cikis_tarihi = request()->input("order.teslim_tarihi") ? date("Y-m-d",strtotime(request()->input("order.teslim_tarihi"))) : date("Y-m-d");
            $op->teslim_tarihi = request()->input("order.teslim_tarihi") ? date("Y-m-d",strtotime(request()->input("order.teslim_tarihi"))) : date("Y-m-d");

            $height = $op->product_height;
            $product_top = $op->product_top;

            if($op->product_width<$op->product_height AND $op->product_height<=$op->product_top)
            {
                $height = $op->product_width;
            }

            if($product_top == 150)
            {
                if($op->product_width == 75)
                {
                    $product_top = 75;
                    $height = $op->product_height;
                }
                elseif($op->product_height==75)
                {
                    $product_top = 75;
                    $height = $op->product_width;
                }
            }
            elseif($product_top == 160)
            {
                if($op->product_width == 80)
                {
                    $product_top = 80;
                    $height = $op->product_height;
                }
                elseif($op->product_height==80)
                {
                    $product_top = 80;
                    $height = $op->product_width;
                }
            }

            $stock = Stock::where("product_sku",$op->product_sku)
                ->where("product_top",$product_top)
                ->where("product_height",$height)
                ->first();


            if($stock){
                $op->stok_kodu = $stock->kod;
            }

            $op->status = 0;
            $op->customer_id = $order->customer_id;
            //baskı image
            if(count($request->input("orderproducts.".$key.".uploadList"))>0)
            {
                $url = $request->input("orderproducts.".$key.".uploadList")[0]["url"] ?? "";
                if($url){
                    $op->production_image = str_replace('upload/','',$url);
                    dispatch(new BaskiTasi($order->fatura_unvan,$url));
                }
            }
            $op->save();

            $ot = new Irsaliyetra();
            $ot->order_id = $order->id;
            $ot->op_id = $op->id;
			$ot->order_number = $order->order_number;
			$ot->tur = "";
			$ot->link = 'setEklendi';
			$ot->description = "Sipariş Eklendi";
			$ot->user_id = $order->user_id;
			$ot->user_type = 'App\\Bayi';
			$ot->kurye_id = 0;
            $ot->save();

            $adet ++;

            if($request->input("orderproducts.".$key.".yukleme_var")==true){
                $image = new Productimage;
                $image->name  = $op->mevcut_urun;
                $image->filename = $op->product_image;
                $image->product_id =  1;
                $image->customer_id = $order->customer_id;
                $image->status = 1;
                $image->save();
            }
        }



        if($adet>0){
            return ["success"=>"Sipariş Eklendi.."];
        }
        return ["success"=>false];
    }

    public function upload(Request $request)
    {
        $name = "";

        if($request->file('file'))
        {
          $image = $request->file('file');

          $name = time().$image->getClientOriginalName();
          $image->storeAs('/public/uploads/org',$name);
          $img = \Image::make($request->file('file'))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path("app/public/uploads/".$name));

            return response()->json(['success' => 'ok','image'=>$name], 200);
        }
        return response()->json(['success' => false,'image'=>$name], 404);
    }

    private function create_order_number()
    {
        return date("YmdHis");
    }
    public function show_json($id)
    {
        return  $this->repo->getByIdWithOp($id);
    }
    public function op_json($id)
    {
        return  $this->repo->getIrsaliyeproductById($id);
    }

    public function op_save(Request $request)
    {

        $this->validate($request,[
            "product_top" =>"required",
            "product_width" =>"required",
            "product_height" =>"required",
            "product_qty" =>"required",
        ]);

        $op = $this->repo->getIrsaliyeproductById($request->input("id"));
        $op->product_top = $request->input("product_top");
        $op->product_width = $request->input("product_width");
        $op->product_height = $request->input("product_height");
        $op->product_qty = $request->input("product_qty");
        $op->product_desc = $request->input("product_desc");
        $op->product_options = $request->input("product_options");


            $height = $op->product_height;
            $product_top = $op->product_top;

            if($op->product_width<$op->product_height AND $op->product_height<=$op->product_top)
            {
                $height = $op->product_width;
            }

            if($product_top == 150)
            {
                if($op->product_width == 75)
                {
                    $product_top = 75;
                    $height = $op->product_height;
                }
                elseif($op->product_height==75)
                {
                    $product_top = 75;
                    $height = $op->product_width;
                }
            }
            elseif($product_top == 160)
            {
                if($op->product_width == 80)
                {
                    $product_top = 80;
                    $height = $op->product_height;
                }
                elseif($op->product_height==80)
                {
                    $product_top = 80;
                    $height = $op->product_width;
                }
            }

            $stock = Stock::where("product_sku",$op->product_sku)
                ->where("product_top",$product_top)
                ->where("product_height",$height)
                ->first();


            if($stock){
                $op->stok_kodu = $stock->kod;
            }

        $op->save();

        $ot = new Irsaliyetra();
        $ot->order_id = $op->order_id;
        $ot->op_id = $op->id;
        $ot->order_number = $request->input("irsaliyesi.order_number");
        $ot->tur = "";
        $ot->link = 'setGüncellendi';
        $ot->description = "Sipariş Güncellendi";
        $ot->user_id = auth()->guard("bayi")->id();
        $ot->user_type = 'App\\Bayi';
        $ot->kurye_id = 0;
        $ot->save();

        return ["success"=>true];
    }

    public function mesaj_yaz($id)
    {
         $op =   $this->repo->getIrsaliyeproductById($id);

        if($op)
        {
            $op->irsaliye_no = $op->parent_id;
            $op->save();
            return view("print.mesaj",compact("op"));
            //return PDF::setPaper('a6','landscape')->loadView('print.mesaj', $data)->stream();
        }
    }

    public function cikis_yaz()
    {
        $ids = request("product_ids");
        $ops =   $this->repo->getIrsaliyeproductByIds($ids);

        return view("print.cikis",compact("ops"));
    }

}
