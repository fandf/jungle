<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function subcustomer($customer_id)
    {
        return Customer::where(function($query)use($customer_id){
            $query->where("id",$customer_id);
            $query->orWhere("parent_id",$customer_id);
        })
        ->select("id","name","company_name","mobile_phone","email","address","city","semt","vergi_dairesi","vergi_no")
        ->get();
    }
}
