<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Sube;
use App\Top;

class UretimdeController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware(["auth","can:baski"]);
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toplar = Top::where("aktif",1)->pluck("name","id")->all();
        return view('uretimde',compact("toplar"));
    }

    public function store()
    {
        $user = Auth::user();
        return $this->repo->getUretimde($user);
    }


    public function uretildi(Request $request)
    {
        $user = Auth::user();
        $top_id = $request->input("top_id");
        foreach($request->input("prd") as $op_id)
        {
            $this->repo->setUretildi($op_id,$user,$top_id);
        }
        return 1;
    }
}
