<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\IrsaliyeRepository;
use Illuminate\Support\Facades\Auth;
use App\Irsaliyetra;
use App\Sube;
use App\Kurye;
use App\Number;
use DB;
use PDF;

class IrsaliyeController extends Controller
{
    protected $repo;

    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware("auth");
        $this->repo = $repo;
    }

    public function index()
    {
        $kuryeler = Kurye::where("aktif",1)->select("id","sube_id","name")->get()->toArray();

        $user = Auth::user();

        return view("irsaliye.index",compact("user","kuryeler"));
    }

    public function fatura_kesilmeyenler()
    {
        $kuryeler = Kurye::where("aktif",1)->select("id","sube_id","name")->get()->toArray();

        $user = Auth::user();

        return view("irsaliye.fatura_kesilmeyenler",compact("user","kuryeler"));
    }

    public function show($id)
    {
        $order = $this->repo->getByIdWithOp($id);

        return view("orders.show",compact("order","kartlar"));
    }

    public function show_json($id)
    {
        return  $this->repo->getByIdWithOp($id);
    }

    public function store_json(Request $request)
    {
        return app("App\Repositories\IrsUpdateRepository")
                ->update($request->all());
    }

    public function opstore_json(Request $request)
    {
        return app("App\Repositories\IrsUpdateRepository")
                ->opguncelle($request);
    }

    public function opnew_json(Request $request)
    {
        return app("App\Repositories\IrsUpdateRepository")
                ->opnew($request);
   }

    public function opsil_json(Request $request)
    {
        return app("App\Repositories\IrsUpdateRepository")
                ->opsil($request->input("order_id"),$request->input("id"));
    }

    public function coupon_update(Request $request)
    {
        return app("App\Repositories\IrsUpdateRepository")
                ->coupon_update($request->input("order_id"),$request->input("coupon_code"),$request->input("coupon_discount"));
    }

    public function uretimde()
    {
        $kuryeler = Kurye::where("aktif",1)->select("id","sube_id","name")->get()->toArray();
        $user = Auth::user();

        return view("irsaliye.uretimde",compact("user","kuryeler"));
    }

    public function uretilen()
    {
        $user = Auth::user();

        $kuryeler = Kurye::where("aktif",1)
                    ->where(function($query) use ($user){

                        if($user->sube_id)
                        {
                            $query->where("sube_id",$user->sube_id);
                        }

                    })
                    ->select("id","sube_id","name")->get()->toArray();

        $user = Auth::user();

        return view("irsaliye.uretilen",compact("user","kuryeler"));
    }

    public function dagitimda()
    {
        $kuryeler = Kurye::where("aktif",1)->select("id","sube_id","name")->get()->toArray();
        $user = Auth::user();

        return view("irsaliye.dagitimda",compact("user","kuryeler"));
    }

    public function fatura_kesilmeyenler_json(Request $request)
    {
        $data = $request->json("data");

        $tarih1 = isset($data["tarih1"]) ? date("Y-m-d",strtotime($data["tarih1"])) : date("Y-m-d");
        $tarih2 = isset($data["tarih2"]) ? date("Y-m-d",strtotime($data["tarih2"])) : date("Y-m-d");
        $tip = isset($data["tip"]) ? $data["tip"] : "cikis_tarihi";

        return ["data"=>$this->repo->getFaturaKesilmeyenler($tarih1,$tarih2,$tip,$data)];
    }


    public function teslimet(Request $request)
    {
        $order_id = $request->json("order_id");

        return $this->repo->setTeslimEdildi($order_id);
    }

    public function teslimetsms(Request $request)
    {
        $order_id = $request->json("order_id");

        return $this->repo->setTeslimEdildiSms($order_id);
    }

    public function cikmadi(Request $request)
    {
        $op_id = $request->input("op_id");

        return $this->repo->setCikmadi($op_id);
    }

    public function hatali(Request $request)
    {
        $op_id = $request->input("op_id");

        return $this->repo->setHatali($op_id);
    }

    public function iptalet(Request $request)
    {
        $op_id = $request->input("op_id");

        return $this->repo->setIptalEdildi($op_id);
    }

    public function uretimegeri(Request $request)
    {
        $op_id = $request->input("op_id");

        return $this->repo->setUretimeGeri($op_id);
    }

    public function basilanageri(Request $request)
    {
        $op_id = $request->input("op_id");

        return $this->repo->setBasilanaGeri($op_id);
    }

    public function printle($id,Request $request)
    {
        $irs_no = $request->input("irs_no","");

        $op =   $this->repo->getIrsaliyeproductById($id);

        if(!$op)
        {
            die("İrsaliye bulunamadı..");
        }

        $user = Auth::user();

        $number = Number::where('type','irsaliye')->where('sube_id',1)->first();

        if(!$number)
        {
            $number = Number::create([
                "type"      => "irsaliye",
                "sube_id"   => 1,
                "no"        => "00001"
            ]);
        }

        if(!$irs_no)
        {
           $irs_no = $number->no;
        }

        $num = preg_replace("/[^0-9]/","", $irs_no);

        $str = preg_replace("/[^A-Za-z]/","", $irs_no);

        if(strlen($num)!=strlen($num+1))
        {
          $num1 = str_pad($num+1, strlen($num),"0",STR_PAD_LEFT);
        }
        else
        {
          $num1 = $num +1;
        }

        $number->no = $str.$num1;

        $number->save();

        $op->irsaliye_no = $irs_no;

        $op->save();


        return view("print.irsaliye",compact("op"));
        //return PDF::setPaper('a5','landscape')->loadView('print.irsaliye', $data)->stream();
    }


    public function mesaj_yaz()
    {
        $ids = request("product_ids");
        $ops =   $this->repo->getIrsaliyeproductByIds($ids);

        foreach($ops as $op)
        {
            if(!$op->irsaliye_no){
                $op->irsaliye_no = "B".str_pad($op->id,6,'0',STR_PAD_LEFT);
                $op->save();
            }
            //return PDF::setPaper('a6','landscape')->loadView('print.mesaj', $data)->stream();
        }
        return view("print.mesaj",compact("ops"));
    }

    public function cikis_yaz()
    {
        $ids = request("product_ids");
        $ops =   $this->repo->getIrsaliyeproductByIds($ids);

        return view("print.cikis",compact("ops"));
    }

    public function etiket_yaz()
    {
        $ids = request("product_ids");

        $ops =   $this->repo->getIrsaliyeproductByIds($ids);

        foreach($ops as $op)
        {
            if(!$op->irsaliye_no){
                $op->irsaliye_no = "B".str_pad($op->id,6,'0',STR_PAD_LEFT);
                $op->save();
            }
            //return PDF::setPaper('a6','landscape')->loadView('print.mesaj', $data)->stream();
        }
        return [
            'c0000',
            'KI501',
            'O0220',
            'f234',
            'KW0278',
            'KI7',
            'V0',
            'L',
            'H12',
            'PC',
            'A2',
            'D11',
            '1a4205100840083B000313-26',
            '121100000260129ACIKLAMA',
            '121100000390129URUN KODU 150X200',
            '121100000520129CARI ADI',
            '1a620480020001112345',
            '141100000830011150',
            '141100001040011JUNGLE',
            '191100200710128B000313-26',
            '191100200040031JNG150140080',
            '^01',
            'Q0001',
            ':0001',
            'E',
        ];
    }

    public function barkod_yaz()
    {
        $ids = request("product_ids");
        $row = request("row")?:1;
        $column = request("column")?:0;
        $ilk_baski = ($row-1)*3+$column;

        $ops =   $this->repo->getIrsaliyeproductByIds($ids);

        foreach($ops as $op)
        {
            if(!$op->irsaliye_no){
                $op->irsaliye_no = "B".str_pad($op->id,6,'0',STR_PAD_LEFT);
                $op->save();
            }
            //return PDF::setPaper('a6','landscape')->loadView('print.mesaj', $data)->stream();
        }
        return view("print.barkod",compact("ops","ilk_baski"));
    }

    public function barkod_oku()
    {
        $barkod = str_replace("*","-",request("barkod"));
        $b = explode("-",$barkod);
        $irsaliye_no = $b[0];
        if(!isset($b[1]))
        {
            abort(403);
        }

        $adet = $b[1];

        return $this->repo->barkodOkundu($irsaliye_no,$adet);
    }

    public function irs_no_json(Request $request)
    {
        $order_id = $request->input("id");

        $type = $request->input("type");

        $irsaliye =  $this->repo->getById($order_id);

        $number =  Number::where("sube_id",1)
                        ->where("type","irsaliye")
                        ->first();

        return $number ? $number->no : '00001';
    }

    public function top_degistir()
    {
        $ops =   $this->repo->getIrsaliyeproductByIds(request("checkedPrd"));
        foreach($ops as $op)
        {
            \Log::info("Top Değişikliği",[$op->id,$op->irsaliye_no,$op->product_top,request("yeni_top")]);
            $op->product_top = request("yeni_top");
            $op->save();
        }
    }
}
