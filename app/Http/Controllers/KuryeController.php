<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KuryeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Sube;
use App\Kurye;

class KuryeController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        return view("kurye.index",compact("user"));
    }

    /**
     * get kurye records
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->user();
        return Kurye::where(function($query)use($user){
            if($user->sube_id)
            {
                $query->where("sube_id",$user->sube_id);
            }
            if(request("sube_id"))
            {
                $query->where("sube_id",request("sube_id"));
            }
            if(request("aktif"))
            {
                $query->where("aktif",request("aktif"));
            }
            if(request("name"))
            {
                $query->where("name",'like','%'.request("name").'%');
            }
            if(request("gsm"))
            {
                $query->where("gsm",'like','%'.request("gsm").'%');
            }
            if(request("tel"))
            {
                $query->where("tel",'like','%'.request("tel").'%');
            }
            if(request("firma"))
            {
                $query->where("firma",'like','%'.request("firma").'%');
            }
            if(request("email"))
            {
                $query->where("email",'like','%'.request("email").'%');
            }
            if(request("username"))
            {
                $query->where("username",'like','%'.request("username").'%');
            }
        })
        ->get([
            "id",
            "sube_id",
            "name",
            "gsm",
            "tel",
            "firma",
            "email",
            "username",
            "aktif"
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\KuryeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KuryeRequest $request)
    {
        $sube_id = $request->input("sube_id");
        $sube = Sube::find($sube_id);
        $kurye = new Kurye;
        $kurye->sube_id  = $sube_id;
        $kurye->il_id  = $sube->il_id;
        $kurye->name = $request->input("name");
        $kurye->gsm = $request->input("gsm");
        $kurye->tel = "";
        $kurye->firma = "";
        $kurye->email = "";
        $kurye->save();
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kurye = Kurye::findOrFail($id);
        $subeler = Sube::where("uretim",1)->lists("subeadi","id");
        $iller = config("iller");

        return view("kurye.show",compact("kurye","subeler","iller"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kurye = Kurye::findOrFail($id);
        $subeler = Sube::where("uretim",1)->lists("subeadi","id");
        $iller = config("iller");

        return view("kurye.edit",compact("kurye","subeler","iller"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\KuryeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kurye = Kurye::findOrFail($id);

        $input = $request->except("id","password");
        if($request->has("aktif"))
        {
            $input["aktif"] = $request->input("aktif");
        }
        else
        {
            $input["aktif"] = 0;
        }

        $kurye->update($input);

        if(request("password"))
        {
            $kurye->password = bcrypt(request("password"));
            $kurye->save();
        }

        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kurye = Kurye::findOrFail($id);
        $kurye->delete();

        return redirect()->route("kurye.index")->with("success","Silme işlemi başarılı");

    }
}
