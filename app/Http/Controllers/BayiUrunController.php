<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productimage;
use App\Orderproduct;
use App\Customer;
use App\Irsaliyetra;

class BayiUrunController extends Controller
{
    public function __construct()
    {
        $this->middleware('ifnotbayi');
    }

    public function index()
    {
        $user = auth()->guard("bayi")->user();

        $images = Productimage::select("id","customer_id","name","filename","status")
        ->where("name","<>","")
        ->where(function($query)use($user){
            if($user->customer_id)
            {
                $query->where("customer_id",$user->customer_id);
            }
        })->get();

        $customers = Customer::select("id","name")
            ->where(function($query)use($user,$images){
                if($user->customer_id)
                {
                    $query->whereIn("id",$images->pluck("customer_id"));
                }
            })
            ->where("parent_id",0)
            ->pluck("name","id")
            ->all();

        $customers = array_unique($customers);

        return view("urun.bayi",compact("images","user","customers"));
    }

    /**
     * get cari records
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->guard("bayi")->user();

        return Productimage::select("id","name","filename","customer_id","status")
        ->where(function($query)use($user){
            if($user->customer_id)
            {
                $query->where("customer_id",$user->customer_id);
            }
            if(request()->has("status"))
            {
                $query->where("status",request("status"));
            }
            if(request("name"))
            {
                $query->where("name",'like','%'.request("name").'%');
            }
            if(request("filename"))
            {
                $query->where("filename",'like','%'.request("filename").'%');
            }

        })
        ->get();

    }

    public function upload($image_id)
    {
        $name = "";
        $product_image = null;
        if($image_id != 'yeni'){
            $product_image = Productimage::findOrFail($image_id);
        }

        if(request()->file('file'))
        {
            $image = request()->file('file');

            $name = time().$image->getClientOriginalName();
            $image->storeAs('/public/images/products/org',$name);
            $img = \Image::make(request()->file('file'))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path("app/public/images/products/".$name));

            if($product_image)
            {
                if(\File::exists(storage_path('/app/public/images/products/org/'.basename($product_image->filename))))
                {
                    unlink(storage_path('/app/public/images/products/org/'.basename($product_image->filename)));
                    unlink(storage_path('/app/public/images/products/'.basename($product_image->filename)));
                }
                $product_image->filename = '/images/products/'.$name;
                $product_image->save();
            }

            return response()->json(['success' => 'ok','image'=>'/images/products/'.$name], 200);
        }
        return response()->json(['success' => false,'image'=>$name], 404);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            "name"=>"required",
            "filename"=>"required"
        ]);

        $image = new Productimage;
        $image->name  = request("name");
        $image->filename = request("filename");
        $image->product_id =  1;
        $image->customer_id = auth()->guard("bayi")->user()->customer_id?:1;
        $image->status = 1;
        $image->save();

        return 1;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $image = Productimage::findOrFail($id);

        $image->name = request("name");
        $image->customer_id = request("customer_id");
        $image->status = request("status");
        $image->save();

        return 1;
    }

    public function iptalet(Request $request)
    {
        $op_id = $request->input("op_id");
        $op = Orderproduct::find($op_id);

        if($op->status==0 AND !$op->irsaliye_no){
            $op->status = 6;
            $op->save();

            $ot = new Irsaliyetra();
			$ot->order_id = $op->order_id;
			$ot->op_id = $op->id;
			$ot->order_number = $op->irsaliyesi->order_number;
			$ot->tur = 'iade';
			$ot->link = 'setIptalEdildi';
			$ot->description = $op->product_name." İptal Edildi";
			$ot->user_type = 'App\\Bayi';
			$ot->user_id = auth()->guard("bayi")->id();
			$ot->kurye_id = 0;
			$ot->save();
            return 1;
        }

        return 0;
    }

    public function delete($id)
    {
        $product_image = Productimage::findOrFail($id);
        if($product_image)
        {
            if(\File::exists(storage_path('/app/public/images/products/org/'.basename($product_image->filename))))
            {
                unlink(storage_path('/app/public/images/products/org/'.basename($product_image->filename)));
                unlink(storage_path('/app/public/images/products/'.basename($product_image->filename)));
            }

            $product_image->delete();
        }
        return 1;
    }

}
