<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Number;
use App\Sube;
use Illuminate\Support\Facades\Auth;
use Event;
use Gate;


class FaturaController extends Controller
{
    protected $irsaliye;

    protected $selected = ["id","order_id","status","alici_adsoyad","stok_kodu","product_name","product_desc","product_sku","product_qty","cikis_tarihi","teslim_tarihi","teslim_saati","shipping","semt_adi","il_adi","kargo","created_at","ek","parent_id"];

    protected $selectedIrsaliye = [
        "id","order_number","gon_adsoyad","total","siptar","coupon_code","payment","payment_ok","mobil","created_at","siparisalan","kapida","bize_mesaj","fatura_no","shipping"
    ];

    public function __construct(Order $irsaliye)
    {
        $this->middleware("auth");
        $this->irsaliye = $irsaliye;
    }

    public function index()
    {
        $user = Auth::user();
        if(!$user->can("fatura-bakabilir"))
        {
            return redirect("/");
        }
        return view("fatura");
    }

    public function store()
    {
        $tarih1 = date("Y-m-d",strtotime(request()->input("tarih1",date("d-m-Y"))));
        $tarih2 = date("Y-m-d",strtotime(request()->input("tarih2",date("d-m-Y"))));
        $user = Auth::user();
        $limit = 5000;
        if(request()->input("tip","") == 'search')
        {
            $limit = 500;
        }
        $sube_id = $user->sube_id;
        $faturalar =  $this->irsaliye
            ->select($this->selectedIrsaliye)
            ->where("onay",">",0)
            ->where(function($query) use($sube_id){

                if(request("order_number"))
                {
                    $query->where("order_number","like","%".request()->input("order_number")."%");
                }
                if(request("gon_adsoyad"))
                {
                    $query->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                }

                if(request("coupon_code"))
                {
                    $query->where("coupon_code","like","%".request()->input("coupon_code")."%");
                }

                if(request("siparisalan"))
                {
                    $query->where("siparisalan","like","%".request()->input("siparisalan")."%");
                }
                if(request("fatura_no"))
                {
                    $query->where("fatura_no","like","%".request()->input("fatura_no")."%");
                }

                if(request("onay"))
                {
                    $query->where("onay",request()->input("onay"));
                }

                if(request("payment"))
                {
                    $query->where("payment",request()->input("payment"));
                }

                if(request("payment_ok"))
                {
                    $query->where("payment_ok",request()->input("payment_ok"));
                }
                if(request("mobil"))
                {
                    $query->where("mobil",request()->input("mobil"));
                }
                if($sube_id
                    OR request("status")
                    OR request("alici_adsoyad")
                    OR request("semt_adi")
                    OR request("ship")
                    OR request("teslim_saati")
                    OR request("teslim_tarihi")
                    OR request("product_name")
                    OR request("kurye_id")
                ){
                    $query->whereHas("orderproducts",function($q)use($sube_id){

                        if(request("status"))
                        {
                            $q->where("status",request()->input("status"));
                        }
                        if(request("kurye_id"))
                        {
                            $q->where("kurye_id",request()->input("kurye_id"));
                        }
                        if(request("alici_adsoyad"))
                        {
                            $q->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                        }
                        if(request("semt_adi"))
                        {
                            $q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                        }

                        if(request("ship"))
                        {
                            $q->where("ship","like","%".request()->input("ship")."%");
                        }

                        if(request("teslim_tarihi"))
                        {
                            $q->where("teslim_tarihi","like","%".request()->input("teslim_tarihi")."%");
                        }
                        if(request("teslim_saati"))
                        {
                            $q->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
                        }
                        if(request("product_name"))
                        {
                            $q->where("product_name","like","%".request()->input("product_name")."%");
                        }
                    });
                }

                if(request("iptaliade"))
                {
                    if(request("iptaliade") != 'true')
                    {
                        $query->whereNotIn("onay",[5,6]);
                    }
                }
                else
                {
                    $query->whereNotIn("onay",[5,6]);
                }
            })
            ->whereBetween("siptar",[$tarih1,$tarih2])
            ->orderBy("siptar","desc")
            ->with("orderproducts")
            ->take($limit)
            ->get();

            $fatura_no = $this->getFaturaNumber();
            return compact("faturalar","fatura_no");
    }

    public function print($id)
    {
        $fat_no = request()->input("fat_no","");

        $irsaliye =   $this->irsaliye->find($id);
        if(!$irsaliye)
        {
            die("İrsaliye bulunamadı..");
        }
        if(!$irsaliye->fatura_no)
        {
            //irsaliye indirim yüzdesi fix
            if($irsaliye->coupon_discount > 0 AND $irsaliye->discount_yuzde == 0)
            {
                $discount_yuzde = 0;
                $subtotal = 0;
                foreach ($irsaliye->orderproducts as $op) {
                    if($op->ek == 0){
                        $subtotal += $op->product_qty * $op->product_price;
                    }
                }
                $discount_yuzde = 100 * $irsaliye->coupon_discount / $subtotal;
                $irsaliye->discount_yuzde = $discount_yuzde;
            }

            $user = Auth::user();

            $number = Number::where('type','fatura')->where('sube_id',0)->first();

            if(!$number)
            {
                $number = Number::create([
                    "type"      => "fatura",
                    "sube_id"   => 0,
                    "no"        => "00001"
                ]);
            }

            if(!$fat_no)
            {
               $fat_no = $number->no;
            }

            $num = preg_replace("/[^0-9]/","", $fat_no);

            $str = preg_replace("/[^A-Za-z]/","", $fat_no);

            if(strlen($num)!=strlen($num+1))
            {
              $num1 = str_pad($num+1, strlen($num),"0",STR_PAD_LEFT);
            }
            else
            {
              $num1 = $num +1;
            }

            $number->no = $str.$num1;

            $number->save();

            $irsaliye->fatura_no = $fat_no;

            $irsaliye->save();
        }
        return view("print.fatura",compact("irsaliye"));
    }
    public function rawprint($id)
    {
        $fat_no = request()->input("fat_no","");
        $irsaliye =   $this->irsaliye->find($id);
        if(!$irsaliye)
        {
            die("İrsaliye bulunamadı..");
        }
        //irsaliye indirim yüzdesi fix
        if($irsaliye->coupon_discount > 0 AND $irsaliye->discount_yuzde == 0)
        {
            $discount_yuzde = 0;
            $subtotal = 0;
            foreach ($irsaliye->orderproducts as $op) {
                if($op->ek == 0){
                    $subtotal += $op->product_qty * $op->product_price;
                }
            }
            $discount_yuzde = 100 * $irsaliye->coupon_discount / $subtotal;
            $irsaliye->discount_yuzde = $discount_yuzde;
        }

        $user = Auth::user();

        $number = Number::where('type','fatura')->where('sube_id',0)->first();

        if(!$number)
        {
            $number = Number::create([
                "type"      => "fatura",
                "sube_id"   => 0,
                "no"        => "00001"
            ]);
        }

        if(!$fat_no)
        {
           $fat_no = $number->no;
        }

        $num = preg_replace("/[^0-9]/","", $fat_no);

        $str = preg_replace("/[^A-Za-z]/","", $fat_no);

        if(strlen($num)!=strlen($num+1))
        {
          $num1 = str_pad($num+1, strlen($num),"0",STR_PAD_LEFT);
        }
        else
        {
          $num1 = $num +1;
        }

        $number->no = $str.$num1;

        $number->save();

        $irsaliye->fatura_no = $fat_no;

        $irsaliye->save();

        $data = [];
        $data["in"]= $irsaliye->order_number;
        $data["a1"]="";
        $data["a2"]="";
        $data["a3"]="";
        $data["a4"]="";
        $data["a5"]="";
        $data["a7"]="";
        $data["b1"]=($irsaliye->vergi_dairesi ? $irsaliye->vergi_dairesi : ".")." ".($irsaliye->vergi_no ? $irsaliye->vergi_no : $irsaliye->kimlik_no);
        $data["b2"]=" ";
        $data["c1"]="";
        $data["c2"]="";
        $data["c3"]="";
        $data["c4"]="";
        $data["d1"]=date("d.m.Y",strtotime($irsaliye->siptar));
        $data["d2"]=date("d.m.Y",strtotime($irsaliye->siptar));
        $data["d3"]=$irsaliye->irsaliye_no;
        $data["d4"]=$irsaliye->order_number;

        for($i=1;$i<=14;$i++)
        {
            $data["e".$i]= "";
            $data["f".$i]= "";
            $data["g".$i]= "";
            $data["h".$i]= "";
            $data["i".$i]= "";
            $data["j".$i]= "";
            $data["k".$i]= "";
            $data["l".$i]= "";
            $data["n".$i]= "";
            $data["o".$i]= "";
        }


        $search = array( "\n","\t","\r");

        $cariadi =str_split(str_replace($search, "",($irsaliye->fatura_unvan ? $irsaliye->fatura_unvan : $irsaliye->gon_adsoyad)));
        for($i=0;$i<=count($cariadi);$i++)
        {
            if($i<=40)
            {
                $data["a1"].=isset($cariadi[$i]) ? $cariadi[$i] : "";
            }
            else
            {
                $data["a2"].=isset($cariadi[$i]) ? $cariadi[$i] : "";
            }
        }

        $adres1 = str_split(str_replace($search, "",($irsaliye->fatura_adres ? $irsaliye->fatura_adres : $irsaliye->gon_adres)." ".($irsaliye->fatura_adres ? '' : $irsaliye->gon_cep)));

        for($i=0;$i<=count($adres1);$i++)
        {
            if($i<=40)
            {
                $data["a3"].=isset($adres1[$i]) ? $adres1[$i] : "";
            }
            else
            {
                $data["a4"].=isset($adres1[$i]) ? $adres1[$i] : "";
            }
        }

        $i=1;

        $matrah1=0;
        $matrah8=0;
        $matrah18=0;
        $kdv8 = 0;
        $kdv18 = 0;

        foreach($irsaliye->orderproducts as $op)
        {
            $data["o".$i] = substr($op->product_sku,0,10);
            $data["e".$i] = substr($op->product_name,0,30);
            $data["g".$i] = "AD";
            $data["h".$i] = 1*$op->product_qty;
            $data["l".$i] = number_format($op->product_price,2);

            $discount = $op->ek == 1 ? 1 :  (100-$irsaliye->discount_yuzde)/100;
            $kdv8+= ($op->product_tax==8) ? 0.08 * $op->product_price*$op->product_qty * $discount : 0;
            $kdv18+= ($op->product_tax==18) ? 0.18 * $op->product_price*$op->product_qty * $discount : 0;
            $matrah8 += ($op->product_tax==8) ? $op->product_price*$op->product_qty * $discount : 0;
            $matrah18 += ($op->product_tax==18) ? $op->product_price*$op->product_qty * $discount : 0;
            $matrah18 += $irsaliye->shipping / 1.18;
            $i++;
        }


        $i=7;
        if($irsaliye->coupon_discount>0)
        {
            $data["g".$i] = 'isk.';
            $data["h".$i] = ' ';
            $data["k".$i] = '';
            $data["l".$i] = 'isk -'.number_format($irsaliye->coupon_discount,2);
            $i++;
        }
        $data["g".$i] = '';
        $data["h".$i] = '';
        $data["k".$i] = '';
        $data["l".$i] = number_format($irsaliye->subtotal,2);
        $i++;

        if($irsaliye->shipping>0)
        {
            $data["g".$i] = 'Teslim.';
            $data["h".$i] = 'ücr';
            $data["k".$i] = 'eti';
            $data["l".$i] = number_format($irsaliye->shipping/1.18,2);
            $i++;
        }

        $data["g".$i] = '    KDV';
        $data["h".$i] = '';
        $data["k".$i] = '';
        $data["l".$i] = '(%8)'.number_format($kdv8,2);
        $i++;
        $data["g".$i] = '    KDV';
        $data["h".$i] = '';
        $data["k".$i] = '';
        $data["l".$i] = '(%18)'.number_format($kdv18+($irsaliye->shipping-$irsaliye->shipping/1.18),2);

        $i++;
        $data["g".$i] = '';
        $data["h".$i] = 'Yeku';
        $data["k".$i] = 'n    ';
        $data["l".$i] = number_format($irsaliye->total+$irsaliye->shipping,2);


        $data["e7"]="Matrah(%8)  ".number_format($matrah8,2);
        $data["e8"]="Matrah (%18) ".number_format($matrah18,2);
        $data["e9"]="YALNIZ ".money_tr_string($irsaliye->total+$irsaliye->shipping);
        $data["n1"]=date("d.m.Y",strtotime($irsaliye->siptar));;
        $data["n2"]="";
        $data["n3"]="";
        $data["n4"]="";

        //$dizayn = 'faturaa42';

        //$view = replace_tr($view);
        //echo "<pre>";
        $view = view("print.rawfatura",$data);

        return ["fat_no"=>$fat_no,"fat"=>[$view->render()]];
    }

    private function getFaturaNumber()
    {
        $number = Number::where('type','fatura')->where('sube_id',0)->first();
        if($number)
        {
            return  $number->no;
        }
        return "";
    }
}
