<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Orderproduct;
use Mail;

class TakipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $irsaliye = null;
        $mesaj = null;
        $durumlar = config("ayar.durumlar");

        return view("takip.index",compact("irsaliye","mesaj","durumlar"));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $order_number =trim($request->input("order_number"));

        $irsaliye = Order::where("order_number",$order_number)->with("irsaliyetras")->first();

        $mesaj = "";

        $irt_durumlar = config("ayar.irt_durumlar");

        return view("takip.store",compact("irsaliye","mesaj","irt_durumlar"));
    }

    public function bayi(Request $request)
    {
        $op_id =trim($request->input("op_id"));

        $op = Orderproduct::where("id",$op_id)->with("irsaliyetras")->first();

        $mesaj = "";

        $durumlar = [
            0   =>  'Yeni Sipariş',
            1   =>  'Hazırlıkta',
            2   =>  'Üretim Aşaması',
            8   =>  'Üretim Aşaması',
            9   =>  'Üretim Aşaması',
            7   =>  'Üretim Aşaması',
            3   =>  'Hazırlandı',
            4   =>  'Teslim Edildi',
            5   =>  'Üretim Aşaması',
            6   =>  'İptal Edildi',
            10  =>  'Üretim Aşaması',
        ];

        return view("takip.bayi",compact("op","mesaj","durumlar"));
    }

    public function testorder($id)
    {

        $order = Order::with("orderproducts")->find($id);

        if($order)
        {
            if(filter_var($order->gon_email, FILTER_VALIDATE_EMAIL))
            {
                /*Mail::queue('emails.order.index', ["order"=>$order->toArray(),"link"=>$_ENV['CDN']], function($m) use($order)
                {
                    $m->subject("Jungle.com ".$order->order_number." nolu siparişiniz");
                    //$m->to($order->gon_email);
                    //$m->bcc("callcenter@Jungle.com");
                    $m->to("n.ermumcu@nurgida.com.tr");
                });*/
            }
            else
            {
                echo $order->gon_email ." is not valid";
            }

            return view("emails.order.email",["order"=>$order->toArray(),"link"=>$_ENV['CDN']]);
        }
    }

    public function testmailview($id)
    {
        $order = Order::with("orderproducts")->find($id)->toArray();

        return view("emails.order.email",compact("order"));
    }

}
