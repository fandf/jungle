<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Number;
use App\Sube;
use Illuminate\Support\Facades\Auth;
use Event;
use Gate;
use Uuid;

class LogoController extends Controller
{
    protected $irsaliye;

    protected $selected = ["id","order_id","status","alici_adsoyad","stok_kodu","product_name","product_desc","product_sku","product_qty","cikis_tarihi","teslim_tarihi","teslim_saati","shipping","semt_adi","il_adi","ship","created_at","ek","parent_id"];

    protected $selectedIrsaliye = [
        "id","order_number","gon_adsoyad","total","siptar","coupon_code","payment","payment_ok","mobil","created_at","siparisalan","kapida","bize_mesaj","fatura_no","shipping"
    ];
    public function __construct(Irsaliye $irsaliye)
    {
        $this->middleware("auth");
        $this->irsaliye = $irsaliye;
    }
    // HENÜZ TAMAMLANMADI TASLAK
    public function show($tarih)
    {
        $tarih = date("Y-m-d",strtotime($tarih));
        $irsaliyeler =  $this->irsaliye->where("siptar",$tarih)->get();
        if(!$irsaliyeler)
        {
            die("İrsaliye bulunamadı..");
        }
        foreach($irsaliyeler as $irsaliye)
        {
            if(!$irsaliye->fatura_no)
            {
                //irsaliye indirim yüzdesi fix
                if($irsaliye->coupon_discount > 0 AND $irsaliye->discount_yuzde == 0)
                {
                    $discount_yuzde = 0;
                    $subtotal = 0;
                    foreach ($irsaliye->orderproducts as $op) {
                        if($op->ek == 0){
                            $subtotal += $op->product_qty * $op->product_price;
                        }
                    }
                    $discount_yuzde = 100 * $irsaliye->coupon_discount / $subtotal;
                    $irsaliye->discount_yuzde = $discount_yuzde;
                }

                $user = Auth::user();
                $number = Number::where('type','fatura')->where('sube_id',0)->first();
                if(!$number)
                {
                    $number = Number::create([
                        "type"      => "fatura",
                        "sube_id"   => 0,
                        "no"        => "00001"
                    ]);
                }
                if(!$fat_no)
                {
                   $fat_no = $number->no;
                }
                $num = preg_replace("/[^0-9]/","", $fat_no);
                $str = preg_replace("/[^A-Za-z]/","", $fat_no);

                if(strlen($num)!=strlen($num+1))
                {
                  $num1 = str_pad($num+1, strlen($num),"0",STR_PAD_LEFT);
                }
                else
                {
                  $num1 = $num +1;
                }
                $number->no = $str.$num1;
                $number->save();
                $irsaliye->fatura_no = $fat_no;
                $irsaliye->save();
            }
        }
        /*
        if(!$irsaliye->uuid)
        {
            $irsaliye->uuid = Uuid::generate();
            $irsaliye->save();
        }*/
        $view = view("logo.xml",compact("irsaliyeler"));
        return response($view->render())
        ->header('Content-type', 'text/xml')
        ->header('Content-Disposition', 'attachment; filename="'.$tarih.'.xml"');

    }

    private function getFaturaNumber()
    {
        $number = Number::where('type','fatura')->where('sube_id',0)->first();
        if($number)
        {
            return  $number->no;
        }
        return "";
    }
}

/***
<?xml version="1.0" encoding="ISO-8859-9"?>
<SALES_ORDERS>
  <ORDER_SLIP DBOP="INS" >
    <NUMBER>~</NUMBER>
    <DATE>10.04.2017</DATE>
    <TIME>171191089</TIME>
    <ARP_CODE>CARI.02</ARP_CODE>
    <RC_RATE>1</RC_RATE>
    <ORDER_STATUS>1</ORDER_STATUS>
    <SALESMAN_CODE>1</SALESMAN_CODE>
    <CURRSEL_TOTAL>1</CURRSEL_TOTAL>
    <DATA_SITEID>1</DATA_SITEID>
    <TRANSACTIONS>
      <TRANSACTION>
        <TYPE>0</TYPE>
        <MASTER_CODE>MALZEME.02</MASTER_CODE>
        <QUANTITY>1</QUANTITY>
        <PRICE>1000</PRICE>
        <VAT_RATE>18</VAT_RATE>
        <UNIT_CODE>ADET</UNIT_CODE>
        <UNIT_CONV1>1</UNIT_CONV1>
        <UNIT_CONV2>1</UNIT_CONV2>
        <ORDER_RESERVE>1</ORDER_RESERVE>
        <DUE_DATE>10.04.2017</DUE_DATE>
        <CURR_PRICE>160</CURR_PRICE>
        <PC_PRICE>1000</PC_PRICE>
        <RC_XRATE>1</RC_XRATE>
        <SOURCE_WH>1</SOURCE_WH>
        <SOURCE_COST_GRP>1</SOURCE_COST_GRP>
        <DATA_SITEID>1</DATA_SITEID>
        <SALESMAN_CODE>1</SALESMAN_CODE>
        <AFFECT_RISK>1</AFFECT_RISK>
        <EDT_PRICE>1000</EDT_PRICE>
        <EDT_CURR>160</EDT_CURR>
        <ORG_DUE_DATE>10.04.2017</ORG_DUE_DATE>
        <ORG_QUANTITY>1</ORG_QUANTITY>
        <ORG_PRICE>1000</ORG_PRICE>
        <RESERVE_DATE>10.04.2017</RESERVE_DATE>
        <RESERVE_AMOUNT>1</RESERVE_AMOUNT>
      </TRANSACTION>
    </TRANSACTIONS>
  </ORDER_SLIP>
</SALES_ORDERS>
*/
