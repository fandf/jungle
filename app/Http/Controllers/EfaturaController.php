<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Number;
use App\Sube;
use Illuminate\Support\Facades\Auth;
use Event;
use Gate;
use Uuid;


class EfaturaController extends Controller
{
    protected $irsaliye;

    protected $selected = ["id","order_id","status","alici_adsoyad","stok_kodu","product_name","product_desc","product_sku","product_qty","cikis_tarihi","teslim_tarihi","teslim_saati","shipping","semt_adi","il_adi","kargo","created_at","ek","parent_id"];

    protected $selectedIrsaliye = [
        "id","order_number","gon_adsoyad","total","siptar","coupon_code","payment","payment_ok","mobil","created_at","siparisalan","kapida","bize_mesaj","fatura_no","shipping"
    ];

    public function __construct(Order $irsaliye)
    {
        $this->middleware("auth");
        $this->irsaliye = $irsaliye;
    }

    public function store($id)
    {
        $fat_no = request("fat_no")?:"";

        $irsaliye =   $this->irsaliye->find($id);
        if(!$irsaliye)
        {
            die("İrsaliye bulunamadı..");
        }
        if(!$irsaliye->fatura_no)
        {

            //irsaliye indirim yüzdesi fix
            if($irsaliye->coupon_discount > 0 AND $irsaliye->discount_yuzde == 0)
            {
                $discount_yuzde = 0;
                $subtotal = 0;
                foreach ($irsaliye->orderproducts as $op) {
                    if($op->ek == 0){
                        $subtotal += $op->product_qty * $op->product_price;
                    }
                }
                $discount_yuzde = 100 * $irsaliye->coupon_discount / $subtotal;
                $irsaliye->discount_yuzde = $discount_yuzde;
            }

            $user = Auth::user();
            $number = Number::where('type','fatura')->where('sube_id',0)->first();
            if(!$number)
            {
                $number = Number::create([
                    "type"      => "fatura",
                    "sube_id"   => 0,
                    "no"        => "00001"
                ]);
            }

            if(!$fat_no OR $fat_no=='null')
            {
               $fat_no = $number->no;
            }
            $num = preg_replace("/[^0-9]/","", $fat_no);
            $str = preg_replace("/[^A-Za-z]/","", $fat_no);

            if(strlen($num)!=strlen($num+1))
            {
              $num1 = str_pad($num+1, strlen($num),"0",STR_PAD_LEFT);
            }
            else
            {
              $num1 = $num +1;
            }
            $number->no = $str.$num1;
            $number->save();
            $irsaliye->fatura_no = $fat_no;
            $irsaliye->save();

        }
        if(!$irsaliye->uuid)
        {
            $irsaliye->uuid = Uuid::generate();
            $irsaliye->save();
        }
        $view = view("efatura.ubl",compact("irsaliye"));
        return response($view->render())
        ->header('Content-type', 'text/xml')
        ->header('Content-Disposition', 'attachment; filename="'.$fat_no.'.xml"');

    }

    private function getFaturaNumber()
    {
        $number = Number::where('type','fatura')->where('sube_id',0)->first();
        if($number)
        {
            return  $number->no;
        }
        return "";
    }
}
