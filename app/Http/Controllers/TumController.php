<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Kurye;
use App\Number;
use App\Sube;
use App\Top;
use Excel;

class TumController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware('auth');
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tum');
    }

    public function store()
    {
        $user = Auth::user();
        $tarih1 = request()->has("tarih1") ? date("Y-m-d",strtotime(request()->input("tarih1"))) : date("Y-m-d");
        $tarih2 = request()->has("tarih2") ? date("Y-m-d",strtotime(request()->input("tarih2"))) : date("Y-m-d");
        $tip = request()->input("tip","cikis_tarihi");
        $sube_id = $user->sube_id ?: 1;
        $irsaliye_no = $this->repo->getIrsaliyeNumber($sube_id);
        $limit = 10000;
        return ["tum"=>$this->repo->getAllWithFilter($tarih1,$tarih2,$tip,$limit)
            ,"irsaliye_no"=>$irsaliye_no
        ];
    }


    public function search(Request $request)
    {
        $user = Auth::user();
        $tarih1 = date("Y-m-d",strtotime("-6 months"));
        $tarih2 = date("Y")."-12-31";
        $tip = "";
        $limit = 500;

        if(!(request("fatura_unvan") OR request("order_number") OR request("top_number") OR request("mevcut_urun"))){
            return [];
        }

        return $this->repo->getAllWithFilter($tarih1,$tarih2,$tip,$limit);
    }

    public function excel()
    {
        $ids = explode(",",request("product_ids"));
        $ops =   $this->repo->getIrsaliyeproductByIds($ids);

        Excel::create("RAPOR", function($excel) use($ops) {

            $excel->sheet('Sayfa 1', function($sheet) use ($ops) {
                $sheet->loadView('print.excel',compact("ops"));
                $sheet->freezeFirstRow();

            });
        })->export('xlsx');
    }
}
