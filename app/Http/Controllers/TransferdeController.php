<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Sube;
use App\Top;

class TransferdeController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware('auth');
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $top_names = Top::where(function($query){
            $query->where("created_at",">",date("Y-m-d",strtotime("-3 months")));
            $query->orWhere("aktif",1);
        })
        ->pluck("name","id")->all();
        return view('transfer',compact("top_names"));
    }

    public function store()
    {
        $user = Auth::user();
        return $this->repo->getTransferde($user);
    }


    public function transfere(Request $request)
    {
        $user = Auth::user();

        foreach($request->input("checkedIrs") as $op_id)
        {
            $this->repo->setTransfer($op_id,$user);
        }
        return 1;
    }

    public function panel()
    {
        $top_names = Top::where(function($query){
            $query->where("created_at",">",date("Y-m-d",strtotime("-6 months")));
            $query->orWhere("aktif",1);
        })
        ->pluck("name","id")->all();

        return view("transfer.panel",compact("top_names"));
    }

    public function urunler()
    {
        $top = Top::where("name",request("top_number"))->firstOrFail();
        $user = Auth::user();

        return ["top_image"=>$top->image,"id"=>$top->id];
        //return ["top_image"=>$top->image,"urunler"=>$this->repo->getTopUrunler($top)];
    }

    public function top_transfere()
    {
        $top = Top::where("name",request("top_number"))->firstOrFail();
        $user = Auth::user();
        $this->repo->setTransferByTop($top,$user);
        return 1;
    }
}
