<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Sube;
use App\Customer;

class CariController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $customers = $this->get();
        return view("cari.index",compact("customers","user"));
    }

    /**
     * get cari records
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->user();

        return Customer::select(
                "id",
                "firmakodu",
                "name",
                "company_name",
                "work_phone",
                "mobile_phone",
                "email",
                "active"
            )->where(function($query)use($user){

            if(request()->has("active"))
            {
                $query->where("active",request("active"));
            }
            if(request("firmakodu"))
            {
                $query->where("firmakodu",'like','%'.request("firmakodu").'%');
            }
            if(request("name"))
            {
                $query->where("name",'like','%'.request("name").'%');
            }
            if(request("company_name"))
            {
                $query->where("company_name",'like','%'.request("company_name").'%');
            }
            if(request("mobile_phone"))
            {
                $query->where("mobile_phone",'like','%'.request("mobile_phone").'%');
            }
            if(request("work_phone"))
            {
                $query->where("work_phone",'like','%'.request("work_phone").'%');
            }
            if(request("unvan"))
            {
                $query->where("unvan",'like','%'.request("unvan").'%');
            }
            if(request("email"))
            {
                $query->where("email",'like','%'.request("email").'%');
            }
        })
        ->orderBy("id","desc")
        ->paginate(50);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "company_name" => "required"

        ]);

        $cari = new Customer;
        $cari->name  = mb_strtoupper($request->input("name"),"UTF-8");
        $f =  explode(" ",$cari->name);
        $cari->firmakodu = $f[0].".".($f[1] ?? "");
        if(Customer::where("firmakodu",$cari->firmakodu)->first()){
            $cari->firmakodu = $cari->firmakodu."-".rand(100,999);
        }
        $cari->company_name = mb_strtoupper($request->input("company_name","UTF-8"));
        $cari->work_phone = $request->input("work_phone")?:"";
        $cari->mobile_phone = $request->input("mobile_phone")?:"";
        $cari->email = $request->input("email")?:"";
        $cari->address = mb_strtoupper($request->input("address"),"UTF-8")?:"";
        $cari->city = mb_strtoupper($request->input("city"),"UTF-8")?:"";
        $cari->semt = mb_strtoupper($request->input("semt"),"UTF-8")?:"";
        $cari->active = 1;
        $cari->save();

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);

        $iller = config("iller");

        return view("cari.show",compact("customer","iller"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        $iller = config("iller");

        return view("cari.edit",compact("customer","iller"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cari = Customer::findOrFail($id);

        $cari->name  = mb_strtoupper($request->input("name"),"UTF-8");
        $cari->company_name = mb_strtoupper($request->input("company_name"),"UTF-8");
        $cari->work_phone = $request->input("work_phone")?:"";
        $cari->mobile_phone = $request->input("mobile_phone")?:"";
        $cari->email = $request->input("email")?:"";
        $cari->active = $request->input("active") ? 1 : 0;
        $cari->save();

        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kurye = Customer::findOrFail($id);
        $kurye->delete();

        return redirect()->route("kurye.index")->with("success","Silme işlemi başarılı");

    }
}
