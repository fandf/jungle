<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sube;
use App\Product;
use App\Customer;
use App\Order;
use App\Orderproduct;
use App\Productimage;
use App\Irsaliyetra;
use App\Stock;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use DB;

class OrderController extends Controller
{
	protected $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
        $this->middleware(["auth","can:siparis-onay"]);
	}
    /**
     * Create of the resource.
     *
     * @return Response
     */
    public function create()
    {
    	$user = auth()->user();
        $kanallar = config("ayar.kanallar");
        $products = Product::select("sku","name","brand","options","tops")->get()->toArray();

        $customers = Customer::select("id","name","company_name","mobile_phone","email","address","city","semt","vergi_dairesi","vergi_no")
            ->where("active",1)
            ->get();

        $customer = NULL;
        if(request("cari")){
            $customer = $customers->where("id",request("cari"))->first();
        }
        if(!$customer){
            $customer = $customers[0];
        }
        $images = Productimage::selectRaw("id,name,filename as src")
        ->where("name","<>","")
        ->where(function($query)use($user){
            if($user->customer_id)
            {
                $query->whereIn("customer_id",explode(",",$user->customer_id));
            }
            else {
                $query->where("id",">",0);
            }
        })
        ->where("status",1)
        ->orderBy("name")
        ->get();
        return view("orders.create",compact("user","kanallar","brands","products","customers","customer","images"));
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function show($id)
    {
    	return $this->order->with("orderproducts")->find($id);
    }


    public function store(Request $request)
    {
        $this->validate($request,[
           // "order.gon_adsoyad" =>  "required",
           // "order.gon_cep" =>  "required",
            "order.fatura_unvan" =>  "required",
            "order.fatura_adres" =>  "required",
           // "order.vergi_dairesi" =>  "required",
           // "order.vergi_no" =>  "required",
            "order.siparisalan" =>  "required",
           // "order.alici_firma" =>  "required",
           // "order.alici_adsoyad" =>  "required",
           // "order.alici_telefon" =>  "required",
           // "order.il_adi" =>  "required",
           // "order.semt_adi" =>  "required",
           // "order.alici_adres" =>  "required",
            "orderproducts.*.product_top" =>"required",
            "orderproducts.*.product_sku" =>"required",
            "orderproducts.*.product_width" =>"required",
            "orderproducts.*.product_height" =>"required",
            "orderproducts.*.product_name" =>"required",
            "orderproducts.*.product_qty" =>"required",
            //"orderproducts.*.product_options" =>"required",
            "orderproducts.*.product_image" =>"required",
            // "orderproducts.*.product_price" =>"required",
            // "orderproducts.*.product_tax" =>"required",
        ]);

        $order = new Order;
        $order->order_number = $this->create_order_number();
        $order->user_id = auth()->id();
        $order->sube_kodu = 0;
        $order->customer_id = 0;
        $order->tax = 0;
        $order->subtotal = 0;
        $order->total = 0;
        $order->customer_id = $request->input("customer.id");
        $order->gon_adsoyad = $request->input("order.gon_adsoyad");
        $order->gon_cep = $request->input("order.gon_cep");
        $order->fatura_unvan = $request->input("order.fatura_unvan");
        $order->fatura_adres = $request->input("order.fatura_adres");
        $order->vergi_dairesi = $request->input("order.vergi_dairesi");
        $order->vergi_no = $request->input("order.vergi_no");
        $order->siparisalan = $request->input("order.siparisalan");
        $order->bize_mesaj = $request->input("order.bize_mesaj");
        $order->onay = 1;
        $order["siptar"] = request()->input("order.siptar") ? date("Y-m-d",strtotime(request()->input("order.siptar"))) : date("Y-m-d");
        $order->save();

        $order->order_number = "A".str_pad($order->id,5,"0",STR_PAD_LEFT);
        $order->save();

        $adet = 0;
        foreach($request->input("orderproducts") as $key=>$r)
        {
            $op = new Orderproduct();
            $op->order_id = $order->id;
            if($request->input("subcustomer.id")){
                $op->alici_customer_id = $request->input("subcustomer.id");
                $op->alici_firma = $request->input("order.alici_firma");
                $op->alici_adsoyad = $request->input("order.alici_adsoyad");
                $op->alici_telefon = $request->input("order.alici_telefon");
                $op->il_adi = $request->input("order.il_adi");
                $op->semt_adi = $request->input("order.semt_adi");
                $op->alici_adres = $request->input("order.alici_adres");
            }

            $op->product_sku = $request->input("orderproducts.".$key.".product_sku");
            $op->product_top = $request->input("orderproducts.".$key.".product_top");
            $op->product_width = $request->input("orderproducts.".$key.".product_width");
            $op->product_height = $request->input("orderproducts.".$key.".product_height");
            $op->product_name = $request->input("orderproducts.".$key.".product_name");
            $op->product_qty = $request->input("orderproducts.".$key.".product_qty");
            $op->product_options = $request->input("orderproducts.".$key.".product_options");
            $op->product_desc = $request->input("orderproducts.".$key.".product_desc");
            $op->product_image = $request->input("orderproducts.".$key.".product_image");
            $op->product_price = $request->input("orderproducts.".$key.".product_price") ?:0;
            $op->product_tax = $request->input("orderproducts.".$key.".product_tax") ?:8;
            $op->mevcut_urun = $request->input("orderproducts.".$key.".mevcut_urun") ? $request->input("orderproducts.".$key.".mevcut_urun")["name"] : "";
            $op->cikis_tarihi = request()->input("order.teslim_tarihi") ? date("Y-m-d",strtotime(request()->input("order.teslim_tarihi"))) : date("Y-m-d");
            $op->teslim_tarihi = request()->input("order.teslim_tarihi") ? date("Y-m-d",strtotime(request()->input("order.teslim_tarihi"))) : date("Y-m-d");
            $op->gon_gizli = $request->input("orderproducts.".$key.".gon_gizli") == true ? 1 : 0;
            $op->status = 0;
            $op->customer_id = $order->customer_id;
            $height = $op->product_height;
            $product_top = $op->product_top;

            if($op->product_width<$op->product_height AND $op->product_height<=$op->product_top)
            {
                $height = $op->product_width;
            }

            if($product_top == 150)
            {
                if($op->product_width == 75)
                {
                    $product_top = 75;
                    $height = $op->product_height;
                }
                elseif($op->product_height==75)
                {
                    $product_top = 75;
                    $height = $op->product_width;
                }
            }
            elseif($product_top == 160)
            {
                if($op->product_width == 80)
                {
                    $product_top = 80;
                    $height = $op->product_height;
                }
                elseif($op->product_height==80)
                {
                    $product_top = 80;
                    $height = $op->product_width;
                }
            }

            $stock = Stock::where("product_sku",$op->product_sku)
                ->where("product_top",$product_top)
                ->where("product_height",$height)
                ->first();

            if($stock){
                $op->stok_kodu = $stock->kod;
            }
            $op->save();

            $ot = new Irsaliyetra();
			$ot->order_id = $order->id;
			$ot->op_id = $op->id;
			$ot->order_number = $order->order_number;
			$ot->tur = "";
			$ot->link = 'setEklendi';
			$ot->description = "Sipariş Eklendi";
			$ot->user_id = auth()->id();
			$ot->user_type = 'App\\User';
			$ot->kurye_id = 0;
			$ot->save();
            $adet++;
        }
        if($adet>0){

            return ["success"=>"Sipariş Eklendi.."];
        }
        return ["success"=>false];
    }

    public function update(Request $request)
    {
        dd($request->all());
    }

    public function upload(Request $request)
    {
        $name = "";

        if($request->file('file'))
        {
          $image = $request->file('file');

          $name = time().$image->getClientOriginalName();
          $image->storeAs('/public/uploads/org',$name);
          $img = \Image::make($request->file('file'))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path("app/public/uploads/".$name));

            return response()->json(['success' => 'ok','image'=>$name], 200);
        }
        return response()->json(['success' => false,'image'=>$name], 404);
    }

    private function create_order_number()
    {
        return date("YmdHis");
    }
}
