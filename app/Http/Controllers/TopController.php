<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Top;

class TopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();

        $toplar = Top::where("aktif",1)
            ->orderBy("id","desc")
            ->take(100)
            ->get();

        return view("top.index",compact("toplar","user"));
    }

    /**
     * get cari records
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->user();

        return Top::where(function($query)use($user){

            if(request("aktif"))
            {
                $query->where("aktif",request("aktif"));
            }
            if(request("filename"))
            {
                $query->where("name",'like','%'.request("filename").'%');
            }

        })
        ->orderBy("id","desc")
        ->take(100)
        ->get();

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            "name"=>"required"
        ]);

        $top = new Top;
        $top->name  = request("name");
        $top->aktif = 1;
        $top->user_id = auth()->id();
        try {
            $top->save();
        } catch (\Exception $e) {
            abort(503);
        }

        return 1;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $top = Top::findOrFail($id);
        $top->name = request("name");
        $top->length  = str_replace(',','.',request("length"));
        $top->aktif = request("aktif");
        $top->save();

        return 1;
    }

    /* public function delete($id)
    {
        $top = Top::findOrFail($id);
        if($top)
        {
            $top->delete();
            return 1;
        }
        return 0;
    } */

    public function delete($id)
    {
        $top = Top::findOrFail($id);
        if($top)
        {
            if(\File::exists(storage_path('/app/public/images/top/'.basename($top->image))))
            {
                unlink(storage_path('/app/public/images/top/'.basename($top->image)));
            }
            $top->image = "";
            $top->save();
            return 1;
        }
        return 0;
    }

    public function upload($top_id)
    {
        $name = "";

        $top = Top::findOrFail($top_id);

        if(request()->file('file'))
        {
            $image = request()->file('file');

            $name = time().$image->getClientOriginalName();
            $image->storeAs('/public/images/top',$name);
            $top->image = '/images/top/'.$name;
            $top->save();

            return response()->json(['success' => 'ok','image'=>'/images/top/'.$name], 200);
        }
        return response()->json(['success' => false,'image'=>$name], 404);
    }

}
