<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Sube;

class HazirlikController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware(["auth","can:baski-hazirlik"]);
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hazirlik');
    }

    public function store()
    {
        $user = Auth::user();
        return $this->repo->getHazirlik($user);
    }


    public function hazirlandi(Request $request)
    {
        $user = Auth::user();
        foreach($request->input("checkedPrd") as $op_id)
        {
            $this->repo->setHazirlandi($op_id,$user);
        }
        return 1;
    }
}
