<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orderproduct;
use App\Jobs\SendTeslimSms;
use App\Jobs\SendTeslimEmail;
use Event;
use App\Irsaliyetra;

class GonderiController extends Controller
{
    public function __construct()
    {
        $this->middleware('ifnotkurye');
    }

    public function index()
    {
    	$user = auth()->guard("kurye")->user();
    	return view("gonderi.index",compact("user"));
    }
    public function store()
    {
    	$user = auth()->guard("kurye")->user();
    	return Orderproduct::
			whereIn("status",[3])
			->where("kurye_id",$user->id)
			->where("cikis_tarihi","<=",date("Y-m-d"))
			->where(function($query){
				if(request()->has("alici_telefon"))
				{
					$query->where("alici_telefon",'like','%'.request("alici_telefon").'%');
				}
				if(request()->has("alici_firma"))
				{
					$query->where("alici_firma",'like','%'.request("alici_firma").'%');
				}
				if(request()->has("alici_adsoyad"))
				{
					$query->where("alici_adsoyad",'like','%'.request("alici_adsoyad").'%');
				}

			})
			->orderBy("created_at")
			->whereHas("irsaliyesi",function($query){
				if(request()->has("gon_adsoyad"))
				{
					$query->where("gon_adsoyad","like",'%'.request("gon_adsoyad").'%');
				}
			})
			->with(["irsaliyesi"=>function($query){
				$query->select("id","order_number","gon_adsoyad");
			}])
			->get([
				"id",
				"alici_adsoyad",
				"alici_telefon",
				"alici_firma",
				"order_id",
				"irsaliye_no"
			]);
    }

    public function irsaliye_item($id)
    {
    	return Irsaliyeproduct::where("id",$id)->with(["irsaliyesi"=>function($query){
    		$query->select('id','order_number','gon_adsoyad','gon_cep','gon_sabit','gon_email','bize_mesaj','kurye_mesaj','payment_ok','kapida','total','shipping');
    	}])->first();
    }

    public function teslimet(Request $request)
    {
        $user = auth()->guard("kurye")->user();
        $op_id = $request->input("checkedIrs.0");

        $op = Irsaliyeproduct::findOrFail($op_id);
        $op->status = 4;
        $tur = 'teslim';
        $description = "Teslim Edildi";
        $op->save();

        $order_id = $op->irsaliyesi->id;
		$order_number = $op->irsaliyesi->order_number;

		dispatch(new SendTeslimSms(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad]));

		dispatch(new SendTeslimEmail(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad,"gon_email"=>$op->irsaliyesi->gon_email]));


        $ot = new Irsaliyetra();
        $ot->order_id = $order_id;
        $ot->order_number = $order_number;
        $ot->tur = $tur;
        $ot->link = 'setTeslimEdildi';
        $ot->description = $description;
        $ot->user_type = 'App\\Kurye';
        $ot->user_id = $user->id;
        $ot->kurye_id = $user->id;
        $ot->save();
        $tur = $op->kargo ? 'kargoda':'kuryede';

        Event::fire('TeslimEdildi', [['order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'tur'=>$tur]]);
        return 1;
        return 1;
    }
}
