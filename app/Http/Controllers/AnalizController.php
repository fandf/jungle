<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orderproduct;
use App\Customer;

class AnalizController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','can:analiz']);
    }

    public function index()
    {
		$user = auth()->user();
		$customers = Customer::pluck("company_name","id")->all();

        return view("analiz",compact("user","customers"));
    }

    public function store()
    {
        $ops =  Orderproduct::where(function($query){
				if(request("tip")=='cikis_tarihi' || request("tip") == 'teslim_tarihi')
				{
					$query->whereBetween(request("tip"),[date("Y-m-d",strtotime(request("tarih1"))),date("Y-m-d",strtotime(request("tarih2")))]);
				}
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }

				if(request("product_options"))
				{
					$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
                }
				if(request("product_width"))
                {
                	$query->where("product_width",request()->input("product_width"));
                }
				if(request("product_height"))
                {
                	$query->where("product_height",request()->input("product_height"));
                }
                if(request("kurye_id"))
                {
                	$query->where("kurye_id",request()->input("kurye_id"));
                }
                if(request("sms"))
                {
                	$query->where("sms",request()->input("sms"));
                }
                if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }

                if(request("status"))
                {
                	$query->where("status",request()->input("status"));
                }
                if(request("kargo"))
                {
                	$query->where("kargo","like","%".request()->input("kargo")."%");
				}
				if(request("top_id"))
				{
					$query->where("top_id",request()->input("top_id"));
				}
				if(request("product_name"))
				{
					$query->where("product_name",request()->input("product_name"));
				}
                if(request("iptaliade"))
                {
                	if(request()->input("iptaliade") != 'true')
                	{
                		$query->whereNotIn("status",[6]);
                	}
				}
				else
                {
                	$query->whereNotIn("status",[6]);
				}

				if(request("top_number"))
				{
					$query->whereHas("topu",function($query2){
						$query2->where("name",request("top_number"));
					});
				}

				if(request("only_top") OR auth()->id()==5) // ozdemir
				{
					$query->where("top_id",">",0);
				}


                //if query needs irsaliyesi
                if(request("order_number")
                	OR request("tip") == 'siptar'
                	OR request("gon_adsoyad")
                	OR request("fatura_unvan")
                	OR request("coupon_code")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("mobil")
                	OR request("siptar")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1){

                	   		if(request("tip")=='siptar')
							{
								$query1->whereBetween("siptar",[date("Y-m-d",strtotime(request("tarih1"))),date("Y-m-d",strtotime(request("tarih2")))]);
							}
							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("irsaliye_no","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("fatura_unvan"))
                	   		{
								$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			                    $query1->where("fatura_unvan","like","%".$fatura_unvan."%");
                	   		}
                	   		if(request("coupon_code"))
                	   		{
			                    $query1->where("coupon_code","like","%".request()->input("coupon_code")."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                	   		if(request("mobil"))
                	   		{
			                    $query1->where("mobil",request()->input("mobil"));
                	   		}
                    	});
                    }
            })
			->with(["stock","topu","irsaliyesi"])
			->orderBy(request("sort","created_at"),request("direction","desc"))
            ->get();

        $result = [];
		$result["total"] = $ops->count();

        foreach($ops as $op)
        {
			if($op->stock){
				$m2 = $op->product_qty * $op->stock->product_top * $op->stock->product_height / 10000;
			}else{
				$m2 = $op->product_qty * $op->product_top * $op->product_height / 10000;
			}
			if(isset($result["tum"][$op->product_name])){
				$result["tum"][$op->product_name] += $m2;
			}else{
				$result["tum"][$op->product_name] = $m2;
			}

			if(isset($result["cari"][$op->irsaliyesi->customer_id][$op->product_name])){
				$result["cari"][$op->irsaliyesi->customer_id][$op->product_name] += $m2;
			}else{
				$result["cari"][$op->irsaliyesi->customer_id][$op->product_name] = $m2;
			}
		}
		return $result;
    }
}
