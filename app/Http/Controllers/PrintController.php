<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Sube;
use Illuminate\Support\Facades\Auth;

class PrintController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    public function etiket($product_id)
    {
    	$product = Product::find($product_id);
		$sube = Sube::find(1);

		$n = app("App\Repositories\Etiket\EtiketImage",[$product,$sube]);

		$img = $n->olustur();

		return $img->response();
    }


}
