<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BayiRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Sube;
use App\Bayi;
use App\Customer;

class BayiGirisController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $customers = Customer::where("active",1)->selectRaw("id,concat(firmakodu,' ',name) as name")->pluck("name","id")->all();

        return view("bayi_giris.index",compact("user","customers"));
    }

    /**
     * get kurye records
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->user();

        return Bayi::where(function($query)use($user){

            if(request("customer_id"))
            {
                $query->where("customer_id",request("customer_id"));
            }
            if(request("aktif"))
            {
                $query->where("aktif",request("aktif"));
            }
            if(request("katalog"))
            {
                $query->where("katalog",request("katalog"));
            }
            if(request("name"))
            {
                $query->where("name",'like','%'.request("name").'%');
            }
            if(request("gsm"))
            {
                $query->where("gsm",'like','%'.request("gsm").'%');
            }
            if(request("tel"))
            {
                $query->where("tel",'like','%'.request("tel").'%');
            }
            if(request("firma"))
            {
                $query->where("firma",'like','%'.request("firma").'%');
            }
            if(request("email"))
            {
                $query->where("email",'like','%'.request("email").'%');
            }
            if(request("username"))
            {
                $query->where("username",'like','%'.request("username").'%');
            }
        })
        ->orderBy("id","desc")
        ->take(20)
        ->get([
            "id",
            "customer_id",
            "name",
            "gsm",
            "tel",
            "firma",
            "email",
            "username",
            "aktif",
            "katalog"
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\BayiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BayiRequest $request)
    {
        $customer = Customer::find($request->input("customer_id"));

        $bayi = new Bayi;
        $bayi->customer_id  = $customer->id;
        $bayi->name = $request->input("name");
        $bayi->gsm = $request->input("gsm");
        $bayi->tel = "";
        $bayi->firma = $customer->company_name;
        $bayi->email = "";
        $bayi->save();
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kurye = Bayi::findOrFail($id);
        $subeler = Sube::where("uretim",1)->lists("subeadi","id");
        $iller = config("iller");

        return view("kurye.show",compact("kurye","subeler","iller"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kurye = Bayi::findOrFail($id);
        $subeler = Sube::where("uretim",1)->lists("subeadi","id");
        $iller = config("iller");

        return view("kurye.edit",compact("kurye","subeler","iller"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\BayiRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bayi = Bayi::findOrFail($id);

        $input = $request->except("id","password");

        if($request->has("aktif"))
        {
            $input["aktif"] = $request->input("aktif");
        }
        else
        {
            $input["aktif"] = 0;
        }

        $bayi->update($input);

        if(request("password"))
        {
            $bayi->password = bcrypt(request("password"));
            $bayi->save();
        }

        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kurye = Bayi::findOrFail($id);
        $kurye->delete();

        return redirect()->route("kurye.index")->with("success","Silme işlemi başarılı");

    }
}
