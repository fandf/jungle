<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Sube;

class OnayController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware(["auth","can:siparis-onay"]);
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('onay');
    }

    public function store()
    {
        $user = Auth::user();
        return $this->repo->getOnayda($user);
    }


    public function onaylandi(Request $request)
    {
        $user = Auth::user();
        foreach($request->input("checkedPrd") as $op_id)
        {
            $this->repo->setOnaylandi($op_id,$user);
        }
        return 1;
    }
}
