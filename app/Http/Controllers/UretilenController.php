<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\IrsaliyeRepository;
use App\Top;

class UretilenController extends Controller
{
    protected $repo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IrsaliyeRepository $repo)
    {
        $this->middleware('auth');
        $this->repo = $repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $top_names = Top::where(function($query){
            $query->where("created_at",">",date("Y-m-d",strtotime("-3 months")));
            $query->orWhere("aktif",1);
        })
        ->pluck("name","id")->all();

        return view('uretilen',compact("top_names"));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        return $this->repo->getUretilen($user);
    }
}
