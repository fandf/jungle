<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function get()
    {
    	return  Product::select("id","name","brand","options","sku","saleprice")
                ->orderBy("name")
                ->get();
    }
}
