<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productimage;
use App\Customer;
use App\Catalog;

class UrunController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();

        $images = Productimage::select("id","customer_id","name","filename","catalog_id","status")
        ->where("name","<>","")
        ->where(function($query)use($user){
            if($user->customer_id)
            {
                $query->where("customer_id",$user->customer_id);
            }
        })
        ->where("catalog_id",1)
        ->orderBy("id","desc")
        ->paginate(50);

        $customers = Customer::select("id","name")
            ->where(function($query)use($user,$images){
                if($user->customer_id)
                {
                    $query->whereIn("id",$images->pluck("customer_id"));
                }
            })
            ->where("parent_id",0)
            ->pluck("name","id")
            ->all();

        $customers = array_unique($customers);
        $katalog = Catalog::pluck("name","id")->all();

        return view("urun.index",compact("images","user","customers","katalog"));
    }

    /**
     * get cari records
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $user = auth()->user();

        return Productimage::select("id","name","filename","customer_id","catalog_id","status")

        ->where(function($query)use($user){
            if($user->customer_id)
            {
                $query->where("customer_id",$user->customer_id);
            }
            if(request()->has("status"))
            {
                $query->where("status",request("status"));
            }
            if(request("name"))
            {
                $query->where("name",request("name"));
            }
            if(request("filename"))
            {
                $query->where("name",'like','%'.request("filename").'%');
            }
            if(request("catalog_id"))
            {
                $query->where("catalog_id",request("catalog_id"));
            }

        })
        ->orderBy("id","desc")
        ->paginate(50);

    }

    public function upload($image_id)
    {
        $name = "";
        $product_image = null;
        if($image_id != 'yeni'){
            $product_image = Productimage::findOrFail($image_id);
        }

        if(request()->file('file'))
        {
            $image = request()->file('file');

            $name = time().$image->getClientOriginalName();
            $image->storeAs('/public/images/products/org',$name);
            $img = \Image::make(request()->file('file'))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path("app/public/images/products/".$name));

            if($product_image)
            {
                if(\File::exists(storage_path('/app/public/images/products/org/'.basename($product_image->filename))))
                {
                    unlink(storage_path('/app/public/images/products/org/'.basename($product_image->filename)));
                    unlink(storage_path('/app/public/images/products/'.basename($product_image->filename)));
                }
                $product_image->filename = '/images/products/'.$name;
                $product_image->save();
            }

            return response()->json(['success' => 'ok','image'=>'/images/products/'.$name], 200);
        }
        return response()->json(['success' => false,'image'=>$name], 404);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            "name"=>"required",
            "filename"=>"required"
        ]);

        $image = new Productimage;
        $image->name  = request("name");
        $image->filename = request("filename");
        $image->product_id =  1;
        $image->customer_id = auth()->user()->customer_id?:1;
        $image->status = 1;
        $image->save();

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Productimage::findOrFail($id);

        return view("urun.show",compact("image"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Productimage::findOrFail($id);

        return view("urun.edit",compact("image"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $image = Productimage::findOrFail($id);

        $image->name = request("name");
        $image->customer_id = request("customer_id");
        $image->catalog_id =  request("catalog_id");
        $image->status = request("status");
        $image->save();

        return 1;
    }

    public function list()
    {
        $images = Productimage::paginate(100);

        return view("urun.list",compact("images"));
    }

    public function delete($id)
    {
        $product_image = Productimage::findOrFail($id);
        if($product_image)
        {
            if(\File::exists(storage_path('/app/public/images/products/org/'.basename($product_image->filename))))
            {
                unlink(storage_path('/app/public/images/products/org/'.basename($product_image->filename)));
                unlink(storage_path('/app/public/images/products/'.basename($product_image->filename)));
            }

            $product_image->delete();
            return 1;
        }
        return 0;
    }
}
