<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faturaproduct extends Model
{

    protected $guarded = array("id");

	public function fatura()
    {
        return $this->belongsTo('App\Fatura','fatura_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Stock');
    }
}
