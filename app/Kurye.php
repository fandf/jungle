<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Kurye extends Authenticatable
{
    use SoftDeletes;

    protected $fillable = [
    
    	'sube_id',
    	'il_id',
    	'name',
    	'gsm',
    	'tel',
    	'firma',
    	'email',
    	'aktif',
        "username"
    ];

    public function orders()
    {
    	return $this->hasMany("App\Order");
    }
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
