<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Fatura extends Model
{
    protected $guarded = array("id");

    protected $fillable = [

      'order_id',
      'user_id',
      'order_number',
      'sube_kodu',
      'customer_id',
      'tax',
      'subtotal',
      'total',
      'coupon_discount',
      'discount_yuzde',
      'shipping',
      'fatura_tarihi',
      'cikis_tarihi',
      'gon_adsoyad',
      'gon_cep',
      'fatura_teslimat',
      'fatura_unvan',
      'fatura_adres',
      'vergi_dairesi',
      'vergi_no',
      'kimlik_no',
      'neden',
      'gon_adres',
      'irsaliye',
      'faturano'
    ];

    public function faturaproducts()
    {
        return $this->hasMany('App\Faturaproduct','fatura_id');
    }


    public function getCikisTarihiAttribute($value)
    {
        return $this->attributes['cikis_tarihi'] = Carbon::parse($value)->format('d-m-Y');
    }

    public function setCikisTarihiAttribute($value)
    {
        return $this->attributes['cikis_tarihi'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getFaturaTarihiAttribute($value)
    {
        return $this->attributes['fatura_tarihi'] = Carbon::parse($value)->format('d-m-Y');
    }

    public function setFaturaTarihiAttribute($value)
    {
        return $this->attributes['fatura_tarihi'] = Carbon::parse($value)->format('Y-m-d');
    }
}
