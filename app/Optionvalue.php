<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Optionvalue extends Model {


	protected $guarded = array("id");

	public function option()
	{
		return $this->belongsTo('\App\Option');
	}

}
