<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {


	protected $guarded = array("id");

	public function products()
	{
		return $this->belongsToMany('\App\Product')->withPivot('sort');
	}

	protected $validationRules = array(
		'name' 			=> 'required',
		'slug'	 		=> 'required',
		'parent_id' 	=> 'required',
		'menu_title' 	=> 'required',
		'description' 	=> 'required'

	);

}
