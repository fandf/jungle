<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('siparis-onay',function($user){
            return property_exists(json_decode($user->permissions),'onay');
        });
        Gate::define('indirim-yapabilir',function($user){
            return property_exists(json_decode($user->permissions),'indirim');
        });
        Gate::define('urun-ekleyebilir',function($user){
            return property_exists(json_decode($user->permissions),'urun');
        });
        Gate::define('iade-yapabilir',function($user){
            return property_exists(json_decode($user->permissions),'iade');
        });
        Gate::define('fatura-bakabilir',function($user){
            return property_exists(json_decode($user->permissions),'fatura');
        });
        Gate::define('baski-hazirlik',function($user){
            return property_exists(json_decode($user->permissions),'hazirlik');
        });
        Gate::define('baski',function($user){
            return property_exists(json_decode($user->permissions),'baski');
        });
        Gate::define('hata-takip',function($user){
            return property_exists(json_decode($user->permissions),'hata');
        });
        Gate::define('analiz',function($user){
            return property_exists(json_decode($user->permissions),'analiz');
        });
        Gate::before(function($user){
            if($user->permissions)
            {
                $p = json_decode($user->permissions);
                if(property_exists($p,'superuser'))
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        });
    }
}
