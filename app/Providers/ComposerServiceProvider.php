<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Kurye;
use App\Sube;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
         view()->composer(['layouts.app'],function($view){
            $hatali = \DB::table("orderproducts")
                ->whereIn("status",[5,10])
                ->count();
            $view->with("hatali",$hatali);
         });
        view()->composer(['onay','hazirlik','uretimde','uretilen','transfer','dagitimda','tum','hatali','fatura','cikislar','cikislar/panel'],function($view){
            $user = Auth::user();
            $view->with("user",$user);
            $kuryes = Kurye::where(function($query)use($user){
                    if($user->sube_id)
                     {
                        $query->where("sube_id",$user->sube_id);
                    }
                })
                ->where("aktif",1)
                ->get();
            $kuryeler = [];
            foreach($kuryes as $kurye)
            {
                $sube_adi = "Jungle";
                $kuryeler[$kurye->id] = [
                    "name"=>$kurye->name." ".$sube_adi,
                    "gsm"=>$kurye->gsm,
                    "sube_id"=>$kurye->sube_id,
                    "aktif"=>$kurye->aktif
                ];
            }
            $view->with("kuryeler",$kuryeler);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
