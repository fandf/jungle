<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Log;

class SendOnayEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $msg;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }

    /**
     * Dağıtım Eposta gönderimi
     *
     * @return void
     */
    public function handle()
    {
        $mesaj = "Sayın " . $this->msg["gon_adsoyad"] .",  ".$this->msg["order_number"]  . " numaralı siparişiniz onaylanmıştır. Gönderim saatine uygun olarak atölyemizde üretilecektir";
        return "email iptal";
        Mail::send('emails.dagitima', compact("mesaj"), function ($m) {
            $m->subject("Nefis Demet ".$this->msg["order_number"]." numaralı siparişiniz onaylandı");
            $m->to($this->msg["gon_email"],$this->msg["gon_adsoyad"]);
        });

    }
}
