<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use File;
use Log;

class BaskiTasi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $firma;
    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($firma,$url)
    {
        $this->firma = $firma;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if(Storage::disk("dropbox")->put($this->firma."/".str_replace('upload/','',$this->url),File::get(storage_path("app/".$this->url)))){
                File::delete(storage_path("app/".$this->url));
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
