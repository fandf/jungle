<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\Sms\SmsVitrini;
use Log;

class SendKargoSms implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $msg;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }


    /**
     * Kargoya gönderildi Sms gönderimi
     *
     * @return void
     */
    public function handle(SmsVitrini $sms)
    {
        $mesaj = "Sayın " . $this->msg["gon_adsoyad"] .",  ".$this->msg["order_number"]  . " numaralı siparişiniz anlaşmalı kargo şirketine teslim edilmiştir.";

        if(env("APP_ENV")=="local")
        {
            $sms->sms_test($this->msg["gon_cep"],$mesaj);
        }
        else
        {
            return "sms";
            $gsm = preg_replace("/[^0-9]/", "", $this->msg["gon_cep"]);
            $gsm = $gsm == '00000000000' ? '' : $gsm;
            if(strlen($gsm) > 9)
            {
                $gonderim = $sms->send($gsm,$mesaj);
                Log::info("SMS KARGO".$this->msg["order_number"],$gonderim);
            }
        }
    }
}
