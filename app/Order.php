<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Order extends Model {

	use SoftDeletes;

    protected $guarded = array("id");

	public function orderproducts()
    {
        return $this->hasMany('App\Orderproduct','order_id');
    }

	public function irsaliyetras()
    {
        return $this->hasMany('App\Irsaliyetra','order_id');
    }

    public function getSiptarAttribute($value)
    {
        return $this->attributes['siptar'] = Carbon::parse($value)->format('d-m-Y');
    }

    public function setSiptarAttribute($value)
    {
        return $this->attributes['siptar'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d.m.Y H:i');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d.m.Y H:i');
    }
}
