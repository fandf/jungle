<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Bayi extends Authenticatable
{
    use SoftDeletes;

    protected $table = "bayi";

    protected $fillable = [

    	'customer_id',
    	'il_id',
    	'name',
    	'gsm',
    	'tel',
    	'firma',
    	'email',
    	'aktif',
        "username",
        "katalog"
    ];

    public function orders()
    {
    	return $this->hasMany("App\Order");
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
