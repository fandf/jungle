<?php

function money_tr_string($money,$input_format=0)
{

    $arr1 = array("","Bir","İki","Üç","Dört","Beş","Altı","Yedi","Sekiz","Dokuz");

    $arr10 = array("","On","Yirmi","Otuz","Kırk","Elli","Atmış","Yetmiş","Seksen","Doksan");

    $arr100 = array("","Yüz","İkiYüz","ÜçYüz","DörtYüz","BeşYüz","AltıYüz","YediYüz","SekizYüz","DokuzYüz");

    $add_word = array("","Bin","Milyon","Milyar","Trilyon","Katrilyon","Kentilyon","Seksilyon","Septilyon","Oktilyon");



    if($input_format==0){ //10000.25

            $money=number_format($money,2,',','.');

    }

    else if($input_format==1){ //10,000.25

            $money=str_replace(',','',$money);

            $money=number_format($money,2,',','.');

    }

    else if($input_format==2){ //10000,25

            $money=str_replace(',','.',$money);

            $money=number_format($money,2,',','.');

    }

    else if ($input_format==3){//10.000,25

            $money=$money;

    }

    $money_part1=explode(",",$money);

    $money_part2=explode(".",$money_part1[0]);



    $output='';

    $trees_len=count($money_part2);

    $addword_start=$trees_len-1;

    for($i=0;$i<$trees_len;$i++){

            if(strlen($money_part2[$i]*1)==3){

                    $output.=$arr100[substr($money_part2[$i],0,1)].$arr10[substr($money_part2[$i],1,1)].$arr1[substr($money_part2[$i],2,1)];

            }

            else if(strlen($money_part2[$i]*1)==2){

                    $output.=$arr10[substr($money_part2[$i]*1,0,1)].$arr1[substr($money_part2[$i]*1,1,1)];

            }

            else if(strlen($money_part2[$i]*1)==1){

                    if(!($addword_start==1 and $money_part2[$i]*1==1)){

                            $output.=$arr1[substr($money_part2[$i]*1,0,1)];

                    }

            }

            if(($money_part2[$i]*1)>0){

                    $output.=$add_word[$addword_start];

            }

            $addword_start=$addword_start-1;

    }

    if(substr($money_part1[1],0,1)==0 and substr($money_part1[1],1,1)==0){

            $output.=' Lira ';

    }

    else {

            $output.=' Lira '.$arr10[substr($money_part1[1],0,1)].$arr1[substr($money_part1[1],1,1)].' Krş';

    }

    return $output;

}
function mb_str_pad( $input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT)
{
    $diff = strlen( $input ) - mb_strlen( $input );
    return str_pad( $input, $pad_length + $diff, $pad_string, $pad_type );
}
