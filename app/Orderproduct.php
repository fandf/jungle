<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Orderproduct extends Model {

    protected $guarded = array("id");

	public function irsaliyesi()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function stock()
    {
        return $this->belongsTo('App\Stock','stok_kodu','kod');
    }

    public function topu()
    {
        return $this->belongsTo('App\Top','top_id','id');
    }

    public function irsaliyetras()
    {
        return $this->hasMany('App\Irsaliyetra','op_id');
    }

    public function getTeslimTarihiAttribute($value)
    {
        return $this->attributes['teslim_tarihi'] = Carbon::parse($value)->format('d-m-Y');
    }

    public function setTeslimTarihiAttribute($value)
    {
        return $this->attributes['teslim_tarihi'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getCikisTarihiAttribute($value)
    {
        return $this->attributes['cikis_tarihi'] = Carbon::parse($value)->format('d-m-Y');
    }

    public function setCikisTarihiAttribute($value)
    {
        return $this->attributes['cikis_tarihi'] = Carbon::parse($value)->format('Y-m-d');
    }
}
