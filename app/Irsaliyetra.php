<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Irsaliyetra extends Model {


	protected $guarded = array("id");

	public function irsaliyes()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function user()
    {
        return $this->morphTo();
    	//return $this->belongsTo('App\User','user_id');
    }

}
