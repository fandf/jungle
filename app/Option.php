<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {


	protected $guarded = array("id");

	public function optionvalues()
	{
		return $this->hasMany('\App\Optionvalue');
	}

}
