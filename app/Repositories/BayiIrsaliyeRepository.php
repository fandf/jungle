<?php
namespace App\Repositories;

use App\Product;
use App\Order;
use App\Orderproduct;
use App\Irsaliyetra;
use Illuminate\Support\Facades\Auth;
use App\Number;
use Event;
use App\Jobs\SendDagitimSms;
use App\Jobs\SendDagitimEmail;
use App\Jobs\SendKargoSms;
use App\Jobs\SendKargoEmail;
use App\Jobs\SendTeslimSms;
use App\Jobs\SendTeslimEmail;

class BayiIrsaliyeRepository
{
	protected $irsaliye;
	protected $irsaliyeproduct;
	protected $user;
	protected $selected = ["id","order_id","irsaliye_no","status","okunan","alici_adsoyad","alici_firma","stok_kodu","product_name","mevcut_urun","product_desc","product_image","product_options","product_top","product_width","product_height","product_sku","product_qty","cikis_tarihi","teslim_tarihi","teslim_saati","shipping","semt_adi","il_adi","kargo","kargo_no","kurye_id","uretim","sms","created_at","kargo","ek"];
	protected $selectedIrsaliye = [
		"id","order_number","gon_adsoyad","fatura_unvan","siptar","coupon_code","payment","payment_ok","mobil","kapida","bize_mesaj","siparisalan"
	];

	//"alici_adsoyad","semt_adi","il_adi","kargo","kargo_no","kargo","bize_mesaj",

	public function __construct(Order $order,Orderproduct $orderproduct)
	{
		$this->order = $order;
		$this->orderproduct = $orderproduct;
	}

	public function getById($id)
	{
		return $this->order->find($id);
	}

	public function getIrsaliyeproductById($id)
	{

		return $this->orderproduct->where("id",$id)
			->whereHas("irsaliyesi",function($query){
				$query->where("customer_id",auth()->guard("bayi")->user()->customer_id);
			})
			->with(["irsaliyesi"])
			->first();
	}

	public function getIrsaliyeproductByIds($ids)
	{
		$customer_id = auth()->guard("bayi")->user()->customer_id;

		return $this->orderproduct->whereIn("id",$ids)
			->whereHas("irsaliyesi",function($query){
				$query->where("customer_id",auth()->guard("bayi")->user()->customer_id);
			})
			->with("irsaliyesi")
			->get();
	}

	public function getByIdWithOp($id)
	{
		$irsaliye =  $this->order->with("orderproducts")->find($id);

		$order = Order::where("order_number",$irsaliye->order_number)->first();

		if($order)
		{
			$irsaliye->kayit_tarihi = date("d.m.Y H:i",strtotime($order->created_at));
		}

		return $irsaliye;
	}

	public function getBayiWithFilter($tarih1,$tarih2,$tarih,$limit)
	{
        $customer_id = auth()->guard("bayi")->user()->customer_id;

		return $this->orderproduct
			->select($this->selected)
			//->where("status",">",0)
            ->where("ek",0)
			->where(function($query) use($customer_id,$tarih1,$tarih2,$tarih){

				if($tarih=='cikis_tarihi' || $tarih == 'teslim_tarihi')
				{
					$query->whereBetween($tarih,[$tarih1,$tarih2]);
				}
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("kargo_no"))
                {
                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
                }
                if(request("teslim_saati"))
                {
                	$query->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
                }
                if(request("kurye_id"))
                {
                	$query->where("kurye_id",request()->input("kurye_id"));
                }
                if(request("sms"))
                {
                	$query->where("sms",request()->input("sms"));
                }

                if(request("status"))
                {
                	$query->where("status",request()->input("status"));
                }
                if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun",request()->input("mevcut_urun"));
                }
                if(request("irsaliye_no"))
                {
                	$query->where("irsaliye_no",request()->input("irsaliye_no"));
                }
                if(request("kargo"))
                {
                	$query->where("kargo","like","%".request()->input("kargo")."%");
                }
                if(request("iptaliade"))
                {
                	if(request()->input("iptaliade") != 'true')
                	{
                		$query->whereNotIn("status",[5,6]);
                	}
                }
                else
                {
                	$query->whereNotIn("status",[5,6]);
                }
                $query->whereHas("irsaliyesi",function($q)use($customer_id){
                    $q->where("customer_id",$customer_id);
                });
                //if query needs irsaliyesi
                if(request("order_number")
                	OR $tarih == 'siptar'
                	OR request("gon_adsoyad")
                	OR request("coupon_code")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("mobil")
                	OR request("siptar")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1)use($tarih1,$tarih2,$tarih){


                	   		if($tarih=='siptar')
							{
								$query1->whereBetween($tarih,[$tarih1,$tarih2]);
							}
							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("order_number","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("coupon_code"))
                	   		{
			                    $query1->where("coupon_code","like","%".request()->input("coupon_code")."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                	   		if(request("mobil"))
                	   		{
			                    $query1->where("mobil",request()->input("mobil"));
                	   		}
                    	});
                    }
            })
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			},"stock"])
			->orderBy(request("sort","created_at"),request("direction","desc"))
			->take($limit)
			->get();
	}

    public function getIrsaliyeNumber($sube_id)
    {
        $number = Number::where('type','irsaliye')->where('sube_id',$sube_id)->first();
        if($number)
        {
            return  $number->no;
        }
        return "";
    }

}
