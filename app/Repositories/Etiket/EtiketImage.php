<?php
namespace App\Repositories\Etiket;

use Image;
use App\Product;
use App\Etiket;
use App\Sube;
use File;

class EtiketImage{

	protected $product;
	protected $sube;
	public function __construct(Product $product,Sube $sube)
	{
		$this->product = $product;
		$this->sube = $sube;
	}

	public function textToImage()
	{
		// create Image from file
		$img = Image::canvas(320, 320, '#fff');

		$img->text('The quick brown fox jumps over the lazy dog.', 120, 100);
		// use callback to define details
		$img->text('The quick brown fox jumps over the lazy dog.', 100, 200, function($font) {
		    $font->file('font/frabk/FRABK.ttf');
		    $font->size(13);
		    $font->color('#000');
		    $font->align('center');
		    $font->valign('top');
		    //$font->angle(90);
		});


		 return $img->response();
	}
	public function bas()
	{
		$img = $this->olustur();
		return $img->encode('data-url');
	}
	public function kaydet()
	{
		$img = $this->olustur();
		$dir =$_SERVER["DOCUMENT_ROOT"].'/labels/' . $this->sube->id;
		if(!File::isDirectory($dir))
		{
			File::makeDirectory($dir, $mode = 0777, true, true);
		}
		$img->save($dir."/".$this->product->id.".jpg");

		return $this->product->id;
	}
	public function olustur()
	{
		$etiket = Etiket::find($this->product->etiket_id);

        $icindekiler =  wordwrap(strip_tags($etiket->icindekiler),160,'\n');
        $icindekiler = trim(preg_replace('/\s\s+/', ' ',$icindekiler));
		$data = explode('\n',$icindekiler);

		$img = Image::canvas(800, 320, '#fff');

		$s = 30;

		$img->text($this->product->name, 10, $s,function($font) {
			$font->file('fonts/gillisans_bold/GilSEB26.ttf');
			$font->size(14);
		});
		$s+=30;
		$img->text('Gıda Tar.ve Hay.Bak. Tarım müd No: '.$this->sube->tarim_belge. ' kayıt nolu işletme kayıt belgesi :', 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('ile ürettiğimiz ve sevk ettiğimiz ürün içerikleri :', 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});

		for ($i=0; $i < count($data) ; $i++) {
			$s+=20;
			$img->text($data[$i], 10, ($s),function($font) {
				$font->file('fonts/gillisans/GillSans.ttf');
				$font->size(13);
			});
		}
		$s +=30;
		$img->text('Üretici Firma Adı: '.$this->sube->customer_name, 10,$s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('Üretici Firma Adresi: '.$this->sube->adres, 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('ÜRT: '.date("d.m.Y").' STK: '.$etiket->son_tuketim, 10,$s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('Parti Numarası : Üretim tarihidir. Orijin Ülke : TM  Miktar : ', 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('Muhafaza Şartları : '.$etiket->muhafaza, 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('Alerjen Uyarısı : '.$etiket->alerjen, 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text('Gıda Tarım ve Hayvancılık Bakanlığı  çalışma izin numaralı işyerinde üretilmiştir.', 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});
		$s+=20;
		$img->text($etiket->dipnot, 10, $s,function($font) {
			$font->file('fonts/gillisans/GillSans.ttf');
			$font->size(13);
		});

		//$img->insert('assets/img/fruitflowers-logo.png', 'bottom-right',10,60);

		return $img;
	}

}
