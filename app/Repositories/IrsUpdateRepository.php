<?php
namespace App\Repositories;

use App\Product;
use App\Order;
use App\Orderproduct;
use App\Ordertra;

class IrsUpdateRepository
{
    protected $irsaliye;
    protected $irsaliyeproduct;
    protected $user;

    public function __construct(Order $irsaliye,Orderproduct $irsaliyeproduct)
    {
        $this->irsaliye = $irsaliye;
        $this->irsaliyeproduct = $irsaliyeproduct;
    }

    public function update($input)
    {
        $input["siptar"] = date("Y-m-d",strtotime(request()->input("siptar")));
        $orderproducts = $input["orderproducts"];
        unset($input["orderproducts"]);
        unset($input["kayit_tarihi"]);
        unset($input["created_at"]);
        unset($input["updated_at"]);
        unset($input["deleted_at"]);
        unset($input["ip_address"]);
        unset($input["payment_extra"]);

        $irsaliye = $this->irsaliye->findOrFail($input["id"]);
        unset($input["id"]);
        $irsaliye->update($input);
        foreach($orderproducts as $op_array)
        {
            if($op_array["ek"]==0)
            {
                unset($op_array["created_at"]);
                unset($op_array["updated_at"]);
                $op = $this->irsaliyeproduct->findOrFail($op_array["id"]);

                unset($op_array["id"]);
                unset($op_array["product"]);
                $op->update($op_array);
            }
        }
        return $this->hesap_guncelle($irsaliye);
    }


    public function opguncelle($request)
    {
        $irsaliye = $this->irsaliye->find($request->input("order_id"));

        if(!$irsaliye)
        {
            return 0;
        }

        $op = $this->irsaliyeproduct->find($request->input("id"));
        if(!$op)
        {
            return 0;
        }

        $op->product_id = $request->input("product_id");
        //$op->product_desc = "";
        // $op->product_tax = $product->kdv;
        // $op->product_sku = $product->sku;
        // $op->product_name = $product->name;
        // $op->product_image = $product->image;
        $op->product_qty = $request->input("product_qty");
        $op->product_price = $request->input("product_price");
        //$op->indirimli = $product->price > $product->saleprice ? 1 : 0;
        $op->save();

        return $this->hesap_guncelle($irsaliye);
    }

    public function opnew($request)
    {

        $irsaliye = $this->irsaliye->find($request->input("order_id"));

        if(!$irsaliye)
        {
            return 0;
        }

        $op = new $this->irsaliyeproduct();

        $product = Product::findOrFail($request->input("new_product.id"));

        $op->order_id = $irsaliye->id;
        $op->product_id = $product->id;
        $op->product_desc = "";
        $op->product_price = $product->saleprice;
        $op->product_tax = $product->kdv;
        $op->product_sku = $product->sku;
        $op->product_name = $product->name;
        $op->product_image = $product->image;
        $op->product_options = "";
        $op->product_qty = $request->input("new_product.qty");
        $op->parent_id = $request->input("new_product.parent_id");
        if(!$op->parent_id)
        {
            $op->shipping = $request->input("new_product.shipping");
            $op->ship = $request->input("new_product.ship");
            $op->cikis_tarihi = date("Y-m-d",strtotime($request->input("new_product.cikis_tarihi")));
            $op->teslim_tarihi = date("Y-m-d",strtotime($request->input("new_product.teslim_tarihi")));
            $op->teslim_saati = $request->input("new_product.teslim_saati");
            $op->indirimli = $product->price > $product->saleprice ? 1 : 0;
        }
        else
        {
            $op->ek = 1;
            $ana = $this->irsaliyeproduct->findOrFail($op->parent_id);
            $op->cikis_tarihi = $ana->cikis_tarihi;
            $op->teslim_tarihi = $ana->teslim_tarihi;
            $op->teslim_saati = $ana->teslim_saati;
        }

        //$op->user_site = 'jungle.com.tr';
        $op->status = 1;

        $op->save();


        return $this->hesap_guncelle($irsaliye);
    }

    public function opsil($irs_id,$op_id)
    {
        $irsaliye = $this->irsaliye->find($irs_id);

        if(!$irsaliye)
        {
            return 0;
        }

        $op = $this->irsaliyeproduct->find($op_id);

        $op->delete();

        return $this->hesap_guncelle($irsaliye);

    }

    public function coupon_update($order_id,$coupon_code,$coupon_discount)
    {
        $irsaliye = $this->irsaliye->find($order_id);
        if($coupon_code)
        {
            return $this->kupon_guncelle($irsaliye,"coupon_code",$coupon_code);
        }
        if($coupon_discount)
        {
            return $this->indirim_guncelle($irsaliye,"coupon_code",$coupon_discount);
        }
    }

    private function kupon_guncelle($irsaliye,$field,$value)
    {
        $coupon = Coupon::where("code",$value)->first();

        if(!$coupon)
        {
            return response()->json(["hata"=>1,"mesaj"=>"Kupon kodu bulunamadı.."],202);
        }

        if(date("Y-m-d") > $coupon->end_date)
        {
             return response()->json(["hata"=>1,"mesaj"=>"Kuponun son kullanma tarihi ".date("d.m.Y",strtotime($coupon->end_date))." geçmiş.."],202);
        }

        if(date("Y-m-d") < $coupon->start_date)
        {
            return response()->json(["hata"=>1,"mesaj"=>"Kuponun ilk kullanma tarihi ". date("d.m.Y",strtotime($coupon->start_date))],202);
        }

        if($coupon->min_order > ($irsaliye->total+$irsaliye->coupon_discount))
        {
            return response()->json(["hata"=>1,"mesaj"=>"Ödeme toplamı kuponun minimum  ".$coupon->min_order." TL tutarının altında"],202);
        }

        $irsaliye->coupon_code = $value;
        $irsaliye->discount_yuzde = $coupon->reduction_amount;

        if($irsaliye->discount_yuzde > 100)
        {
            $irsaliye->discount_yuzde = 100;
        }

        return $this->hesap_guncelle($irsaliye);
    }

    private function indirim_guncelle($irsaliye,$field,$value)
    {
        $indirimsiz = $irsaliye->subtotal + $irsaliye->coupon_discount;
        $irsaliye->discount_yuzde = 100 * $value/$indirimsiz;

        return $this->hesap_guncelle($irsaliye);
    }

    private function hesap_guncelle($irsaliye)
    {
        $indirimsiz = 0;
        $indirimsiz_kdv8=0;
        $indirimsiz_kdv18=0;
        $discount = 0;
        $kdv8 = 0;
        $kdv18 = 0;
        $subtotal = 0;
        $discount_yuzde = $irsaliye->discount_yuzde;
        $shipping = 0;

        foreach ($irsaliye->orderproducts as $op)
        {
            $s = $op->product_price * $op->product_qty;

            $indirimsiz +=$s;
            $indirimsiz_kdv8  += ($op->product_tax == 8) ? $s*0.08 : 0;
            $indirimsiz_kdv18 += ($op->product_tax == 18) ? $s*0.18 : 0;
            $shipping += $op->shipping;

            if($op->product AND $op->indirimli == 0 AND $op->ek == 0)
            {
                $subtotal += $s - $s * $discount_yuzde / 100;
                $kdv8  += ($op->product_tax == 8) ? ($s - $s * $discount_yuzde / 100)*0.08 : 0;
                $kdv18 += ($op->product_tax == 18) ? ($s - $s * $discount_yuzde / 100)*0.18 : 0;
            }
            else
            {
                $subtotal += $s;
                $kdv8  += ($op->product_tax == 8) ? $s*0.08 : 0;
                $kdv18 += ($op->product_tax == 18) ? $s*0.18 : 0;

            }
        }

        $irsaliye->total = $subtotal + ($kdv18+$kdv8);
        $irsaliye->subtotal = $subtotal;
        $irsaliye->tax = $kdv18+$kdv8;
        $irsaliye->coupon_discount = $indirimsiz  - $subtotal;
        $irsaliye->shipping = $shipping;

        $irsaliye->save();
        unset($irsaliye->payment_extra);
        unset($irsaliye->created_at);
        unset($irsaliye->updated_at);
        unset($irsaliye->deleted_at);
        unset($irsaliye->kayit);
        unset($irsaliye->ip_address);
        return $irsaliye;

    }




}
