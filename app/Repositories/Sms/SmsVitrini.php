<?php
namespace App\Repositories\Sms;
use Log;

class SmsVitrini{

	function send($kime, $msj, $title = "") {

	    $x = array("(", ")", " ", "-");

	    $mesajData['user'] = array(
	        'name' => env("SMS_VITRINI_USER"),
	        'pass' => env("SMS_VITRINI_PASS")
	    );

	    if ($title == "") {
	        $mesajData['msgBaslik'] = env("SMS_TITLE");
	    }
	    else {
	        $mesajData['msgBaslik'] = $title;
	    }

	    $mesajData['msgData'][] = array(
	        'tel' => $kime,
	        'msg' => $msj
	    );


	    $bad = array("\u00fc", "\u011f", "\u0131", "\u015f", "\u00e7", "\u00f6", "\u00dc", "\u011e", "\u0130", "\u015e", "\u00c7", "\u00d6");
	    $good = array("ü", "ğ", "ı", "ş", "ç", "ö", "Ü", "Ğ", "İ", "Ş", "Ç", "Ö");

	    $request = "data=" . base64_encode(json_encode($mesajData));

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, 'http://api.mesajpaneli.com/json_api/');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    $result = curl_exec($ch);
	    curl_close($ch);
	    return json_decode(base64_decode($result), TRUE);
	}
	function sms_test($kime, $msj, $title = "") {
		Log::info("SMS : ".$kime,[$msj]);
	}
}
