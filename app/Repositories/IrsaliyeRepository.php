<?php
namespace App\Repositories;

use App\Product;
use App\Order;
use App\Orderproduct;
use App\Irsaliyetra;
use Illuminate\Support\Facades\Auth;
use App\Number;
use Event;
use App\Jobs\SendDagitimSms;
use App\Jobs\SendDagitimEmail;
use App\Jobs\SendKargoSms;
use App\Jobs\SendKargoEmail;
use App\Jobs\SendTeslimSms;
use App\Jobs\SendTeslimEmail;

class IrsaliyeRepository
{
	protected $order;
	protected $orderproduct;
	protected $user;
	protected $selected = ["id","order_id","irsaliye_no","status","okunan","alici_adsoyad","alici_firma","stok_kodu","product_name","mevcut_urun","product_desc","product_image","production_image","product_options","product_top","top_id","product_width","product_height","product_sku","product_qty","cikis_tarihi","teslim_tarihi","teslim_saati","shipping","semt_adi","il_adi","kargo","kargo_no","kurye_id","uretim","sms","created_at","kargo","ek"];
	protected $selectedIrsaliye = [
		"id","order_number","gon_adsoyad","fatura_unvan","siptar","coupon_code","payment","payment_ok","mobil","kapida","bize_mesaj","siparisalan"
	];

	//"alici_adsoyad","semt_adi","il_adi","kargo","kargo_no","kargo","bize_mesaj",

	public function __construct(Order $order,Orderproduct $orderproduct)
	{
		$this->order = $order;
		$this->orderproduct = $orderproduct;
	}

	public function getById($id)
	{
		return $this->order->find($id);
	}

	public function getIrsaliyeproductById($id)
	{
		return $this->orderproduct->where("id",$id)->with("irsaliyesi")->first();
	}

	public function getIrsaliyeproductByIds($ids)
	{
		return $this->orderproduct
			->whereIn("id",$ids)
			->with(["irsaliyesi","stock"])
			->orderBy("product_top")
			->get();
	}

	public function getByIdWithOp($id)
	{
		$irsaliye =  $this->order->with("orderproducts")->find($id);

		$order = Order::where("order_number",$irsaliye->order_number)->first();

		if($order)
		{
			$irsaliye->kayit_tarihi = date("d.m.Y H:i",strtotime($order->created_at));
		}

		return $irsaliye;
	}

	public function guncelle($id,$field,$value)
	{
		$irsaliye = $this->order->find($id);

		if($irsaliye)
        {
            if($irsaliye->$field!=$value)
            {
                $irsaliye->$field = $value;
                $irsaliye->save();
                return 1;
            }
        }
        return 0;
	}

	public function getOnayda()
	{
		$user = Auth::user();
		$sube_id = $user->sube_id;
        $subeler = null;
        if($user->ek_subeler)
        {
            $subeler = explode(",",$user->ek_subeler);
		}

		$products = $this->orderproduct
			->select($this->selected)
			->whereIn("status",[0])
			->where("uretim",0)
			->where(function($query)use($sube_id,$subeler){
				$query->where("cikis_tarihi","<=",date("Y-m-d",strtotime("+1 months")));

                if(request("product_name"))
                {
                	$query->where("product_name","like","%".request()->input("product_name")."%");
                }
                if(request("product_sku"))
                {
                	$query->where("product_sku","like","%".request()->input("product_sku")."%");
                }
                if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun","like","%".request()->input("mevcut_urun")."%");
                }
                if(request("product_desc"))
                {
                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
                }
                if(request("product_options"))
                {
                	$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
                }
				if(request("product_width"))
                {
                	$query->where("product_width",request()->input("product_width"));
                }
				if(request("product_height"))
                {
                	$query->where("product_height",request()->input("product_height"));
                }
                if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
			})
			->whereHas("irsaliyesi",function($query){
				if(request("siptar"))
                {
                	$query->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
                }
				if(request("bize_mesaj"))
                {
                	$query->where("bize_mesaj","like","%".request()->input("bize_mesaj")."%");
                }
				if(request("gon_adsoyad"))
                {
                	$query->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                }
				if(request("fatura_unvan"))
                {
                	$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        $query->where("fatura_unvan","like","%".$fatura_unvan."%");
                }
				if(request("order_number"))
                {
                	$query->where("irsaliye_no","like","%".request()->input("order_number")."%");
                }
			})
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			}])
			->orderBy(request("sort","product_top"),request("direction",""))
			->get();


		return compact("products");
	}

	public function getHazirlik()
	{
		$user = Auth::user();
		$sube_id = $user->sube_id;

		$products = $this->orderproduct
			->select($this->selected)
			->whereIn("status",[1])
			->where(function($query)use($sube_id){
				$query->where("cikis_tarihi","<=",date("Y-m-d",strtotime("+1 months")));

                if(request("product_name"))
                {
                	$query->where("product_name","like","%".request()->input("product_name")."%");
                }
                if(request("product_sku"))
                {
                	$query->where("product_sku","like","%".request()->input("product_sku")."%");
                }
                if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun","like","%".request()->input("mevcut_urun")."%");
                }
                if(request("product_desc"))
                {
                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
                }
                if(request("product_options"))
                {
                	$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
                }
				if(request("product_width"))
                {
                	$query->where("product_width",request()->input("product_width"));
                }
				if(request("product_height"))
                {
                	$query->where("product_height",request()->input("product_height"));
                }
                if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
			})
			->whereHas("irsaliyesi",function($query){
				if(request("siptar"))
                {
                	$query->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
                }
				if(request("bize_mesaj"))
                {
                	$query->where("bize_mesaj","like","%".request()->input("bize_mesaj")."%");
                }
				if(request("gon_adsoyad"))
                {
                	$query->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                }
				if(request("fatura_unvan"))
                {
                	$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        $query->where("fatura_unvan","like","%".$fatura_unvan."%");
                }
				if(request("order_number"))
                {
                	$query->where("irsaliye_no","like","%".request()->input("order_number")."%");
                }
			})
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			}])
			->orderBy(request("sort","product_top"),request("direction",""))
			->get();


		return compact("products");
	}

	public function getUretimde()
	{
		$user = Auth::user();
		$sube_id = $user->sube_id;


		$products = $this->orderproduct
			->select($this->selected)
			->whereIn("status",[2])
			->where(function($query)use($sube_id){
				$query->where("cikis_tarihi","<=",date("Y-m-d",strtotime("+1 months")));

                if(request("product_name"))
                {
                	$query->where("product_name","like","%".request()->input("product_name")."%");
                }
                if(request("product_sku"))
                {
                	$query->where("product_sku","like","%".request()->input("product_sku")."%");
                }
                if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun","like","%".request()->input("mevcut_urun")."%");
                }
                if(request("product_desc"))
                {
                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
                }
                if(request("product_options"))
                {
                	$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
                }
				if(request("product_width"))
                {
                	$query->where("product_width",request()->input("product_width"));
                }
				if(request("product_height"))
                {
                	$query->where("product_height",request()->input("product_height"));
                }
				if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
			})
			->whereHas("irsaliyesi",function($query){
				if(request("siptar"))
                {
                	$query->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
                }
				if(request("bize_mesaj"))
                {
                	$query->where("bize_mesaj","like","%".request()->input("bize_mesaj")."%");
                }
				if(request("gon_adsoyad"))
                {
                	$query->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                }
				if(request("fatura_unvan"))
                {
                	$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        $query->where("fatura_unvan","like","%".$fatura_unvan."%");
                }
				if(request("order_number"))
                {
                	$query->where("irsaliye_no","like","%".request()->input("order_number")."%");
                }
			})
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			}])
			->orderBy(request("sort","siptar"),request("direction","desc"))
			->get();


		$gelecek_products = null;


		return compact("products","gelecek_products");
	}

	public function getUretilen()
	{
		$sube_id = Auth::user()->sube_id;
		$uretilmis = $this->orderproduct
				->select($this->selected)
				->where("status",8)
				->where("ek",0)
				->where(function($query) use($sube_id){

                    if(request("alici_adsoyad"))
	                {
	                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
	                }
	                if(request("semt_adi"))
	                {
	                	$query->where(function($q){
	                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
	                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
	                	});
	                }
	                if(request("kargo"))
	                {
	                	$query->where("kargo","like","%".request()->input("kargo")."%");
	                }
	                if(request("kargo_no"))
	                {
	                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
	                }
	                if(request("teslim_saati"))
	                {
	                	$query->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
	                }
	                if(request("product_name"))
	                {
	                	$query->where("product_name","like","%".request()->input("product_name")."%");
					}
	                if(request("product_desc"))
	                {
	                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
					}
					if(request("product_options"))
					{
						$query->where("product_options",request()->input("product_options"));
					}
					if(request("product_top"))
					{
						$query->where("product_top",request()->input("product_top"));
					}
					if(request("product_width"))
					{
						$query->where("product_width",request()->input("product_width"));
					}
					if(request("product_height"))
					{
						$query->where("product_height",request()->input("product_height"));
					}
					if(request("cikis_tarihi"))
					{
						$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
					}

	                if(request("top_id"))
	                {
	                	$query->where("top_id",request()->input("top_id"));
	                }

	                if(request("order_number")
                	OR request("gon_adsoyad")
                	OR request("fatura_unvan")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("siptar")
                	OR request("bize_mesaj")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1){

							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("irsaliye_no","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("bize_mesaj"))
                	   		{
			                    $query1->where("bize_mesaj","like","%".trim(request()->input("bize_mesaj"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("fatura_unvan"))
                	   		{
			                    $fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        			$query1->where("fatura_unvan","like","%".$fatura_unvan."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                    	});
                    }
                })
				->orderBy(request("sort","updated_at"),request("direction",""))
				->with(["irsaliyesi"=>function($query){
					$query->select($this->selectedIrsaliye);
				}])
				->get();

		$kismiuretim = $this->orderproduct
				->select($this->selected)
				->where("status",2)
				->where("ek",1)
				->with(["irsaliyesi"=>function($query){
					$query->select($this->selectedIrsaliye);
				}])
				->orderBy(request("sort","siptar"),request("direction","desc"))
				->get();

		$irsaliye_no = $this->getIrsaliyeNumber($sube_id ?: 1);
		return compact("uretilmis","kismiuretim","irsaliye_no");
	}

	public function getTransferde()
	{
		$user = Auth::user();
		$sube_id = $user->sube_id;

		$products = $this->orderproduct
			->select($this->selected)
			->whereIn("status",[9])
			->where(function($query)use($sube_id){
				$query->where("cikis_tarihi","<=",date("Y-m-d",strtotime("+1 months")));

                if(request("product_name"))
                {
                	$query->where("product_name","like","%".request()->input("product_name")."%");
                }
                if(request("product_sku"))
                {
                	$query->where("product_sku","like","%".request()->input("product_sku")."%");
                }
                if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun","like","%".request()->input("mevcut_urun")."%");
                }
                if(request("product_desc"))
                {
                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
                }
                if(request("product_options"))
                {
                	$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
                }
				if(request("product_width"))
                {
                	$query->where("product_width",request()->input("product_width"));
                }
				if(request("product_height"))
                {
                	$query->where("product_height",request()->input("product_height"));
                }
				if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
			})
			->whereHas("irsaliyesi",function($query){
				if(request("siptar"))
                {
                	$query->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
                }
				if(request("bize_mesaj"))
                {
                	$query->where("bize_mesaj","like","%".request()->input("bize_mesaj")."%");
                }
				if(request("gon_adsoyad"))
                {
                	$query->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                }
				if(request("fatura_unvan"))
                {
                	$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        $query->where("fatura_unvan","like","%".$fatura_unvan."%");
                }
				if(request("order_number"))
                {
                	$query->where("irsaliye_no","like","%".request()->input("order_number")."%");
                }
			})
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			}])
			->orderBy(request("sort","siptar"),request("direction","desc"))
			->get();


		$gelecek_products = null;


		return compact("products","gelecek_products");
	}

	public function getHatali()
	{
		$user = Auth::user();
		$sube_id = $user->sube_id;

		return $this->orderproduct
			->select($this->selected)
			->whereIn("status",[5,10])
			->where(function($query)use($sube_id){
				$query->where("cikis_tarihi","<=",date("Y-m-d",strtotime("+1 months")));

                if(request("product_name"))
                {
                	$query->where("product_name","like","%".request()->input("product_name")."%");
                }
                if(request("product_sku"))
                {
                	$query->where("product_sku","like","%".request()->input("product_sku")."%");
                }
                if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun","like","%".request()->input("mevcut_urun")."%");
                }
                if(request("product_desc"))
                {
                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
                }
                if(request("product_options"))
                {
                	$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
                }
				if(request("product_width"))
                {
                	$query->where("product_width",request()->input("product_width"));
                }
				if(request("product_height"))
                {
                	$query->where("product_height",request()->input("product_height"));
                }
				if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
			})
			->whereHas("irsaliyesi",function($query){
				if(request("siptar"))
                {
                	$query->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
                }
				if(request("bize_mesaj"))
                {
                	$query->where("bize_mesaj","like","%".request()->input("bize_mesaj")."%");
                }
				if(request("gon_adsoyad"))
                {
                	$query->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                }
				if(request("fatura_unvan"))
                {
                	$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        $query->where("fatura_unvan","like","%".$fatura_unvan."%");
                }
				if(request("order_number"))
                {
                	$query->where("irsaliye_no","like","%".request()->input("order_number")."%");
                }
			})
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			}])
			->orderBy(request("sort","siptar"),request("direction","desc"))
			->get();


	}


	public function getTopUrunler($top)
	{
		$sube_id = Auth::user()->sube_id;
		return $this->orderproduct
				->select($this->selected)
				->whereNotIn("status",[6])
				->where("top_id",$top->id)
				->where(function($query) use($sube_id){

                    if(request("alici_adsoyad"))
	                {
	                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
	                }
	                if(request("semt_adi"))
	                {
	                	$query->where(function($q){
	                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
	                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
	                	});
	                }
	                if(request("kargo"))
	                {
	                	$query->where("kargo","like","%".request()->input("kargo")."%");
	                }
	                if(request("kargo_no"))
	                {
	                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
	                }
	                if(request("teslim_saati"))
	                {
	                	$query->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
	                }
	                if(request("product_name"))
	                {
	                	$query->where("product_name","like","%".request()->input("product_name")."%");
					}
	                if(request("product_desc"))
	                {
	                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
					}
					if(request("product_options"))
					{
						$query->where("product_options",request()->input("product_options"));
					}
					if(request("product_top"))
					{
						$query->where("product_top",request()->input("product_top"));
					}
					if(request("product_width"))
					{
						$w = explode("x",request()->input("product_width"));
						if($w[0]){
							$query->where("product_width",$w[0]);
						}
						if(isset($w[1])){
							$query->where("product_height",$w[1]);
						}
					}
					if(request("product_height"))
					{
						$query->where("product_height",request()->input("product_height"));
					}
					if(request("cikis_tarihi"))
					{
						$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
					}

	                if(request("top_id"))
	                {
	                	$query->where("top_id",request()->input("top_id"));
	                }

	                if(request("order_number")
                	OR request("gon_adsoyad")
                	OR request("fatura_unvan")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("siptar")
                	OR request("bize_mesaj")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1){

							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("irsaliye_no","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("bize_mesaj"))
                	   		{
			                    $query1->where("bize_mesaj","like","%".trim(request()->input("bize_mesaj"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("fatura_unvan"))
                	   		{
			                    $fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        			$query1->where("fatura_unvan","like","%".$fatura_unvan."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                    	});
                    }
                })
				->with(["irsaliyesi"=>function($query){
					$query->select($this->selectedIrsaliye);
				}])
				->orderBy("customer_id")
				->get();

	}


	public function getCikista()
	{
		$sube_id = Auth::user()->sube_id;
		$uretilmis = $this->orderproduct
				->select($this->selected)
				->where("status",7)
				->where("ek",0)
				->where(function($query) use($sube_id){

                    if(request("alici_adsoyad"))
	                {
	                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
	                }
	                if(request("semt_adi"))
	                {
	                	$query->where(function($q){
	                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
	                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
	                	});
	                }
	                if(request("kargo"))
	                {
	                	$query->where("kargo","like","%".request()->input("kargo")."%");
	                }
	                if(request("kargo_no"))
	                {
	                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
	                }
	                if(request("teslim_saati"))
	                {
	                	$query->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
	                }
	                if(request("product_name"))
	                {
	                	$query->where("product_name","like","%".request()->input("product_name")."%");
					}
	                if(request("product_desc"))
	                {
	                	$query->where("product_desc","like","%".request()->input("product_desc")."%");
					}
					if(request("product_options"))
					{
						$query->where("product_options",request()->input("product_options"));
					}
					if(request("product_top"))
					{
						$query->where("product_top",request()->input("product_top"));
					}
					if(request("product_width"))
					{
						$query->where("product_width",request()->input("product_width"));
					}
					if(request("product_height"))
					{
						$query->where("product_height",request()->input("product_height"));
					}
					if(request("cikis_tarihi"))
					{
						$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
					}

					if(request("top_id"))
	                {
	                	$query->where("top_id",request()->input("top_id"));
	                }

	                if(request("order_number")
                	OR request("gon_adsoyad")
                	OR request("fatura_unvan")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("bize_mesaj")
                	OR request("siptar")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1){
							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("irsaliye_no","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("bize_mesaj"))
                	   		{
			                    $query1->where("bize_mesaj","like","%".trim(request()->input("bize_mesaj"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("fatura_unvan"))
                	   		{
			                    $fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        			$query1->where("fatura_unvan","like","%".$fatura_unvan."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                    	});
                    }
                })
				->orderBy(request("sort","siptar"),request("direction","desc"))
				->with(["irsaliyesi"=>function($query){
					$query->select($this->selectedIrsaliye);
				}])
				->get();

		$irsaliye_no = $this->getIrsaliyeNumber($sube_id ?: 1);
		return compact("uretilmis","irsaliye_no");
	}



	public function getDagitimda()
	{
		$sube_id = Auth::user()->sube_id;
		$kuryede =  $this->orderproduct
				->select($this->selected)
				->where("status",3)
				->where("ek",0)
				->where(function($query) use($sube_id){

                    if(request("alici_adsoyad"))
	                {
	                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
	                }
	                if(request("semt_adi"))
	                {
	                	$query->where(function($q){
	                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
	                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
	                	});
					}
					if(request("kargo"))
	                {
	                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
	                }
	                if(request("kargo_no"))
	                {
	                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
	                }
	                if(request("teslim_saati"))
	                {
	                	$query->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
	                }
	                if(request("product_name"))
	                {
	                	$query->where("product_name","like","%".request()->input("product_name")."%");
					}
					if(request("product_options"))
					{
						$query->where("product_options",request()->input("product_options"));
					}
					if(request("product_top"))
					{
						$query->where("product_top",request()->input("product_top"));
					}
					if(request("product_width"))
					{
						$query->where("product_width",request()->input("product_width"));
					}
					if(request("product_height"))
					{
						$query->where("product_height",request()->input("product_height"));
					}
					if(request("product_desc"))
					{
						$query->where("product_desc",request()->input("product_desc"));
					}

	                if(request("kurye_id"))
	                {
	                	$query->where("kurye_id",request()->input("kurye_id"));
	                }
	                if(request("cikis_tarihi"))
	                {
	                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
					}
					if(request("top_id"))
	                {
	                	$query->where("top_id",request()->input("top_id"));
					}

	                if(request("order_number")
                	OR request("gon_adsoyad")
                	OR request("fatura_unvan")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("bize_mesaj")
                	OR request("siptar")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1){
							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("irsaliye_no","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("bize_mesaj"))
                	   		{
			                    $query1->where("bize_mesaj","like","%".trim(request()->input("bize_mesaj"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("fatura_unvan"))
                	   		{
			                    $fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			        			$query1->where("fatura_unvan","like","%".$fatura_unvan."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                    	});
                    }
                })
				->with(["irsaliyesi"=>function($query){
					$query->select($this->selectedIrsaliye);
				}])
				->orderBy(request("sort","siptar"),request("direction","desc"))
				->get();

		$irsaliye_no = $this->getIrsaliyeNumber($sube_id ?: 1);
		return compact("kuryede","irsaliye_no");
	}

	public function getAllWithFilter($tarih1,$tarih2,$tarih,$limit)
	{
		$sube_id = Auth::user()->sube_id;
		return $this->orderproduct
			->select($this->selected)
			//->where("status",">",0)
			->where("ek",0)
			->where(function($query) use($sube_id,$tarih1,$tarih2,$tarih){
				if($tarih=='cikis_tarihi' || $tarih == 'teslim_tarihi')
				{
					$query->whereBetween($tarih,[$tarih1,$tarih2]);
				}

                if(request("alici_adsoyad"))
                {
                	$query->where("alici_adsoyad","like","%".request()->input("alici_adsoyad")."%");
                }
                if(request("semt_adi"))
                {
                	$query->where(function($q){
                		$q->where("semt_adi","like","%".request()->input("semt_adi")."%");
                		$q->orWhere("il_adi","like","%".request()->input("semt_adi")."%");
                	});
                }
                if(request("kargo_no"))
                {
                	$query->where("kargo_no","like","%".request()->input("kargo_no")."%");
                }
                if(request("teslim_saati"))
                {
                	$query->where("teslim_saati","like","%".request()->input("teslim_saati")."%");
                }

				if(request("product_options"))
				{
					$query->where("product_options",request()->input("product_options"));
				}
				if(request("product_top"))
                {
                	$query->where("product_top",request()->input("product_top"));
				}

				if(request("product_width"))
				{
					$w = explode("x",request()->input("product_width"));
					if($w[0]){
						$query->where("product_width",$w[0]);
					}
					if(isset($w[1])){
						$query->where("product_height",$w[1]);
					}
				}
                if(request("kurye_id"))
                {
                	$query->where("kurye_id",request()->input("kurye_id"));
                }
                if(request("sms"))
                {
                	$query->where("sms",request()->input("sms"));
                }
                if(request("cikis_tarihi"))
                {
                	$query->where("cikis_tarihi",date("Y-m-d",strtotime(request()->input("cikis_tarihi"))));
                }

                if(request("status"))
                {
                	$query->where("status",request()->input("status"));
                }
                if(request("kargo"))
                {
                	$query->where("kargo","like","%".request()->input("kargo")."%");
				}
				if(request("top_id"))
				{
					$query->where("top_id",request()->input("top_id"));
				}
				if(request("product_name"))
				{
					$query->where("product_name",request()->input("product_name"));
				}
				if(request("mevcut_urun"))
                {
                	$query->where("mevcut_urun",request()->input("mevcut_urun"));
                }
                if(request("iptaliade"))
                {
                	if(request()->input("iptaliade") != 'true')
                	{
                		$query->whereNotIn("status",[6]);
                	}
				}
				else
                {
                	$query->whereNotIn("status",[6]);
				}

				if(request("top_number"))
				{
					$query->whereHas("topu",function($query2){
						$query2->where("name",request("top_number"));
					});
				}

				if(request("only_top") OR auth()->id()==5) // ozdemir
				{
					$query->where("top_id",">",0);
				}


                //if query needs irsaliyesi
                if(request("order_number")
                	OR $tarih == 'siptar'
                	OR request("gon_adsoyad")
                	OR request("fatura_unvan")
                	OR request("coupon_code")
                	OR request("siparisalan")
                	OR request("payment")
                	OR request("mobil")
                	OR request("siptar")
                	OR request("payment_ok"))
                    {
                	   	$query->whereHas("irsaliyesi",function($query1)use($tarih1,$tarih2,$tarih){

                	   		if($tarih=='siptar')
							{
								$query1->whereBetween($tarih,[$tarih1,$tarih2]);
							}
							if(request("siptar"))
							{
								$query1->where("siptar",date("Y-m-d",strtotime(request()->input("siptar"))));
							}
                	   		if(request("order_number"))
                	   		{
			                    $query1->where("irsaliye_no","like","%".trim(request()->input("order_number"))."%");
                	   		}
                	   		if(request("gon_adsoyad"))
                	   		{
			                    $query1->where("gon_adsoyad","like","%".request()->input("gon_adsoyad")."%");
                	   		}
                	   		if(request("fatura_unvan"))
                	   		{
								$fatura_unvan = str_replace('ı','I',request()->input("fatura_unvan"));
			                    $query1->where("fatura_unvan","like","%".$fatura_unvan."%");
                	   		}
                	   		if(request("coupon_code"))
                	   		{
			                    $query1->where("coupon_code","like","%".request()->input("coupon_code")."%");
                	   		}
                	   		if(request("siparisalan"))
                	   		{
			 					$query1->where("siparisalan","like","%".request()->input("siparisalan")."%");
                	   		}
                	   		if(request("payment"))
                	   		{
			 					$query1->where("payment",request()->input("payment"));
                	   		}
                	   		if(request("payment_ok"))
                	   		{
			                    $query1->where("payment_ok",request()->input("payment_ok"));
                	   		}
                	   		if(request("mobil"))
                	   		{
			                    $query1->where("mobil",request()->input("mobil"));
                	   		}
                    	});
                    }
            })
			->with(["irsaliyesi"=>function($query){
				$query->select($this->selectedIrsaliye);
			},"topu"=>function($query){
				$query->select("id","name");
			},"stock"=>function($query){
				$query->select("kod","product_top","product_height");
			}])
			->orderBy(request("sort","created_at"),request("direction","desc"))
			->take($limit)
			->get();
	}

	public function setOnaylandi($op_id,$user)
	{
        $op = $this->orderproduct->find($op_id);
        $op->status = 1;
		$op->save();

        $order_id = $op->irsaliyesi->id;
        $order_number = $op->irsaliyesi->order_number;
		$ot = new Irsaliyetra();
        $ot->order_id = $order_id;
        $ot->op_id = $op->id;
        $ot->order_number = $order_number;
        $ot->tur = 'onaylandi';
        $ot->link = 'setOnaylandi';
        $ot->description = "Baskı Hazırlığına Verildi";
        $ot->user_type = 'App\\User';
        $ot->user_id = $user->id;
        $ot->kurye_id = 0;
        $ot->save();

        Event::fire('Uretildi', [['id'=>$op->id,'order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'urun'=>$op->product_name]]);
		return 1;
	}

	public function setHazirlandi($op_id,$user)
	{
        $op = $this->orderproduct->find($op_id);
        $op->status = 2;
		$op->save();

        $order_id = $op->irsaliyesi->id;
        $order_number = $op->irsaliyesi->order_number;
		$ot = new Irsaliyetra();
		$ot->op_id = $op->id;
        $ot->order_id = $order_id;
        $ot->order_number = $order_number;
        $ot->tur = 'hazirlandi';
        $ot->link = 'setHazirlandi';
        $ot->description = "Baskıya Verildi";
        $ot->user_type = 'App\\User';
        $ot->user_id = $user->id;
        $ot->kurye_id = 0;
        $ot->save();

        Event::fire('Uretildi', [['id'=>$op->id,'order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'urun'=>$op->product_name]]);
		return 1;
	}

	public function setUretildi($op_id,$user,$top_id)
	{
        $op = $this->orderproduct->find($op_id);
        $op->uretim = 1;
        $op->status = 8;
		$op->top_id = $top_id;
        $op->save();
        $order_id = $op->irsaliyesi->id;
        $order_number = $op->irsaliyesi->order_number;

		$ot = new Irsaliyetra();
        $ot->order_id = $order_id;
		$ot->op_id = $op_id;
		$ot->order_number = $order_number;
        $ot->tur = 'uretildi';
        $ot->link = 'setUretildi';
        $ot->description = "Basıldı";
        $ot->user_type = 'App\\User';
        $ot->user_id = $user->id;
        $ot->kurye_id = 0;
        $ot->save();

        Event::fire('Uretildi', [['id'=>$op->id,'order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'urun'=>$op->product_name]]);
		return 1;
	}

	public function setTransferByTop($top,$user)
	{
		$ops = $this->orderproduct
			->where("top_id",$top->id)
			->whereIn("status",[8])
			->get();

		foreach($ops as $op){
			$op->status = 9;
			$op->save();
			$order_id = $op->irsaliyesi->id;
			$order_number = $op->irsaliyesi->order_number;
			$ot = new Irsaliyetra();
			$ot->order_id = $order_id;
			$ot->op_id = $op->id;
			$ot->order_number = $order_number;
			$ot->tur = 'transfer';
			$ot->link = 'setTransfer';
			$ot->description = "Transferde";
			$ot->user_type = 'App\\User';
			$ot->user_id = $user->id;
			$ot->kurye_id = 0;
			$ot->save();
		}

        //Event::fire('Transfer', [['id'=>$op->id,'order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'urun'=>$op->product_name]]);
		return 1;
	}

	public function setTransfer($op_id,$user)
	{
		$op = $this->orderproduct->find($op_id);
        $op->status = 9;
        $op->save();
        $order_id = $op->irsaliyesi->id;
		$order_number = $op->irsaliyesi->order_number;

		$ot = new Irsaliyetra();
		$ot->order_id = $order_id;
		$ot->op_id = $op_id;
        $ot->order_number = $order_number;
        $ot->tur = 'transfer';
        $ot->link = 'setTransfer';
        $ot->description = "Transferde";
        $ot->user_type = 'App\\User';
        $ot->user_id = $user->id;
        $ot->kurye_id = 0;
        $ot->save();

        //Event::fire('Transfer', [['id'=>$op->id,'order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'urun'=>$op->product_name]]);
		return 1;
	}

	public function setCikista($user,$op_id)
	{
		$op = $this->orderproduct->findOrFail($op_id);
		$op->kurye_id = 0;
		$op->status = 7;
		$op->save();
		$order_id = $op->irsaliyesi->id;
		$order_number = $op->irsaliyesi->order_number;
		$ot = new Irsaliyetra();
		$ot->order_id = $order_id;
		$ot->op_id = $op_id;
		$ot->order_number = $order_number;
		$ot->tur = '';
		$ot->link = 'setCikista';
		$ot->description = "Çıkış Yapıldı";
		$ot->user_type = 'App\\User';
		$ot->user_id = $user->id;
		$ot->kurye_id = 0;
		$ot->save();

		if($op->irsaliyesi->bilgilendirme_sms)
		{
			//dispatch(new SendKargoSms(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad]));
		}
		if($op->irsaliyesi->bilgilendirme_email)
		{
			//dispatch(new SendKargoEmail(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad,"gon_email"=>$op->irsaliyesi->gon_email]));
		}
		//Event::fire('Cikis', [['order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number]]);

		return 1;

	}

	public function setDagitimda($user,$op_id,$kurye_id)
	{
		$op = $this->orderproduct->findOrFail($op_id);

		$op->status = 3;
		$op->kurye_id = $kurye_id;
		$op->cikis_tarihi = date("Y-m-d");
		$op->save();
		$order_id = $op->irsaliyesi->id;
		$order_number = $op->irsaliyesi->order_number;
		$ot = new Irsaliyetra();
		$ot->order_id = $order_id;
		$ot->op_id = $op_id;
		$ot->order_number = $order_number;
		$ot->tur = '';
		$ot->link = 'setDagitimda';
		$ot->description = "Çıkış Yapıldı";
		$ot->user_type = 'App\\User';
		$ot->user_id = $user->id;
		$ot->kurye_id = $kurye_id;
		$ot->save();
		if($op->irsaliyesi->bilgilendirme_sms)
		{
			dispatch(new SendKargoSms(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad]));
		}
		if($op->irsaliyesi->bilgilendirme_email)
		{
			dispatch(new SendKargoEmail(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad,"gon_email"=>$op->irsaliyesi->gon_email]));
		}
		Event::fire('Dagitildi', [['order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number]]);

		return 1;

	}

	public function setTeslimEdildi($user,$op_id,$kurye_id)
    {
        $op = $this->orderproduct->findOrFail($op_id);
		$op->status = 4;
		$op->kurye_id = $kurye_id;
		$op->teslim_tarihi = date("Y-m-d");
        $tur = 'teslim';
        $description = "Kargoya Gönderildi";
        $op->save();

        $order_id = $op->irsaliyesi->id;
		$order_number = $op->irsaliyesi->order_number;

		//dispatch(new SendTeslimSms(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad]));

		//dispatch(new SendTeslimEmail(["gon_cep"=>$op->irsaliyesi->gon_cep,"order_number"=>$order_number,"gon_adsoyad"=>$op->irsaliyesi->gon_adsoyad,"gon_email"=>$op->irsaliyesi->gon_email]));


        $ot = new Irsaliyetra();
        $ot->order_id = $order_id;
		$ot->order_number = $order_number;
		$ot->op_id = $op_id;
        $ot->tur = $tur;
        $ot->link = 'setTeslimEdildi';
        $ot->description = $description;
        $ot->user_id = $user->id;
        $ot->user_type = 'App\\User';
        $ot->kurye_id = 0;
        $ot->save();
        $tur = $op->kargo ? 'kargoda':'kuryede';

        Event::fire('TeslimEdildi', [['order_id'=>$order_id,'status'=>$op->status,'order_number'=>$order_number,'tur'=>$tur]]);
        return 1;
    }

	public function setCikmadi($op_id)
	{
		$op = $this->orderproduct->find($op_id);
		$op->status = 5;
		$op->save();

			$ot = new Irsaliyetra();
			$ot->order_id = $op->order_id;
			$ot->order_number = $op->irsaliyesi->order_number;
			$ot->op_id = $op_id;
			$ot->tur = 'cikmadi';
			$ot->link = 'setCikmadi';
			$ot->description = $op->product_name." Ürün Çıkmadı";
			$ot->user_type = 'App\\User';
			$ot->user_id = Auth::user()->id;
			$ot->kurye_id = 0;
			$ot->save();

		return 1;
	}

	public function setHatali($op_id)
	{
		$op = $this->orderproduct->find($op_id);
		$op->status = 10;
		$op->save();

			$ot = new Irsaliyetra();
			$ot->order_id = $op->order_id;
			$ot->order_number = $op->irsaliyesi->order_number;
			$ot->op_id = $op_id;
			$ot->tur = 'hatali';
			$ot->link = 'setHatali';
			$ot->description = $op->product_name." Ürün Hatalı Çıktı";
			$ot->user_type = 'App\\User';
			$ot->user_id = Auth::user()->id;
			$ot->kurye_id = 0;
			$ot->save();

		return 1;
	}

	public function setIptalEdildi($op_id)
	{
		$op = $this->orderproduct->find($op_id);
		$op->status = 6;
		$op->save();

			$ot = new Irsaliyetra();
			$ot->order_id = $op->order_id;
			$ot->op_id = $op_id;
			$ot->order_number = $op->irsaliyesi->order_number;
			$ot->tur = 'iade';
			$ot->link = 'setIptalEdildi';
			$ot->description = $op->product_name." İptal Edildi";
			$ot->user_type = 'App\\User';
			$ot->user_id = Auth::user()->id;
			$ot->kurye_id = 0;
			$ot->save();

		return 1;
	}

	public function setUretimeGeri($op_id)
	{
		$op = $this->orderproduct->find($op_id);
		$op->uretim = 0;
		$op->status = 0;
		$top = $op->top_id;
		$op->top_id = 0;
		$op->save();

		$ot = new Irsaliyetra();
		$ot->order_id = $op->irsaliyesi->id;
		$ot->op_id = $op_id;

		$ot->order_number = $op->irsaliyesi->order_number;
		$ot->tur = 'geri';
		$ot->link = 'setUretimeGeri';
		$ot->description = $op->product_name." Gelen Siparişlere Geri Gönderildi önceki top:".$top;
		$ot->user_type = 'App\\User';
		$ot->user_id = Auth::user()->id;
		$ot->kurye_id = 0;
		$ot->save();

		return 1;
	}

	public function setBasilanaGeri($op_id)
	{
		$op = $this->orderproduct->find($op_id);
		$op->uretim = 1;
		$op->status = 2;
		$op->save();

		$ot = new Irsaliyetra();
		$ot->order_id = $op->irsaliyesi->id;
		$ot->op_id = $op_id;
		$ot->order_number = $op->irsaliyesi->order_number;
		$ot->tur = 'geri';
		$ot->link = 'setBasilanaGeri';
		$ot->description = $op->product_name." Basılan Siparişlere Geri Gönderildi";
		$ot->user_type = 'App\\User';
		$ot->user_id = Auth::user()->id;
		$ot->kurye_id = 0;
		$ot->save();

		return 1;
	}

	public function barkodOkundu($irsaliye_no,$adet)
	{
		$op = $this->orderproduct->where("irsaliye_no",$irsaliye_no)->firstOrFail();

		$okunanlar = explode(",",$op->okunan);
		if(!in_array($adet,$okunanlar))
		{
			array_push($okunanlar,$adet);
			sort($okunanlar);
			$op->okunan = ltrim(implode(",",$okunanlar),",");
			$op->save();
		}
		if($op->product_qty == count(explode(",",$op->okunan))){
			if(!($op->status==2 OR $op->status==7)){
				return 1;
			}

			$op->status = 3;
			$op->save();

			$ot = new Irsaliyetra();
			$ot->order_id = $op->order_id;
			$ot->op_id = $op->id;
			$ot->order_number = $op->irsaliyesi->order_number;
			$ot->tur = "";
			$ot->link = 'setHazirda';
			$ot->description = "Sayım Tamamlandı";
			$ot->user_id = auth()->id();
			$ot->user_type = 'App\\User';
			$ot->kurye_id = 0;
			$ot->save();
		}
		return 1;
	}

    public function getIrsaliyeNumber($sube_id)
    {
        $number = Number::where('type','irsaliye')->where('sube_id',$sube_id)->first();
        if($number)
        {
            return  $number->no;
        }
        return "";
    }

}
