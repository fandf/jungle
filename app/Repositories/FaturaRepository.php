<?php
namespace App\Repositories;

use App\Product;
use App\Fatura;
use App\Faturaproduct;
use App\Order;
use App\Orderproduct;
use App\Ordertra;
use Sentry;

class FaturaRepository
{
	protected $fatura;
	protected $faturaproduct;
	protected $irsaliye;
	protected $irsaliyeproduct;
	protected $user;

	protected $selected = [

		"id",
		'order_id',
		'user_id',
		'order_number',
		'sube_kodu',
		'customer_id',
		'tax',
		'subtotal',
		'total',
		'coupon_discount',
		'discount_yuzde',
		'shipping',
		'fatura_tarihi',
		'cikis_tarihi',
		'gon_adsoyad',
		'gon_cep',
		'fatura_teslimat',
		'fatura_unvan',
		'fatura_adres',
		'vergi_dairesi',
		'vergi_no',
		'kimlik_no',
		'neden',
		'gon_adres',
		'irsaliye',
		'faturano',
		"created_at"
	];


	public function __construct(Fatura $fatura,Faturaproduct $faturaproduct,Irsaliye $irsaliye,Irsaliyeproduct $irsaliyeproduct)
	{
		$this->fatura = $fatura;
		$this->faturaproduct =  $faturaproduct;
		$this->irsaliye = $irsaliye;
		$this->irsaliyeproduct = $irsaliyeproduct;
		$this->user = Sentry::getUser();
	}

	public function findById($fatura_id)
	{
		return $this->fatura->find($fatura_id);
	}

	public function getByFaturaIdWithOp($fatura_id)
	{
		$fatura = $this->fatura->where("id",$fatura_id)->with("faturaproducts")->first();

		return $fatura;
	}


	public function getByIrsaliyeIdWithOp($order_id)
	{
		$fatura = $this->fatura->where("order_id",$order_id)->with("faturaproducts")->first();

		$irsaliye = $this->irsaliye->find($order_id);

		if($fatura)
		{
			return $this->updateFaturaFromIrsaliye($fatura,$irsaliye);
		}


		if($irsaliye)
		{
			return $this->createFaturaFromIrsaliye($irsaliye);
		}

	}


	public function getByIrsaliyeId($order_id)
	{
		return $this->fatura->where("order_id",$order_id)->first();
	}

	public function createFaturaFromIrsaliye($irsaliye)
	{
		$fatura = new $this->fatura();

		$fatura->order_id		= $irsaliye->id;
		$fatura->order_number		= $irsaliye->order_number;
		$fatura->user_id			= $irsaliye->user_id;
		$fatura->sube_kodu			= $irsaliye->sube_kodu;
		$fatura->customer_id		= $irsaliye->customer_id;
		$fatura->tax				= $irsaliye->tax;
		$fatura->subtotal			= $irsaliye->subtotal;
		$fatura->total				= $irsaliye->total;
		$fatura->coupon_discount	= $irsaliye->coupon_discount;
		$fatura->discount_yuzde		= $irsaliye->discount_yuzde;
		$fatura->shipping			= $irsaliye->shipping;
		$fatura->fatura_tarihi		= $irsaliye->fatura_tarihi;
		$fatura->cikis_tarihi		= $irsaliye->cikis_tarihi;
		$fatura->gon_adsoyad		= $irsaliye->gon_adsoyad;
		$fatura->gon_cep			= $irsaliye->gon_cep;
		$fatura->fatura_teslimat	= $irsaliye->fatura_teslimat;
		$fatura->fatura_unvan		= $irsaliye->fatura_unvan;
		$fatura->fatura_adres		= $irsaliye->fatura_adres;
		$fatura->vergi_dairesi		= $irsaliye->vergi_dairesi;
		$fatura->vergi_no			= $irsaliye->vergi_no;
		$fatura->kimlik_no			= $irsaliye->kimlik_no;
		$fatura->neden				= $irsaliye->neden;
		$fatura->gon_adres			= $irsaliye->gon_adres? $irsaliye->gon_adres : '';
		$fatura->irsaliye			= $irsaliye->irsaliye;

		$fatura->save();

		foreach ($irsaliye->orderproducts as $op) {
			$arr = $op->toArray();
			unset($arr["updated_at"]);
			unset($arr["created_at"]);
			unset($arr["order_id"]);
			unset($arr["uretim"]);
			$fatura->faturaproducts()->create($arr);
		}

		return $fatura;
	}

	public function updateFaturaFromIrsaliye($fatura,$irsaliye)
	{

		$fatura->order_id		= $irsaliye->id;
		$fatura->order_number		= $irsaliye->order_number;
		$fatura->user_id			= $irsaliye->user_id;
		$fatura->sube_kodu			= $irsaliye->sube_kodu;
		$fatura->customer_id		= $irsaliye->customer_id;
		$fatura->tax				= $irsaliye->tax;
		$fatura->subtotal			= $irsaliye->subtotal;
		$fatura->total				= $irsaliye->total;
		$fatura->coupon_discount	= $irsaliye->coupon_discount;
		$fatura->discount_yuzde		= $irsaliye->discount_yuzde;
		$fatura->shipping			= $irsaliye->shipping;
		//$fatura->fatura_tarihi		= $irsaliye->fatura_tarihi;
		$fatura->cikis_tarihi		= $irsaliye->cikis_tarihi;
		$fatura->gon_adsoyad		= $irsaliye->gon_adsoyad;
		$fatura->gon_cep			= $irsaliye->gon_cep;
		$fatura->fatura_teslimat	= $irsaliye->fatura_teslimat;
		$fatura->fatura_unvan		= $irsaliye->fatura_unvan;
		$fatura->fatura_adres		= $irsaliye->fatura_adres;
		$fatura->vergi_dairesi		= $irsaliye->vergi_dairesi;
		$fatura->vergi_no			= $irsaliye->vergi_no;
		$fatura->kimlik_no			= $irsaliye->kimlik_no;
		$fatura->neden				= $irsaliye->neden;
		$fatura->gon_adres			= $irsaliye->gon_adres? $irsaliye->gon_adres : '';
		$fatura->irsaliye			= $irsaliye->irsaliye;

		$fatura->save();

		$faturaproducts = $fatura->faturaproducts()->lists("product_id","product_id")->all();

		foreach ($irsaliye->orderproducts as $op) {

			if(in_array($op->product_id, $faturaproducts))
			{
				unset($faturaproducts[$op->product_id]);
			}

			$arr = $op->toArray();
			unset($arr["updated_at"]);
			unset($arr["created_at"]);
			unset($arr["order_id"]);
			unset($arr["uretim"]);

			$fp = Faturaproduct::where("fatura_id",$fatura->id)
							->where("product_id",$op->product_id)
							->first();
			if($fp)
			{
				$fp->update($arr);
			}
			else
			{
				$fatura->faturaproducts()->create($arr);
			}
		}

		if(count($faturaproducts))
		{
			foreach ($faturaproducts as $product_id) {
				$fatura->faturaproducts()->where("product_id",$product_id)->delete();
			}
		}

		return $fatura;
	}

	public function getAll($tarih1,$tarih2,$tarih='fatura_tarihi')
	{
		$sube_id = $this->user->sube_id;

		return $this->fatura
				->select($this->selected)
				->whereBetween($tarih,[$tarih1,$tarih2])
				->orderBy($tarih,"desc")
				->with("faturaproducts")
				->get();

	}

}
