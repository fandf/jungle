<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productimage extends Model {

	protected $fillable = [
		'product_id',
		'customer_id',
		'catalog_id',
		'name',
		'filename',
		'alt',
		'status',
	];

	public function product()
	{
		return $this->belongsTo('App\Product');
	}

	public function customer()
	{
		return $this->belongsTo('App\Customer');
	}

	public function catalog()
	{
		return $this->belongsTo('App\Catalog');
	}

}
