<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model {

	use SoftDeletes;

    protected $table = 'customers';

    protected $guarded = array('id');


    public static $rules = [
		'company_name' => 'required',
		'unvan' => 'required',
	];

	public function adresler()
	{
		return $this->hasMany("App\Adres");
	}
	public function orders()
	{
		return $this->hasMany("App\Order");
	}
	public function productimages()
	{
		return $this->hasMany("App\Productimage");
	}
	public function user()
	{
		return $this->belongsTo("App\User");
	}

}
