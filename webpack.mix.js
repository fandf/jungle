let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/gonderi.js', 'public/js')
   .js('resources/assets/js/bayi.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .version();
