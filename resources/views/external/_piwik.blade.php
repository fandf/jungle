<script type="text/javascript">
  var _paq = _paq || [];
  @if (!Auth::guest())
  _paq.push(['setUserId', '<?=Auth::user()->email;?>']);
  @endif
  _paq.push(['trackPageView']);_paq.push(['enableLinkTracking']);
  (function() {
    var u="//analitik.hayatbilisim.com/";_paq.push(['setTrackerUrl', u+'piwik.php']);_paq.push(['setSiteId', 'l3zqj1BoBE']);var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>