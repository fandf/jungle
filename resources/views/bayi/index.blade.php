@extends('layouts.bayi')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <tum-page></tum-page>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript">
var printHtml = function (html) {
    $("iframe#print").remove();
    var hiddenFrame = $('<iframe id="print" height="0" width="0"></iframe>').appendTo('body')[0];
    var htmlDocument = "<!doctype html>"+
                "<html>"+
                    '<body onload="print();">' + // Print only after document is loaded
                        html +
                    '</body>'+
                "</html>";
    var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
    doc.write(htmlDocument);
    doc.close();
};

var durumlar = <?=json_encode(config("ayar.durumlar"));?>;
var labels = <?=json_encode(config("ayar.labels"));?>;
var payments = <?=json_encode(config("ayar.payments"));?>;
var kartlar = <?=json_encode(config("ayar.kartlar"));?>;
var nedenler = <?=json_encode(config("ayar.nedenler"));?>;
var colors = <?=json_encode(config("ayar.colors"));?>;
var api_link = '<?=config("site.api_link");?>';
var pusher_channel = '<?=env("PUSHER_CHANNEL");?>';
var user = {id:<?=$user->id;?>,name:'<?=$user->name;?>'};
var tarih1 = '<?=date("d-m-Y");?>';
var tarih2 = '<?=date("d-m-Y");?>';
</script>
<script type="text/javascript" src="{{mix('js/bayi.js')}}"></script>
@endsection
