@extends("layouts.bayi")

@section("content")

    <div class="row">
        <div class="col-md-12">
            <bayi-page></bayi-page>
        </div>
    </div>

@endsection

@section("scripts")

<script type="text/javascript">

var kanallar = <?=json_encode($kanallar);?>;
var api_link = '<?=config("site.api_link");?>';
var pusher_channel = '<?=env("PUSHER_CHANNEL");?>';
var user = {id:<?=$user->id;?>,name:'<?=$user->name;?>'};
var tarih1 = '<?=date("01-m-Y");?>';
var tarih2 = '<?=date("d-m-Y");?>';
var products = <?=json_encode($products);?>;
var customer = <?=json_encode($customer);?>;
function initialState (){
  return {
    order:{
        bize_mesaj:"",
        gon_adsoyad:customer.name,
        gon_cep:customer.mobile_phone,
        fatura_unvan:customer.company_name,
        fatura_adres:customer.address,
        vergi_dairesi:customer.vergi_dairesi,
        vergi_no:customer.vergi_no,
        siparisalan:"cari",
        alici_firma:customer.company_name,
        alici_adsoyad:customer.name,
        alici_telefon:customer.mobile_phone,
        il_adi:customer.city,
        semt_adi:customer.semt,
        alici_adres:customer.address,
        alici_adrestarifi:"",
        teslim_tarihi:"{{date("d-m-Y",strtotime("+5 weekdays"))}}"
    },
    orderproducts:[{
        product:products[0],
        product_sku:products[0]["sku"],
        product_name:products[0]["name"],
        toplar:[133,150,160,175],
        product_image:"",
        product_top:"133",
        product_width:"133",
        product_height:"",
        product_options:'',
        product_price:"",
        product_tax:"",
        product_qty:1,
        cikis_tarihi:"",
        mevcut_urun:"",
        uploadList:[],
        fileList:[]
    }],
    api_link:window.api_link,
    user:window.user,
    is_loading:false,
    yukleme_var:false,
    kanallar:window.kanallar,
    errors:{},
    products:window.products,
    customers:[customer],
    subcustomers:[customer],
    customer:customer,
    subcustomer:customer,
    productimages:<?=json_encode($images);?>,
    target: '/bayi/baski',
    msg: 'Görsel yükle',
  }
}
</script>
<script type="text/javascript" src="{{mix('js/bayi.js')}}"></script>
@endsection
