@extends('layouts.kurye')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <gonderi-page></gonderi-page>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript">

var api_link = '<?=config("site.api_link");?>';
var pusher_channel = '<?=env("PUSHER_CHANNEL");?>';
var user = {id:<?=$user->id;?>,name:'<?=$user->name;?>',sube_id:<?=$user->sube_id;?>};
</script>
<script type="text/javascript" src="{{mix('js/gonderi.js')}}"></script>
@endsection
