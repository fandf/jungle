<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Jungle') }}</title>

    <!-- Styles -->
    <link href="{{mix("css/app.css")}}" rel="stylesheet">
    <link href="/css/vue-select.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Jungle = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top black-bg header">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}" style="padding: 0;">
                        <img src="/images/logo.png" class="img" width="160">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                    @if (!Auth::guest())
                    @if(Auth::user()->can("siparis-onay"))
                        <li{!!(request()->segment(1)=='order' AND request()->segment(2)=='create')?' class="active"':''!!}><a href="/order/create">Yeni Sipariş</a></li>
                        <li{!!request()->segment(1)=='onay'?' class="active"':''!!}><a href="/onay">Gelen Siparişler</a></li>
                        @endif
                        @if(Auth::user()->can("baski-hazirlik"))
                        <li{!!request()->segment(1)=='hazirlik'?' class="active"':''!!}><a href="/hazirlik">Hazırlık</a></li>
                        @endif
                        @if(Auth::user()->can("baski"))
                        <li{!!request()->segment(1)=='uretimde'?' class="active"':''!!}><a href="/uretimde">Baskıda</a></li>
                        <li{!!request()->segment(1)=='uretilen'?' class="active"':''!!}><a href="/uretilen">Basılmış</a></li>
                        <li{!!request()->segment(1)=='transfer'?' class="active"':''!!}><a href="/transfer">Transferde</a></li>
                        <li{!!request()->segment(1)=='cikislar'?' class="active"':''!!}><a href="/cikislar">Barkodlanmış</a></li>
                        @endif
                        <li{!!request()->segment(1)=='dagitimda'?' class="active"':''!!}><a href="/dagitimda">Hazırlanmış</a></li>
                        @if(Auth::user()->can("hata-takip"))
                        <li{!!request()->segment(1)=='hatali'?' class="active"':''!!}>
                            <a href="/hatali" style="display:flex;">
                                @if($hatali)
                                <img src="/images/alert.gif" width="30" style="margin-top:-8px;">
                                @endif
                                &nbsp;Hatalı
                                @if($hatali)
                                ({{$hatali}})
                                @endif

                            </a>
                        </li>
                        @endif
                        <li{!!request()->segment(1)==''?' class="active"':''!!}><a href="/">Raporlar</a></li>
                        @if(Auth::user()->can("fatura-bakabilir"))
                        <li{!!request()->segment(1)=='fatura'?' class="active"':''!!}><a href="/fatura">Faturalar</a></li>
                        @endif
                    @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right hidden-sm">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Giriş</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/urun">Ürünler</a></li>
                                    <li><a href="/toplar">Toplar</a></li>
                                    @if(Auth::user()->can("analiz"))
                                    <li><a href="/analiz">Analiz</a></li>
                                    <li><a href="/cari">Cariler</a></li>
                                    <li><a href="/bayi_giris">Bayi Kullanıcılar</a></li>
                                    <li><a href="/kurye">Teslimat</a></li>
                                    @endif
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Çıkış
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <section class="wrapper site-min-height">
        @yield('content')
        </section>


    </div>
    <ul class="nav navbar-nav visible-sm">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Giriş</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/cari">Cariler</a></li>
                        <li><a href="/kurye">Kuryeler</a></li>
                        <li>
                            <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                Çıkış
                            </a>

                        </li>
                    </ul>
                </li>

            @endif
        </ul>
    <footer class="site-footer">
        <div class="text-center">
            2018 ® Hayat Bilişim
            <a href="#" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
    </footer>
    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    @yield('scripts')

    @if(env("APP_ENV")=="production")
        @include("external._piwik")
    @endif
</body>
</html>
