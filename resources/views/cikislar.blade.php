@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <cikislar-page></cikislar-page>
        </div>
    </div>

@endsection
@section("scripts")
<script type="text/javascript">
var printHtml = function (html) {
    $("iframe#print").remove();
    var hiddenFrame = $('<iframe id="print" height="0" width="0"></iframe>').appendTo('body')[0];
    var htmlDocument = "<!doctype html>"+
                "<html>"+
                    '<body onload="print();">' + // Print only after document is loaded
                        html +
                    '</body>'+
                "</html>";
    var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
    doc.write(htmlDocument);
    doc.close();
};
var kuryeler = <?=json_encode($kuryeler);?>;
var durumlar = {1:"\u0130\u015fleme Al\u0131nd\u0131",0:"Onaylanmad\u0131",2:"\u00dcretildi",3:"Da\u011f\u0131t\u0131mda",4:"Teslim Edildi",5:"\u0130ade Edildi",6:"\u0130ptal Edildi",7:"Kargoya Verildi",8:"K\u0131smi \u00dcretilmi\u015f"};
var labels = {1:"label-info",2:"label-warning",3:"label-danger",4:"label-success",5:"label-info",6:"label-info",7:"label-info",8:"label-warning"};
var colors = <?=json_encode(config("ayar.colors"));?>;
var payments = <?=json_encode(config("ayar.payments"));?>;
var kartlar = <?=json_encode(config("ayar.kartlar"));?>;
var top_names = <?=json_encode($top_names);?>;
var nedenler = {
    "bireysel": "BİREYSEL",
    "kurumsal": "KURUMSAL"
};
var api_link = '<?=config("site.api_link");?>';
var pusher_channel = '<?=env("PUSHER_CHANNEL");?>';
var user = {id:<?=$user->id;?>,name:'<?=$user->name;?>',sube_id:<?=$user->sube_id;?>,onay:<?=$user->can("siparis-onay")?1:0;?>,indirim:<?=$user->can("indirim-yapabilir")?1:0;?>,urun:<?=$user->can("urun-ekleyebilir")?1:0;?>,iade:<?=$user->can("iade-yapabilir")?1:0;?>};
var secimData={order_number:"",bize_mesaj:"",semt_adi:"",gon_adsoyad:"",alici_adsoyad:"",product_name:"",teslim_saati:"",kargo:"",siparisalan:"",payment:"",payment_ok:"",sort:"updated_at",direction:""};
</script>
<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
@endsection
