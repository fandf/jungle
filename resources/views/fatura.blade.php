@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <fatura-page></fatura-page>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript">
var durumlar = <?=json_encode(config("ayar.durumlar"));?>;
var labels = <?=json_encode(config("ayar.labels"));?>;
var payments = <?=json_encode(config("ayar.payments"));?>;
var kartlar = <?=json_encode(config("ayar.kartlar"));?>;
var nedenler = <?=json_encode(config("ayar.nedenler"));?>;
var colors = <?=json_encode(config("ayar.colors"));?>;
var kuryeler = <?=json_encode($kuryeler);?>;
var api_link = '<?=config("site.api_link");?>';
var pusher_channel = '<?=env("PUSHER_CHANNEL");?>';
var user = {id:<?=$user->id;?>,name:'<?=$user->name;?>',sube_id:<?=$user->sube_id;?>,onay:<?=$user->can("siparis-onay")?1:0;?>,indirim:<?=$user->can("indirim-yapabilir")?1:0;?>,urun:<?=$user->can("urun-ekleyebilir")?1:0;?>,iade:<?=$user->can("iade-yapabilir")?1:0;?>};
var rawprinter = '<?=$user->printer;?>';

</script>
<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
@endsection
