<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
    <title>Barkodlar</title>
    <style>
        *,body,html{padding:0;margin:0;}

        @page
        {
            size: 210mm 297mm;/* this affects the margin in the printer settings */
            margin: 1mm 5mm 1mm 5mm;
        }

        body
        {
            /* this affects the margin on the content before sending to printer */
            margin: 0px;
        }
        td{
            padding:0;
        }
        div.Section1 {
            width:21cm;
            height:270mm;
        }
        img{
            padding:0;
        }
        @media print{
            .kart {display:none}
            tr.page-break  { display: block; page-break-before: always; }
        }
    </style>
</head>
<body>

<div class="Section1">

    <div style="position:absolute;width:21cm;margin:0;font-size:12px;">
    <table border=0 cellpadding="0"  cellspacing="0" style="width:100%;margin-left:5mm">

        <tr>


    @for($i=1;$i<($ilk_baski);$i++)
         @if($i>0 AND $i%3==1)
            </tr><tr>
        @endif
         <td style="margin:0;margin:0;padding:0;">
            <table  border="0" cellpadding=0 cellspacing=0 style="width:8cm;padding-top:2mm;padding-bottom:11mm;margin-top:4mm;margin-bottom-4mm;margin-right:10mm;overflow:hidden;">
                <tbody>
                <tr>
                    <td style="text-align:center;">
                       &nbsp;
                        <br>&nbsp;
                    </td>
                    <td style="padding:0;text-align:center;">
                        <img src="/images/white.png" style="height:20mm;">
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><strong>&nbsp;.</strong> </td>
                </tr>
                <tr>
                    <td colspan="2"><strong>&nbsp;.</strong> </td>
                </tr>
                <tr>
                    <td colspan="2"><strong>&nbsp;.</strong> </td>
                </tr>
                </tbody>
            </table>
        </td>
    @endfor

    <?php $key=0;?>
    @foreach($ops as $op)
    @for($adet=1;$adet<=$op->product_qty;$adet++)
        @if($key+($i)>0 AND ($key+($i-1))%3==0)
            </tr><tr>
        @endif
        @if(($i+$key)>24 AND ($i+$key)%24==1)
            </tr>
            </table>
            </div>
            <p style="page-break-after: always;">&nbsp;</p>
            <p style="page-break-before: always;">&nbsp;</p>
            <div style="position:absolute;width:21cm;margin:0;font-size:12px;">
            <table border=0 cellpadding="0"  cellspacing="0" style="width:100%;margin-left:5mm">
            <tr>
        @endif
            <td style="margin:0;margin:0;padding:0;">
                <table  border="0" cellpadding=0 cellspacing=0 style="width:8cm;padding-top:1mm;padding-bottom:11mm;margin-top:4mm;margin-bottom-4mm;margin-right:10mm;overflow:hidden;">
                    <tbody>
                    <tr>
                        <td style="text-align:center;border:1px solid black;">
                            <img src="{!!\PicoPrime\BarcodeGen\BarcodeGen::generate([$op->irsaliye_no."-".$key, 50, 'horizontal', 'code128', 1])->encode('data-url')!!}" alt="barcode">
                            <br>#{!!$op->irsaliye_no."-".$adet!!}
                        </td>
                        <td style="padding:0;text-align:center;border:1px solid black;">
                        @if(config("app.env")=="production")
                            <img src="{{$op->product_image}}" style="height:20mm;">
                        @else
                            <img src="/uploads/org/1545042457s-l1600 (1).jpg" style="height:20mm;">
                        @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border:1px solid black;"><strong>Ürün Kodu :</strong> {{$op->mevcut_urun}}  {{$op->product_desc}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border:1px solid black;"><strong>Ebat :</strong> {{$op->product_options}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border:1px solid black;"><strong>Firma :</strong> {{$op->irsaliyesi->fatura_unvan}}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
    <?php $key++;?>
    @endfor
    @endforeach
        </tr>
    </table>
</div>
</body>
</html>
