<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Tatlı Meyve</title>
    <style type="text/css">

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}


body {
  position: relative;
  width: 21cm;
  height: 14.85cm;
  margin: 0 auto;
  color: #001028;
  /*background: #FFFFFF url("/img/irsaliye.jpg") no-repeat top left;*/
  font-family: 'Open Sans', sans-serif;
  font-size: 12px;
}

header {
  padding: 10px 0;
  margin: 0 0 15px;
}



table {
  width: 90%;
  border-spacing: 0;
  margin-bottom: 0;
}


table td {
  text-align: right;
}

table th {

  text-align: center;
  white-space: nowrap;
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;
}

.page-break {
    page-break-after: always;
}

    </style>
  </head>
  <body>
    <header class="clearfix">
      <table>
          <tr>
            <td width="75%">&nbsp;</td>
            <td width="25%" class="service">{!!date("d.m.Y")!!}</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="service">{!!date("d.m.Y",strtotime($op->cikis_tarihi))!!}</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="service">{!!date("d.m.Y",strtotime($op->irsaliyesi->siptar))!!}</td>
          </tr>
          <tr>
            <td>
              &nbsp;
            </td>
            <td class="service">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!date("H:i")!!}</td>
          </tr>
          <tr>
            <td class="service">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {!!date("d.m.Y")!!}

              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {!!$op->teslim_saati!!}
            </td>
            <td class="service">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!$op->irsaliyesi->order_number!!}</td>
          </tr>

      </table>

   </header>

  <table>
    <tr>
      <td class="service" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!$op->product_name!!}</td>
      <td class="unit" width="10%">{!!$op->product_qty!!}</td>
      <td width="10%">&nbsp;</td>
      <td class="service" width="30%">{!!$op->irsaliyesi->gon_adsoyad!!}</td>
      <td class="service">{!!$op->irsaliyesi->gon_cep!!}</td>
    </tr>
    <?php $i = 0;?>
    @if($i==0)
    <tr>
      <td class="service" width="30%">&nbsp;</td>
      <td class="unit" width="10%">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td class="service" width="20%">&nbsp;</td>
      <td class="service">&nbsp;</td>
    </tr>
    <?php $i++;?>
    @endif
    @if($i==1)
    <tr>
      <td class="service" width="30%">&nbsp;</td>
      <td class="unit" width="10%">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td class="service" width="20%">{!!$op->alici_adsoyad!!}</td>
      <td class="service">{!!$op->alici_telefon!!}</td>
    </tr>
    @endif


  </table>

  <br/>
  <br/>
  <br/>
  <br/>
  <table>
    <tr>
      <td width="20%"></td>
      <td class="service">{!!$op->alici_firma!!} {!!$op->alici_adres!!} {!!$op->semt_adi." ".$op->id_adi!!}</td>
    </tr>
    <tr>
      <td width="20%"></td>
      <td class="service"> {!!$op->alici_adrestarifi!!}</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td width="20%"></td>
      <td class="service"></td>
    </tr>
    <tr>
      <td width="20%"></td>
      <td class="service">
        {!!$op->irsaliyesi->bize_mesaj!!}
        @if($op->irsaliyesi->payment=='kapida')
          <br/><strong>KAPIDA ÖDEME YAPILACAK</strong>
        @endif
      </td>
    </tr>
  </table>

  </body>
</html>
