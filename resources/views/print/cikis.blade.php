<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
    <title>Çıkışlar</title>
    <style>
        *,body,html{padding:0;margin:0;}
        body{width:21cm;
             height:275mm;
        }
        td{
            padding:5px;
        }
        div.Section1 {
            width:21cm;
            height:270mm;
            margin:auto;
        }
        @media print{
            .kart {display:none}
            .page-break  { page-break-before: always; }
        }
    </style>
</head>
<body>
<?php $key=0;?>
<div class="Section1">

    <table border="1" cellpadding="0" cellspacing="0" style="width:100%;">
        <tr>
            <th>Sipariş Tarihi</th>
            <th>Resim</th>
            <th>Kod</th>
            <th>Ölçü</th>
            <th>Adet</th>
            <th>Kalite</th>
            <th>Fatura</th>
        </tr>
    <?php $miktar = 0;$m2toplam=0;?>
    @foreach($ops as $op)
    <?php $miktar+= $op->product_qty;?>
        <tr>
            <td>
               {{date("d.m.Y",strtotime($op->irsaliyesi->siptar))}}<br>
               #{!!$op->irsaliye_no!!}
            </td>

            <td>
                 @if($op->product_image)
                    <img src="{{$op->product_image}}" height="82">
                @endif
            </td>
            <td>
                {{$op->mevcut_urun}}
            </td>
            <td>
                <?php

                    $e1 = floatval($op->product_width)?:0;
                    $e2 = floatval($op->product_height)?:0;
                    $m2 = $e1*$e2/10000;
                    $m2toplam +=$m2;
                ?>
                {{$op->product_width}}x{{$op->product_height}}  ({{$m2}})

            </td>
            <td align="center">
                {{$op->product_qty}}
            </td>
             <td>
                {{$op->product_name}}
            </td>
            <td>
                {{$op->irsaliyesi->fatura_unvan?:$op->irsaliyesi->gon_adsoyad}}
            </td>

        </tr>

          @if($key%11==10 AND $key>0)
          </table>
          <div class="page-break"></div>
          <table border="1" cellpadding="0" cellspacing="0" style="width:100%;">
            <tr>
                <th>Sipariş Tarihi</th>
                <th>Resim</th>
                <th>Kod</th>
                <th>Ölçü</th>
                <th>Adet</th>
                <th>Kalite</th>
                <th>Fatura</th>
            </tr>

          @endif
<?php $key++;?>

    @endforeach
            <tr>
                <th colspan="3"></th>
                <th>{{$m2toplam}}</th>
                <th>{{$miktar}}</th>
                <th colspan="2"></th>
            </tr>
    </table>
</div>
</body>
</html>
