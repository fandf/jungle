<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Çıkışlar</title>
</head>
<body>
<?php $key=0;?>
<div class="Section1">

    <table>
        <tr>
            <th>Sipariş Tarihi</th>
            <th>İrsaliye No</th>
            <th>Kod</th>
            <th>Ölçü</th>
            <th>Adet</th>
            <th>Kalite</th>
            <th>Fatura</th>
        </tr>
    <?php $miktar = 0;$m2toplam=0;?>
    @foreach($ops as $op)
    <?php $miktar+= $op->product_qty;?>
        <tr>
            <td>
               {{date("d.m.Y",strtotime($op->irsaliyesi->siptar))}}<br>
            </td>
            <td>
                #{!!$op->irsaliye_no!!}
            </td>
            <td>
                {{$op->mevcut_urun}}
            </td>
            <td>
                <?php

                    if($op->stock){
                        $m2 = $op->product_qty * $op->stock->product_top * $op->stock->product_height / 10000;
                    }else{
                        $m2 = $op->product_qty * $op->product_top * $op->product_height / 10000;
                    }
                    $m2toplam +=$m2;
                ?>
                {{$op->product_width}}x{{$op->product_height}}  ({{$m2}})

            </td>
            <td align="center">
                {{$op->product_qty}}
            </td>
             <td>
                {{$op->product_name}}
            </td>
            <td>
                {{$op->irsaliyesi->fatura_unvan?:$op->irsaliyesi->gon_adsoyad}}
            </td>

        </tr>
    @endforeach
        <tr>
            <th colspan="3"></th>
            <th>{{$m2toplam}}</th>
            <th>{{$miktar}}</th>
            <th colspan="2"></th>
        </tr>
    </table>
</div>
</body>
</html>
