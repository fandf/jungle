<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Nefis Demet</title>
    <style type="text/css">


.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
}
@page {
    size: A5;
    margin: 0;
}
html {
    /* off-white, so body edge is visible in browser */
    background: #eee;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}

body {
  position: relative;
  width: 14.8cm;  
  height: 21cm; 
  margin:0 auto;
  color: #001028;
  
  
 background: url("/images/fatura.png") no-repeat top left;
  -webkit-background-size: contain;
  -moz-background-size: contain;
  -o-background-size: contain;
  background-size: contain;

  font-family: 'Open Sans', sans-serif;
  font-size: 10px;
}
    </style>
  </head>
  <body>
  
<div style="position:absolute;margin:175px 340px 10px 30px;">

  {!!$irsaliye->fatura_unvan ? $irsaliye->fatura_unvan : $irsaliye->gon_adsoyad!!}<br>

  {!!$irsaliye->fatura_adres ? $irsaliye->fatura_adres : $irsaliye->gon_adres!!}
    
</div>


<div style="position:absolute;margin:280px 10px 10px 120px;line-height: 1em;">

  {!!$irsaliye->vergi_dairesi ? $irsaliye->vergi_dairesi : "." !!}<br>

  {!!$irsaliye->vergi_no ? $irsaliye->vergi_no : $irsaliye->kimlik_no!!}
    
</div>

<div style="position:absolute;margin: 193px 0 0 445px;font-size: 10px;line-height: 1.8em;">
    {!!date("d.m.Y",strtotime($irsaliye->siptar))!!}<br>
    {!!date("d.m.Y",strtotime($irsaliye->siptar))!!}<br>
    {!!$irsaliye->irsaliye_no!!}<br>
    {!!$irsaliye->order_number!!}<br>
    {{--date("H:i",strtotime($irsaliye->updated_at))--}}
</div>
<?php $matrah8 = 0;$kdv8=0;?>
<?php $matrah18 = 0;$kdv18=0;?>
<div style="position: absolute;margin:360px 20px 10px 10px;line-height: 1em;font-size: 11px;">

   <table style="width: 500px;">

      @foreach($irsaliye->orderproducts as $op)
      <tr>
        <td  class="service" width="10%" style="font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!$op->product_sku!!}</td>
        <td  class="service" width="40%" style="font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!$op->product_name!!}</td>
        <td class="unit" width="10%" style="font-size: 10px;">AD</td>
        <td width="25%" align="center" style="font-size: 10px;">{!! 1*$op->product_qty !!}</td>
        <td width="15%" align="right" style="font-size: 10px;">{!! number_format($op->product_price,2)!!}</td>
      </tr>
      <?php
      $kdv8+= ($op->product_tax==8) ? 0.08 * $op->product_price*$op->product_qty *(100-$irsaliye->discount_yuzde)/100 : 0;
      $kdv18+= ($op->product_tax==18) ? 0.18 * $op->product_price*$op->product_qty *(100-$irsaliye->discount_yuzde)/100 : 0;
      $matrah8 += ($op->product_tax==8) ? $op->product_price*$op->product_qty *(100-$irsaliye->discount_yuzde)/100 : 0;
      $matrah18 += ($op->product_tax==18) ? $op->product_price*$op->product_qty *(100-$irsaliye->discount_yuzde)/100 : 0;
      ?>
      @endforeach
     
  </table> 
</div>

<div style="position: absolute;margin:510px 40px 10px 30px;line-height: 1.3em;font-size: 12px;">
  <table style="width: 480px;">
    <tr>
      <td width="60%"></td>
      <td width="20%"></td>
      <td width="20%"></td>
    </tr>
    @if($irsaliye->coupon_discount>0)
    <tr>
      <td></td>
      <td style="font-size: 10px;">İndirim</td>
      <td align="right" style="font-size: 10px;">-{{number_format($irsaliye->coupon_discount,2)}}</td>
    </tr>
    @endif
    <tr>
      <td></td>
      <td style="font-size: 10px;">Ara Toplam</td>
      <td align="right" style="font-size: 10px;">{{number_format($irsaliye->subtotal,2)}}</td>
    </tr>
    <tr>
      <td></td>
      <td style="font-size: 10px;">Teslimat</td>
      <td align="right" style="font-size: 10px;">{{number_format($irsaliye->shipping-$irsaliye->shipping/1.18,2)}}</td>
    </tr>
    <tr>
      <td style="font-size: 10px;">Matrah(%8) {{number_format($matrah8,2)}}</td>
      <td style="font-size: 10px;">KDV (%8)</td>
      <td align="right" style="font-size: 10px;">{{number_format($kdv8,2)}}</td>
    </tr>
    <tr>
      <td style="font-size: 10px;">Matrah(%18) {{number_format($matrah18,2)}}</td>
      <td style="font-size: 10px;">KDV (%18)</td>
      <td align="right" style="font-size: 10px;">{{number_format($kdv18+$irsaliye->shipping/1.18,2)}}</td>
    </tr>
    <tr>
      <td></td>
      <td style="font-size: 10px;">Genel Toplam</td>
      <td align="right" style="font-size: 10px;"><strong>{{number_format($irsaliye->total+$irsaliye->shipping,2)}}</strong></td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" style="font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{money_tr_string($irsaliye->total+$irsaliye->shipping)}}</td>
    </tr>
  </table>
  
</div>

  </body>
</html>