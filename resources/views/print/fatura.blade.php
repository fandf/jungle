<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Nefis Demet</title>
    <style type="text/css">


.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
}
@page {
    size: A5 landscape;
    margin: 0 0 -40px 0;
}
@media print {
  html, body {
    width: 210mm;
    height: 148mm;
  }
}
html {
    /* off-white, so body edge is visible in browser */
    background: #eee;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}
.break{
  page-break-after: avoid;
}
body {
  position: absolute;
  width: 20.5cm;  
  height: 14.6cm; 
  margin:0 auto;
  color: #001028;
  font-family: 'Open Sans', sans-serif;
  font-size: 11px;

}
 @media print {
        html, body {
            border: 1px solid white;
            height: 99%;
            page-break-after: avoid;
            page-break-before: avoid;
        }
    }
    </style>
  </head>
  <body>
  
<div style="position:absolute;margin:4cm 12cm 8cm 1.5cm;font-size: 14px;">

  {!!$irsaliye->fatura_unvan ? $irsaliye->fatura_unvan : $irsaliye->gon_adsoyad!!}<br>

  {!!$irsaliye->fatura_adres ? $irsaliye->fatura_adres : $irsaliye->gon_adres!!}
    
</div>


<div style="position:absolute;margin:6.5cm 11cm 7.5cm 4.5cm;line-height: 1em;">

  {!!$irsaliye->vergi_dairesi ? $irsaliye->vergi_dairesi : "." !!}
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  {!!$irsaliye->vergi_no ? $irsaliye->vergi_no : $irsaliye->kimlik_no!!}
    
</div>

<div style="position:absolute;margin: 4cm 0 0 17cm;font-size: 11px;line-height: 1.6em;">
    {!!date("d.m.Y",strtotime($irsaliye->siptar))!!}<br>
    {!!date("d.m.Y",strtotime($irsaliye->siptar))!!}<br>
    {!!$irsaliye->irsaliye_no!!}<br>
    {!!$irsaliye->order_number!!}<br>
    {{--date("H:i",strtotime($irsaliye->updated_at))--}}
</div>
<?php $matrah8 = 0;$kdv8=0;?>
<?php $matrah18 = 0;$kdv18=0;?>
<div style="position: absolute;margin:8cm 1.5cm 4cm 1.5cm;line-height: 1em;font-size: 11px;">

   <table style="width: 17.5cm;">

      @foreach($irsaliye->orderproducts as $op)
      <tr>
        <td  class="service" width="2cm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!$op->product_sku!!}</td>
        <td  class="service" width="8cm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!!$op->product_name!!}</td>
        <td class="unit" width="1.5cm">AD</td>
        <td width="3cm" align="center">{!! 1*$op->product_qty !!}</td>
        <td width="3cm" align="right">{!! number_format($op->product_price,2)!!}</td>
      </tr>
      <?php
      $discount = $op->ek == 1 ? 1 :  (100-$irsaliye->discount_yuzde)/100;
      $kdv8+= ($op->product_tax==8) ? 0.08 * $op->product_price*$op->product_qty * $discount : 0;
      $kdv18+= ($op->product_tax==18) ? 0.18 * $op->product_price*$op->product_qty * $discount : 0;
      $matrah8 += ($op->product_tax==8) ? $op->product_price*$op->product_qty * $discount : 0;
      $matrah18 += ($op->product_tax==18) ? $op->product_price*$op->product_qty * $discount : 0;
      $matrah18 += $irsaliye->shipping / 1.18;
      ?>
      @endforeach
     
  </table>
  
</div>

<div style="position: absolute;margin:9.5cm 11cm 5cm 1.5cm;line-height: 1.4em;font-size: 11px;">
  Matrah(%8) {{number_format($matrah8,2)}}<br>
  Matrah(%18) {{number_format($matrah18,2)}}<br><br>
 
</div>
<div style="position: absolute;margin:11.5cm 11cm 2cm 1.5cm;line-height: 1.4em;font-size: 11px;">
  {{money_tr_string($irsaliye->total+$irsaliye->shipping)}}
</div>
<div style="position: absolute;margin:11cm 1.5cm 1.5cm 16cm;line-height: 1em;font-size: 10px;">
  <table style="width: 3cm;line-height: 1em;font-size: 10px;">
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%">&nbsp;</td>
    </tr>
    @if($irsaliye->coupon_discount>0)
    <tr>
      <td align="right">İndirim</td>
      <td align="right">-{{number_format($irsaliye->coupon_discount,2)}}</td>
    </tr>
    @endif
    <tr>
      <td align="right">Ara Toplam</td>
      <td align="right">{{number_format($irsaliye->subtotal,2)}}</td>
    </tr>
    <tr>
      <td align="right">Teslimat</td>
      <td align="right">{{number_format($irsaliye->shipping/1.18,2)}}</td>
    </tr>
    <tr>
      <td align="right">KDV (%8)</td>
      <td align="right">{{number_format($kdv8,2)}}</td>
    </tr>
    <tr>
      <td align="right">KDV (%18)</td>
      <td align="right">{{number_format($kdv18+($irsaliye->shipping-$irsaliye->shipping/1.18),2)}}</td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td align="right"><strong>{{number_format($irsaliye->total+$irsaliye->shipping,2)}}</strong></td>
    </tr>
  </table>
  
</div>
  </body>
</html>
