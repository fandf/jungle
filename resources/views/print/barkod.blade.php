<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
    <title>Barkodlar</title>
    <style>
        *,body,html{padding:0;margin:0;}

        @page
        {
            size: 210mm 297mm;/* this affects the margin in the printer settings */
            margin: 0 2mm 0 2mm;
        }

        body
        {
            /* this affects the margin on the content before sending to printer */
            margin: 0px;
        }

        img{
            padding:0;
        }
        @media print{
            .kart {display:none}
            .page-break  { page-break-before: always; }
        }

        .passcodeCell {
            width: 68mm;
            height: 37.4mm;
            /*border-style: dashed;*/
            align-content: center;
            position: relative;
        }
        .passcodeGrid {
            display: grid;
            grid-template-rows: repeat(8, 37.3mm);
            grid-template-columns: repeat(3, 68mm);
        }
        .right {
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            width: 38mm;
            font-size:12px;
        }
        .right img{
            padding-top:3mm;
            width:100%;
        }
        .right p{
            margin-left:2mm;
        }
        .left {
            position: absolute;
            left: 0;
            width: 25mm;
            top: 0;
            bottom: 0;

        }
        .left img{
            padding:3mm;
            display: block;
            max-width: 100%;
            max-height: 100%;
            width: 100%;
            height: auto;
        }
        .font-md{
            font-size:10px;
        }
        .font-xs{
            font-size:9px;
        }
    </style>
</head>
<body>
<?php $key=0;?>
<div class="passcodeGrid">
 @foreach($ops as $op)
 @for($adet=1;$adet<=$op->product_qty;$adet++)
  <div class="passcodeCell">
    <div class="left">
        <img src="{{$op->product_image}}" style="max-width:30mm;max-height:30mm;">
    </div>
    <div class="right">
        <img src="{!!\PicoPrime\BarcodeGen\BarcodeGen::generate([$op->irsaliye_no."-".$key, 40, 'horizontal', 'code128', 1])->encode('data-url')!!}" alt="barcode">
        <p>
            #{!!$op->irsaliye_no."-".$adet!!}<br>
            {{$op->product_width}}x{{$op->product_height}} <br>
            <span class="font-md">{{$op->product_name}} {{$op->mevcut_urun}}</span><br>
            @if($op->product_options)
            <span class="font-xs">{{$op->product_options}}</span><br>
            @endif
            <span class="font-md">{{$op->irsaliyesi->fatura_unvan}}</span>
        </p>
    </div>
  </div>
  @if($key%24==23 AND $key>0)
  </div><div class="passcodeGrid page-break">
  @endif
<?php $key++;?>
@endfor
@endforeach
</div>
</body>
</html>
