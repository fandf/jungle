<?php
    $toplam = 0;
    $teslimat_toplam=0;
    $toplam1 = 0;
?>
<table border=1>
<caption>{{$sube}}</caption>
@foreach ($orders as $key=>$o)
<?php
            $ara_toplam = 0;
            $teslimat = 0;
?>
    <tr>
    <td>{{($key+1)}}</td>
    <td>{{date("d.m.Y",strtotime($o->siptar))}}</td>
   {{--  <td>{{$o->orderno}} ({{$o->ordernosub}}) ({{$o->sellerid}})</td> --}}
    <td>{{$o->order_number}}</td>
    <td>{{config("ayar.payments")[$o->payment]}}</td>
    <td>
    @foreach($o->orderproducts as $op)

        @if(!($sube_id))


            @if($op->ek == 0)
            <?php
                $yuzde = $o->coupon_discount>0 ? $o->discount_yuzde : 0;
                $ara_toplam += ((100-$yuzde)/100)*$op->product_price*$op->product_qty*(100+$op->product_tax)/100;
            ?>
                {{$op->product_name}}
                ({{number_format(((100-$yuzde)/100)*$op->product_price*$op->product_qty*(100+$op->product_tax)/100,2,'.','')}}) <br>
            @else
                <?php $ara_toplam += $op->product_price*$op->product_qty*(100+$op->product_tax)/100;?>
                {{$op->product_name}}
                ({{number_format($op->product_price*$op->product_qty*(100+$op->product_tax)/100,2,'.','')}})
                <br>
            @endif
            <?php
            $ara_toplam += $op->shipping;
            $teslimat += $op->shipping;
            ?>

        @else
            PROBLEM
        @endif
    @endforeach
        @if($teslimat)
        Teslimat Ücreti ({{number_format($teslimat,2,'.','')}})<br>
         @endif
    </td>
    <td align='right'>{{number_format($o->shipping,2,'.','')}}</td>
    <td align='right'>{{number_format($o->total,2,'.','')}}</td>
    <?php
            $toplam += $ara_toplam-$teslimat;
            $teslimat_toplam+=$teslimat;
            $toplam1 += $o->total;
    ?>
    <td>{{(($o->total-$ara_toplam)>1?($o->total-$ara_toplam):"")}}</td>
    </tr>
@endforeach
<tr>
<th colspan='4'>TOPLAM</th>
<td align='right'>{{$toplam}}</td>
<td align='right'>{{number_format($teslimat_toplam,2,'.','')}}</td>
<td align='right'>{{number_format($toplam1,2,'.','')}}</td>
<td align='right'>{{($toplam1-$toplam)}}</td>
</table>
