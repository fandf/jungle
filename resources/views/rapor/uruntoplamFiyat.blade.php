<table border="1">
<caption>
	Ürünler<br/>
	<?=date("d.m.Y",strtotime($tarih1));?> / 
	<?=date("d.m.Y",strtotime($tarih2));?>
</caption>	
	<tr>
		<th>
			KOD
		</th>
		<th>
			ÜRÜN
		</th>
		<th>
			ÜRÜN FİYATI
		</th>
		<th>
			SATIŞ FİYATI
		</th>
		<th>
			SATIŞ ADEDİ
		</th>
	</tr>
	@foreach($tablo as $sku=>$m)
	@foreach($m as $fiyat=>$m)
	<tr>
		<td><?=$sku;?></td>
		<td><?=$names[$sku];?></td>
		<td><?=$prices[$sku];?></td>
		<td><?=$fiyat;?></td>
		<td align="right"><?=1*$m;?></td>
	</tr>
	@endforeach
	@endforeach
</table>