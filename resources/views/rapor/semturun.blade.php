<table border=1>
	<thead>
		<tr>
			<th>2016</th>
			<th></th>
			@foreach($aylar as $i=>$ay)
				@if($i>0)
				<th colspan=2>{{$ay}}</th>
				@endif
			@endforeach
		</tr>
		<tr>
			<th>İL</th>
			<th>SEMT</th>
			@foreach($aylar as $i=>$ay)
				@if($i>0)
				<th>ADET</th><th>TUTAR</th>
				@endif
			@endforeach
	</thead>
	<tbody>
	@foreach($tablo as $il_adi=>$t1) 
	
		@foreach($t1 as $semt_adi=>$t2)
		<tr>
			<th>{{$il_adi}} </th>
			<th> {{$semt_adi}}</th>	
			
			@foreach($t2 as $yil=>$t3)
			@if($yil==2017)
					
					@foreach($aylar as $i => $ay)
						@if($i>0)
							<td align="right">{{isset($t3[$i]["adet"]) ? $t3[$i]["adet"]: ""}}</td>
							<td align="right">{{isset($t3[$i]["toplam"]) ? $t3[$i]["toplam"]: ""}}</td>
						@endif
					@endforeach
				
			@endif
			@endforeach

		</tr>
		@endforeach

	@endforeach
	</tbody>
</table>