@foreach($tablo as $ay=>$t)
<br/>
<table>
	<caption>{{$aylar[$ay]}}</caption>
	<thead>
		<tr>
			<th>ŞUBE</th>
			<th>ADET</th>
			<th>İNDİRİM</th>
			<th>KDV</th>
			<th>KDV HARİÇ TOPLAM</th>
		</tr>
	</thead>
	@foreach($t as $sube=>$t1)
	<tr>
		<th>{{isset($subeler[$sube]) ? $subeler[$sube] : $sube}} </th>
	
		<td>{{isset($t1["adet"]) ? $t1["adet"] : ""}}</td>
		<td>{{isset($t1["discount"]) ? $t1["discount"] : ""}}</td>
		<td>{{isset($t1["kdv"]) ? $t1["kdv"] : ""}}</td>
		<td>{{isset($t1["total"]) ? $t1["total"] : ""}}</td>
	</tr>

		

	@endforeach

</table>

@endforeach