<table border="1">
<?php $toplam = 0;$teslimat = 0; $indirim = 0; $kdv = 0;?>
@foreach($orders as $order)
	
	<tr><td colspan="8">&nbsp;</td></tr>
	<tr>
		<th colspan="8">{!!$order->order_number!!}</th>	
	</tr>
	<tr>
		<th colspan="5">Ürün</th>
		<th>Miktar</th>	
		<th>Fiyatı</th>	
		<th>Toplam</th>	
	</tr>

	@foreach($order->orderproducts as $op)

	<tr>
		<th colspan="4">
			{!!$op->product_name!!}
		</th>
		<td align="right">
			{!!$op->product_qty!!}
		</td>
		<td align="right">
			{!!$op->product_price!!}
		</td>
		<td align="right">
			{!!$op->product_price - $op->product_price !!}
		</td>
		<td align="right">
			{!!$op->product_price * $op->product_qty !!}
		</td>
	</tr>
	@endforeach
	
	<?php $toplam+=$order->total; $teslimat += $order->shipping; $indirim += $order->coupon_discount;$kdv += $order->tax;?>
	<tr>
		<th>TESLİMAT ÜCRETİ</th>
		<td align="right"><?=$order->shipping?></td>
		<th>İNDİRİM</th>
		<td align="right"><?=$order->coupon_discount?></td>
		<th>KDV</th>
		<td align="right"><?=$order->tax?></td>
		<th>SİPARİŞ TOPLAMI</th>
		<td align="right"><?=$order->total?></td>
	</tr>

@endforeach
	<tr><td colspan="6">&nbsp;</td></tr>
	<tr><th colspan="6">GENEL TOPLAMLAR</th></tr>
	<tr>
		<th>TESLİMAT TOPLAMI</th>
		<td align="right"><?=$teslimat?></td>
		<th>İNDİRİM TOPLAM</th>
		<td align="right"><?=$indirim?></td>
		<th>KDV TOPLAM</th>
		<td align="right"><?=$kdv?></td>
		<th>TOPLAM</th>
		<td align="right"><?=$toplam?></td>
	</tr>
</table>