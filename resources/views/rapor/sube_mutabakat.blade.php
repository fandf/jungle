<html>
<head><meta charset="UTF-8"></head>

<body>

<table width="600" border="1">

<tbody>
<tr>
	<th colspan="4">{{date("d.m.Y",strtotime($tarih1))}} / {{date("d.m.Y",strtotime($tarih2))}}</th>
</tr>
<tr>
	<th colspan="4">MEYVE BURDA SATIŞLARI</th>
</tr>
<tr>
	<th>&nbsp;</th>
	<th>SİPARİŞ NO</th>
	<th>TOPLAM</th>
	<th>TESLİMAT</th>
</tr>
<?php
$total_Jungle = 0;
$shipping_Jungle = 0;
$i=1;
?>

@foreach($Jungle_satislari as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<td align="right">{{number_format($order->total,2,',','')}}</td>
<td align="right">{{number_format($order->shipping,2,',','')}}</td>
</tr>

<?php
$total_Jungle += $order->total;
$shipping_Jungle += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach

<tr>
	<td>&nbsp;</td>
	<th>TOPLAM</th>
	<th align="right">{{number_format($total_Jungle,2,',','')}}</th>
	<th align="right">{{number_format($shipping_Jungle,2,',','')}}</th>
</tr>

<th colspan="4">ŞUBE SATIŞLARI</th>
<?php
$total_sube = 0;
$shipping_sube = 0;

?>
@foreach($sube_satislari as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<td align="right">{{number_format($order->total,2,',','')}}</td>
<td align="right">{{number_format($order->shipping,2,',','')}}</td>
</tr>

<?php
$total_sube += $order->total;
$shipping_sube += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach
<tr>
	<td>&nbsp;</td>
	<th>TOPLAM</th>
	<th align="right">{{number_format($total_sube,2,',','')}}</th>
	<th align="right">{{number_format($shipping_sube,2,',','')}}</th>
</tr>

<?php
$total_baska = 0;
$shipping_baska = 0;

?>
<th colspan="4">BAŞKA ŞUBEYE</th>
@foreach($baska_subeye as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<th align="right">{{number_format($order->total,2,',','')}}</th>
<th align="right">{{number_format($order->shipping,2,',','')}}</th>
</tr>

<?php
$total_baska += $order->total;
$shipping_baska += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach
<tr>
	<td>&nbsp;</td>
	<th>TOPLAM</th>
	<th align="right">{{number_format($total_baska,2,',','')}}</th>
	<th align="right">{{number_format($shipping_baska,2,',','')}}</th>
</tr>

</tbody>
</table>

<table border="1" width="600">
<tbody>
	<tr>
		<th colspan="2">TOPLAM SATIŞ</th>
		<th align="right">{{number_format($total_Jungle+$total_sube+$total_baska,2,',','')}}</th>
	</tr>
	<tr>
		<th colspan="2">MEYVE BURDA KOMİSYON %{{$komisyon}}</th>
		<th align="right">{{number_format(($total_Jungle+$total_sube+$total_baska) * $komisyon/100,2,',','')}}</th>
	</tr>
	<tr>
		<th colspan="2">ŞUBEDEKİ NAKİT</th>
		<th align="right">{{number_format($total_sube + $total_baska,2,',','')}}</th>
	</tr>
	<tr>
		<th colspan="2">YOL ÜCRETİ</th>
		<th align="right">{{number_format($shipping_Jungle +$shipping_sube + $shipping_baska,2,',','')}}</th>
	</tr>
	<tr>
		<th colspan="2">MEYVE BURDA ÖDEYECEĞİ MİKTAR</th>
		<th align="right">{{number_format(
			($shipping_Jungle +$shipping_sube + $shipping_baska) + ($total_Jungle) - (($total_Jungle+$total_sube+$total_baska) * $komisyon/100),2,',','')
		}}</th>
	</tr>

</tbody>

</table>
</body>
</html>
