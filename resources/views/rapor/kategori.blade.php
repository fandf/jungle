
<table>
	<thead>
		<tr>
			<th>Ürün Kategori</th>
			@foreach($tarihler as $tarih)
			<th>{{date("d.m.Y",strtotime($tarih))}}</th>
			@endforeach
		</tr>

	</thead>
	<tbody>
		@foreach($p as $kategori=>$a)
		<tr>
			<td>{{$kategori}}</td>
			@foreach($tarihler as $tarih)
			<td>{{isset($a[$tarih]) ? $a[$tarih] : ""}}</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>