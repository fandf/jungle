<html>
<head><meta charset="UTF-8"></head>
<body>
<table width="600" border="1">
<tbody>
<tr>
	<th colspan="4">{{date("d.m.Y",strtotime($tarih1))}} / {{date("d.m.Y",strtotime($tarih2))}}</th>
</tr>
<tr>
	<th colspan="4">{{config("app.name")}} SATIŞLARI</th>
</tr>
<tr>
	<th>&nbsp;</th>
	<th>SİPARİŞ NO</th>
	<th>TOPLAM</th>
	<th>TESLİMAT</th>
</tr>
<?php
$total_Jungle = 0;
$shipping_Jungle = 0;
$i=1;
?>

@foreach($Jungle_satislari as $op)
<tr>
<td>{{$i}}</td>
<td>{{$op->irsaliyesi->order_number}}</td>
<td align="right">{{number_format($op->irsaliyesi->total,2,'.','')}}</td>
<td align="right">{{number_format($op->shipping,2,'.','')}}</td>
</tr>

<?php
$total_Jungle += $op->irsaliyesi->total;
$shipping_Jungle += $op->shipping>10 ? $op->shipping : 0;
$i++

?>

@endforeach

<tr>
	<td>&nbsp;</td>
	<th>TOPLAM</th>
	<th align="right">{{number_format($total_Jungle,2,'.','')}}</th>
	<th align="right">{{number_format($shipping_Jungle,2,'.','')}}</th>
</tr>

<th colspan="4">ŞUBE SATIŞLARI</th>
<?php
$total_sube = 0;
$shipping_sube = 0;

?>
@foreach($sube_satislari as $op)
<tr>
<td>{{$i}}</td>
<td>{{$op->irsaliyesi->order_number}}</td>
<td align="right">{{number_format($op->irsaliyesi->total,2,'.','')}}</td>
<td align="right">{{number_format($op->shipping,2,'.','')}}</td>
</tr>

<?php
$total_sube += $op->irsaliyesi->total;
$shipping_sube += $op->shipping>10 ? $op->shipping : 0;
$i++

?>

@endforeach
<tr>
	<td>&nbsp;</td>
	<th>TOPLAM</th>
	<th align="right">{{number_format($total_sube,2,'.','')}}</th>
	<th align="right">{{number_format($shipping_sube,2,'.','')}}</th>
</tr>



</tbody>
</table>

<table border="1" width="600">
<tbody>
	<tr>
		<th colspan="2">TOPLAM SATIŞ</th>
		<th align="right">{{number_format($total_Jungle+$total_sube,2,'.','')}}</th>
	</tr>
	<tr>
		<th colspan="2">MEYVE BURDA KOMİSYON %{{request("komisyon",25)}}</th>
		<th align="right">{{number_format(($total_Jungle+$total_sube) * request("komisyon",25)/100,2,'.','')}}</th>
	</tr>
	<tr>
		<th colspan="2">ŞUBEDEKİ NAKİT</th>
		<th align="right">{{number_format($total_sube,2,'.','')}}</th>
	</tr>
	<tr>
		<th colspan="2">YOL ÜCRETİ</th>
		<th align="right">{{number_format($shipping_Jungle +$shipping_sube,2,'.','')}}</th>
	</tr>
	<tr>
		<th colspan="2">MEYVE BURDA ÖDEYECEĞİ MİKTAR</th>
		<th align="right">{{number_format(
			($shipping_Jungle +$shipping_sube) + ($total_Jungle) - (($total_Jungle+$total_sube) * request("komisyon",25)/100),2,'.','')
		}}</th>
	</tr>

</tbody>

</table>
</body>
</html>
