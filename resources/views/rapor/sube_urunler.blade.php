<html>
<head><meta charset="UTF-8"></head>

<body>

<table width="600" border="1">
<tr>
	<th colspan="4">{{date("d.m.Y",strtotime($tarih1))}} / {{date("d.m.Y",strtotime($tarih2))}}</th>
</tr>
<tr>
	<th colspan="4">ÜRÜN ADETLERİ</th>
</tr>
<?php $urun_toplam = 0;?>
@foreach($adetler as $sku=>$adet)
<tr>
	<td>{{$sku}}</td>
	<td>{{$names[$sku]}}</td>
	<td>{{$adet}}</td>
	<td>{{number_format($prices[$sku]/$adet,2,',','')}}</td>
</tr>
@endforeach
</table>

</body>
</html>