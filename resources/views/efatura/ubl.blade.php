<?xml version="1.0" encoding="UTF-8"?>
<Invoice xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:cctc="urn:un:unece:uncefact:documentation:2" xmlns:ubltr="urn:oasis:names:specification:ubl:schema:xsd:TurkishCustomizationExtensionComponents" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 UBL-Invoice-2.1.xsd" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2">
    <ext:UBLExtensions><ext:UBLExtension><ext:ExtensionContent></ext:ExtensionContent></ext:UBLExtension></ext:UBLExtensions>
    <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
    <cbc:CustomizationID>TR1.2</cbc:CustomizationID>
    <cbc:ProfileID>TICARIFATURA</cbc:ProfileID>
    <cbc:ID>GIB{{date("Y")}}{{str_pad($irsaliye->fatura_no, 9, "0", STR_PAD_LEFT)}}</cbc:ID>
    <cbc:CopyIndicator>false</cbc:CopyIndicator>
    <cbc:UUID>{{$irsaliye->uuid}}</cbc:UUID>
    <cbc:IssueDate>{{date("Y-m-d",strtotime($irsaliye->siptar))}}</cbc:IssueDate>
    <cbc:IssueTime>{{date("H:i:s")}}</cbc:IssueTime>
    <cbc:InvoiceTypeCode>SATIS</cbc:InvoiceTypeCode>
    <cbc:Note>Yalnız #{{money_tr_string($irsaliye->total+$irsaliye->shipping)}}#</cbc:Note>
    <cbc:DocumentCurrencyCode>TRY</cbc:DocumentCurrencyCode>
    <cbc:LineCountNumeric>{{$irsaliye->orderproducts()->count()+($irsaliye->shipping>0?1:0)}}</cbc:LineCountNumeric>
    <cac:DespatchDocumentReference>
        <cbc:ID>{{$irsaliye->order_number}}</cbc:ID>
        <cbc:IssueDate>{{date("Y-m-d",strtotime($irsaliye->siptar))}}</cbc:IssueDate>
    </cac:DespatchDocumentReference>
    <cac:Signature>
        <cbc:ID schemeID="VKN_TCKN">{{config("site.vergi_no")}}</cbc:ID>
        <cac:SignatoryParty>
            <cac:PartyIdentification>
                <cbc:ID schemeID="VKN">{{config("site.vergi_no")}}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PostalAddress>
                <cbc:StreetName>Emniyetevler mah. Saka Sokak</cbc:StreetName>
                <cbc:BuildingNumber>3/A</cbc:BuildingNumber>
                <cbc:CitySubdivisionName>Kağıthane</cbc:CitySubdivisionName>
                <cbc:CityName>İstanbul</cbc:CityName>
                <cbc:PostalZone>{{config("site.posta_kodu")}}</cbc:PostalZone>
                <cac:Country>
                    <cbc:Name>Türkiye</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
        </cac:SignatoryParty>
        <cac:DigitalSignatureAttachment>
            <cac:ExternalReference>
                <cbc:URI>#Signature</cbc:URI>
            </cac:ExternalReference>
        </cac:DigitalSignatureAttachment>
    </cac:Signature>
    <cac:AccountingSupplierParty>
        <cac:Party>
            <cbc:WebsiteURI>https://www.junglehali.com.tr</cbc:WebsiteURI>
            <cac:PartyIdentification>
                <cbc:ID schemeID="VKN">{{config("site.vergi_no")}}</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name>{{config("site.firma")}}</cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
                <cbc:ID>1234567890</cbc:ID>
                <cbc:StreetName>Esatpaşa mah. Karlıdere cad</cbc:StreetName>
                <cbc:BuildingNumber>49/B</cbc:BuildingNumber>
                <cbc:CitySubdivisionName>Ataşehir</cbc:CitySubdivisionName>
                <cbc:CityName>İstanbul</cbc:CityName>
                <cbc:PostalZone>34415</cbc:PostalZone>
                <cac:Country>
                    <cbc:Name>Türkiye</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
            <cac:PartyTaxScheme>
                <cac:TaxScheme>
                    <cbc:Name>{{config("site.vergi_dairesi")}}</cbc:Name>
                </cac:TaxScheme>
            </cac:PartyTaxScheme>
            <cac:Contact>
                <cbc:Telephone>{{config("site.callcenter")}}</cbc:Telephone>
                <cbc:ElectronicMail>{{config("site.email")}}</cbc:ElectronicMail>
            </cac:Contact>
        </cac:Party>
    </cac:AccountingSupplierParty>
    <cac:AccountingCustomerParty>
        <cac:Party>
            <cbc:WebsiteURI></cbc:WebsiteURI>
            <cac:PartyIdentification>
            @if($irsaliye->vergi_no)
                <cbc:ID schemeID="VKN">{{$irsaliye->vergi_no}}</cbc:ID>
            @else
                <cbc:ID schemeID="TCKN">{{$irsaliye->kimlik_no?:"11111111111"}}</cbc:ID>
            @endif
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name>{{$irsaliye->fatura_unvan ? $irsaliye->fatura_unvan : $irsaliye->gon_adsoyad}}</cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
                <cbc:ID>{{$irsaliye->id}}</cbc:ID>
                <cbc:StreetName>{{$irsaliye->fatura_adres ? $irsaliye->fatura_adres : $irsaliye->gon_adres}}</cbc:StreetName>
                <cbc:BuildingNumber></cbc:BuildingNumber>
                <cbc:CitySubdivisionName></cbc:CitySubdivisionName>
                <cbc:CityName></cbc:CityName>
                <cbc:PostalZone></cbc:PostalZone>
                <cac:Country>
                    <cbc:Name>Türkiye</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
            <cac:PartyTaxScheme>
                <cac:TaxScheme>
                    <cbc:Name>{{$irsaliye->vergi_dairesi?:""}}</cbc:Name>
                </cac:TaxScheme>
            </cac:PartyTaxScheme>
            <cac:Contact>
                <cbc:Telephone>{{$irsaliye->gon_cep}}</cbc:Telephone>
                <cbc:ElectronicMail>{{$irsaliye->gon_email}}</cbc:ElectronicMail>
            </cac:Contact>
        </cac:Party>
    </cac:AccountingCustomerParty>
    @if($op = $irsaliye->orderproducts()->first())
    <cac:BuyerCustomerParty>
        <cac:Party>
            <cbc:WebsiteURI></cbc:WebsiteURI>
            <cac:PartyIdentification>
                <cbc:ID schemeID="TCKN">11111111111</cbc:ID>
            </cac:PartyIdentification>
            <cac:PartyName>
                <cbc:Name>{{$op->alici_adsoyad}}</cbc:Name>
            </cac:PartyName>
            <cac:PostalAddress>
                <cbc:ID>1234567890</cbc:ID>
                <cbc:StreetName>{{$op->alici_adres}}</cbc:StreetName>
                <cbc:BuildingNumber></cbc:BuildingNumber>
                <cbc:CitySubdivisionName>{{$op->semt_adi}}</cbc:CitySubdivisionName>
                <cbc:CityName>{{$op->il_adi}}</cbc:CityName>
                <cbc:PostalZone></cbc:PostalZone>
                <cac:Country>
                    <cbc:Name>Türkiye</cbc:Name>
                </cac:Country>
            </cac:PostalAddress>
            <cac:PartyTaxScheme>
                <cac:TaxScheme>
                    <cbc:Name></cbc:Name>
                </cac:TaxScheme>
            </cac:PartyTaxScheme>
            <cac:Contact>
                <cbc:Telephone>{{$op->alici_telefon}}</cbc:Telephone>
                <cbc:ElectronicMail></cbc:ElectronicMail>
            </cac:Contact>
        </cac:Party>
    </cac:BuyerCustomerParty>
    @endif
    <cac:PaymentMeans>
        <cbc:PaymentMeansCode>1</cbc:PaymentMeansCode>
        <cac:PayeeFinancialAccount>
            <cbc:ID></cbc:ID>
            <cbc:CurrencyCode>TRY</cbc:CurrencyCode>
            <cbc:PaymentNote>{{config("ayar.payments")[$irsaliye->payment]??""}}</cbc:PaymentNote>
        </cac:PayeeFinancialAccount>
    </cac:PaymentMeans>
    <cac:PaymentTerms>
        <cbc:Note>{{$irsaliye->payment_info}}</cbc:Note>
        <cbc:PaymentDueDate>{{date("Y-m-d",strtotime($irsaliye->siptar))}}</cbc:PaymentDueDate>
    </cac:PaymentTerms>
    <?php $matrah8 = 0;$kdv8=0;$matrah18 = 0;$kdv18=0;?>
    <?php
    foreach($irsaliye->orderproducts as $op){
        $discount = $op->ek == 1 ? 1 :  (100-$irsaliye->discount_yuzde)/100;
        $kdv8+= ($op->product_tax==8) ? 0.08 * $op->product_price*$op->product_qty * $discount : 0;
        $kdv18+= ($op->product_tax==18) ? 0.18 * $op->product_price*$op->product_qty * $discount : 0;
        $matrah8 += ($op->product_tax==8) ? $op->product_price*$op->product_qty * $discount : 0;
        $matrah18 += ($op->product_tax==18) ? $op->product_price*$op->product_qty * $discount : 0;
        $matrah18 += $irsaliye->shipping / 1.18;
        $kdv18 += $irsaliye->shipping > 0 ? ($irsaliye->shipping - $irsaliye->shipping / 1.18) : 0;
    }
    ?>
    <cac:TaxTotal>
        <cbc:TaxAmount currencyID="TRY">{{number_format(($kdv8+$kdv18),2,'.','')}}</cbc:TaxAmount>
        @if($kdv8)
        <cac:TaxSubtotal>
            <cbc:TaxableAmount currencyID="TRY">{{number_format($matrah8,2,'.','')}}</cbc:TaxableAmount>
            <cbc:TaxAmount currencyID="TRY">{{number_format($kdv8,2,'.','')}}</cbc:TaxAmount>
            <cbc:CalculationSequenceNumeric>1.0</cbc:CalculationSequenceNumeric>
            <cbc:Percent>8</cbc:Percent>
            <cac:TaxCategory>
                <cac:TaxScheme>
                    <cbc:Name>Katma Değer Vergisi</cbc:Name>
                    <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
        @endif
        @if($kdv18)
        <cac:TaxSubtotal>
            <cbc:TaxableAmount currencyID="TRY">{{number_format($matrah18,2,'.','')}}</cbc:TaxableAmount>
            <cbc:TaxAmount currencyID="TRY">{{number_format($kdv18,2,'.','')}}</cbc:TaxAmount>
            <cbc:CalculationSequenceNumeric>1.0</cbc:CalculationSequenceNumeric>
            <cbc:Percent>18</cbc:Percent>
            <cac:TaxCategory>
                <cac:TaxScheme>
                    <cbc:Name>Katma Değer Vergisi</cbc:Name>
                    <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
                </cac:TaxScheme>
            </cac:TaxCategory>
        </cac:TaxSubtotal>
        @endif
    </cac:TaxTotal>
    <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount currencyID="TRY">{{number_format(($matrah8+$matrah18+$irsaliye->coupon_discount),2,'.','')}}</cbc:LineExtensionAmount>
        <cbc:TaxExclusiveAmount currencyID="TRY">{{number_format(($matrah8+$matrah18),2,'.','')}}</cbc:TaxExclusiveAmount>
        <cbc:TaxInclusiveAmount currencyID="TRY">{{number_format(($matrah8+$matrah18+$kdv8+$kdv18),2,'.','')}}</cbc:TaxInclusiveAmount>
        @if($irsaliye->coupon_discount>0)
        <cbc:AllowanceTotalAmount currencyID="TRY">{{number_format($irsaliye->coupon_discount,2,'.','')}}</cbc:AllowanceTotalAmount>
        @endif
        <cbc:PayableAmount currencyID="TRY">{{number_format(($matrah8+$matrah18+$kdv8+$kdv18),2,'.','')}}</cbc:PayableAmount>
    </cac:LegalMonetaryTotal>
    @foreach($irsaliye->orderproducts as $key=>$op)
    <cac:InvoiceLine>
        <cbc:ID>{{$key+1}}</cbc:ID>
        <cbc:InvoicedQuantity unitCode="NIU">{{$op->product_qty}}</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="TRY">{{number_format($op->product_qty*$op->product_price,2,'.','')}}
        </cbc:LineExtensionAmount>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="TRY">{{number_format($op->product_price*((100+$op->product_tax)/100)-$op->product_price,2,'.','')}}</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="TRY">{{number_format($op->product_price,2,'.','')}}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="TRY">{{number_format($op->product_price*((100+$op->product_tax)/100)-$op->product_price,2,'.','')}}</cbc:TaxAmount>
                <cbc:Percent>{{$op->product_tax}}</cbc:Percent>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:Name>KDV</cbc:Name>
                        <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        <cac:Item>
            <cbc:Name>{{$op->product_name}}</cbc:Name>
        </cac:Item>
        <cac:Price>
            <cbc:PriceAmount currencyID="TRY">{{number_format($op->product_price,2,'.','')}}</cbc:PriceAmount>
        </cac:Price>
    </cac:InvoiceLine>
    @endforeach
    @if($irsaliye->shipping>0)
    <cac:InvoiceLine>
        <cbc:ID>{{$key+2}}</cbc:ID>
        <cbc:InvoicedQuantity unitCode="NIU">1</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="TRY">{{number_format($irsaliye->shipping/1.18,2,'.','')}}</cbc:LineExtensionAmount>
        <cac:TaxTotal>
            <cbc:TaxAmount currencyID="TRY">{{number_format(($irsaliye->shipping-$irsaliye->shipping/1.18),2,'.','')}}</cbc:TaxAmount>
            <cac:TaxSubtotal>
                <cbc:TaxableAmount currencyID="TRY">{{number_format($irsaliye->shipping/1.18,2,'.','')}}</cbc:TaxableAmount>
                <cbc:TaxAmount currencyID="TRY">{{number_format(($irsaliye->shipping-$irsaliye->shipping/1.18),2,'.','')}}</cbc:TaxAmount>
                <cbc:Percent>18</cbc:Percent>
                <cac:TaxCategory>
                    <cac:TaxScheme>
                        <cbc:Name>KDV</cbc:Name>
                        <cbc:TaxTypeCode>0015</cbc:TaxTypeCode>
                    </cac:TaxScheme>
                </cac:TaxCategory>
            </cac:TaxSubtotal>
        </cac:TaxTotal>
        <cac:Item>
            <cbc:Name>Teslimat</cbc:Name>
        </cac:Item>
        <cac:Price>
            <cbc:PriceAmount currencyID="TRY">{{number_format($irsaliye->shipping/1.18,2,'.','')}}</cbc:PriceAmount>
        </cac:Price>
    </cac:InvoiceLine>
    @endif
</Invoice>
