<div style="overflow-y: auto;">
	@if($mesaj)
	<div class="alert alert-warning">{!!$mesaj!!}</div>
	@endif
		@if($irsaliye)
		<h4>{!!$irsaliye->order_number!!}</h4>
		<table class="table table-bordered table-striped">
			<tr>
				<th>İşlem</th>
				<th>İşlem Yapan</th>
				<th>İşlem Zamanı</th>
			</tr>
			@foreach($irsaliye->irsaliyetras as $irt)
			<tr>
				<td>{!!$irt->description!!}</td>
				<td>{!!$irt->user ? $irt->user->name:""!!}</td>
				<td>{!!date("d.m.Y H:i",strtotime($irt->created_at))!!}</td>
			</tr>
			@endforeach
		</table>
		@endif
</div>