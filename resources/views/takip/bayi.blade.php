<div style="overflow-y: auto;">
    @if($mesaj)
    <div class="alert alert-warning">{!!$mesaj!!}</div>
    @endif
    @if($op)
    <h4>{!!$op->irsaliye_no!!}</h4>
    <table class="table table-bordered table-striped">
        <tr>
            <th>İşlem</th>
            <th>İşlem Yapan</th>
            <th>İşlem Zamanı</th>
        </tr>
        @foreach($op->irsaliyetras as $irt)
        <tr>
            <td>
                @switch($irt->link)
                    @case('setEklendi')
                        Sipariş Eklendi
                        @break
                    @case('setGüncellendi')
                        Güncelleme Yapıldı
                        @break
                    @case('setOnaylandi')
                        Üretime Alındı
                        @break
                    @case('setDagitimda')
                        Hazırlandı
                        @break
                    @case('setTeslimEdildi')
                        Teslim Edildi
                        @break
                    @default
                        Üretim Aşaması
                        @break
                @endswitch
            </td>
            <td>{!!$irt->user ? $irt->user->name:""!!}</td>
            <td>{!!date("d.m.Y H:i",strtotime($irt->created_at))!!}</td>
        </tr>
        @endforeach
    </table>
    @endif
</div>
