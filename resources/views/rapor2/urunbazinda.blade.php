<html>
<head><meta charset="UTF-8"></head>

<body>
@foreach($tablo as $link=>$t2)
<table>
<caption>{{strtoupper($link)}}</caption>
<tbody>
<tr>
	@foreach($t2 as $sube=>$t1)
<td valign="top">
	<table>
		<thead>

		<tr>
			<th>{{isset($subeler[$sube]) ? $subeler[$sube] : $sube}} </th>
			<td>{{number_format($t1["total"],2,',','')}}</td>
			<td>{{number_format($t1["toplam"],2,',','')}}</td>
		</tr>
		</thead>
		<tbody>
			@foreach($t1["product"] as $urun=>$t)
		<tr>
			<th>{{$urun}}</th>
			<td>{{$t["qty"]}}</td>
			<td>{{number_format($t["price"]/$t["qty"],2,',','')}}</td>
		</tr>
			@endforeach
		</tbody>
	</table>
</td>
<td>&nbsp;</td>
	@endforeach
</tr>
</table>

@endforeach
</body>
</html>