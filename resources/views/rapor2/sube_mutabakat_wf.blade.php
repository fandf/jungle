<html>
<head><meta charset="UTF-8"></head>

<body>

<table width="600" border="1">

<tbody>
<tr>
	<th colspan="4">{{date("d.m.Y",strtotime($tarih1))}} / {{date("d.m.Y",strtotime($tarih2))}}</th>
</tr>
<tr>
	<th colspan="4">{{config("app.name")}} SATIŞLARI</th>
</tr>
<tr>
	<th></th>
	<th>SİPARİŞ NO</th>
	<th>TOPLAM</th>
	<th>TESLİMAT</th>
</tr>
<?php
$total_Jungle = 0;
$shipping_Jungle = 0;
$i=1;
?>

@foreach($Jungle_satislari as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<td align="right">{{$order->total}}</td>
<td align="right">{{$order->shipping}}</td>
</tr>

<?php
$total_Jungle += $order->total;
$shipping_Jungle += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach

<tr>
	<td></td>
	<th>TOPLAM</th>
	<th align="right">{{$total_Jungle}}</th>
	<th align="right">{{$shipping_Jungle}}</th>
</tr>

<th colspan="4">ŞUBE SATIŞLARI</th>
<?php
$total_sube = 0;
$shipping_sube = 0;

?>
@foreach($sube_satislari as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<td align="right">{{$order->total}}</td>
<td align="right">{{$order->shipping}}</td>
</tr>

<?php
$total_sube += $order->total;
$shipping_sube += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach
<tr>
	<td></td>
	<th>TOPLAM</th>
	<th align="right">{{$total_sube}}</th>
	<th align="right">{{$shipping_sube}}</th>
</tr>

<?php
$total_baska = 0;
$shipping_baska = 0;

?>
<th colspan="4">BAŞKA ŞUBEYE</th>
@foreach($baska_subeye as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<th align="right">{{$order->total}}</th>
<th align="right">{{$order->shipping}}</th>
</tr>

<?php
$total_baska += $order->total;
$shipping_baska += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach
<tr>
	<td></td>
	<th>TOPLAM</th>
	<th align="right">{{$total_baska}}</th>
	<th align="right">{{$shipping_baska}}</th>
</tr>

<?php
$total_fruitanya = 0;
$shipping_fruitanya = 0;

?>
<th colspan="4">FRUITANYA SATIŞLARI</th>
@foreach($Jungle_fruitanya as $order)
<tr>
<td>{{$i}}</td>
<td>{{$order->order_number}}</td>
<th align="right">{{$order->total}}</th>
<th align="right">{{$order->shipping}}</th>
</tr>

<?php
$total_fruitanya += $order->total;
$shipping_fruitanya += $order->shipping>9 ? $order->shipping : 0;
$i++

?>

@endforeach
<tr>
	<td></td>
	<th>TOPLAM</th>
	<th align="right">{{$total_fruitanya}}</th>
	<th align="right">{{$shipping_fruitanya}}</th>
</tr>
</tbody>
</table>

<table border="1" width="600">
<tbody>
	<tr>
		<th>TOPLAM SATIŞ</th>
		<th align="right">{{$total_Jungle+$total_sube+$total_baska}}</th>
	</tr>
	<tr>
		<th>Jungle KOMİSYON %{{$komisyon}}</th>
		<th align="right">{{($total_Jungle+$total_sube+$total_baska) * $komisyon/100}}</th>
	</tr>
	<tr>
		<th>FRUITANYA KOMİSYON %20</th>
		<th align="right">{{($total_fruitanya) * 20/100}}</th>
	</tr>
	<tr>
		<th>ŞUBEDEKİ NAKİT</th>
		<th align="right">{{$total_sube + $total_baska}}</th>
	</tr>
	<tr>
		<th>YOL ÜCRETİ</th>
		<th align="right">{{$shipping_Jungle +$shipping_sube + $shipping_baska + $shipping_fruitanya}}</th>
	</tr>
	<tr>
		<th>Jungle ÖDEYECEĞİ MİKTAR</th>
		<th align="right">{{
			($shipping_Jungle +$shipping_sube + $shipping_baska) + ($total_Jungle) - (($total_Jungle+$total_sube+$total_baska) * $komisyon/100) - ($total_fruitanya * 20/100)
		}}</th>
	</tr>

</tbody>

</table>
</body>
</html>
