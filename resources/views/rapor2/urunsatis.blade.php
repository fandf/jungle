<table border="1">
<?php $toplam = 0;$teslimat = 0;
 $indirim = 0; $kdv = 0;?>
	<tr>
		<th>Ürün</th>
		<th>Miktar</th>	
		<th>Fiyatı</th>	
		<th>Toplam</th>	
	</tr>
	@foreach($orders as $op)
	
	<tr>
		<th>
			{!!$op->title!!}
		</th>
		<td align="right">
			{!!$op->quantity!!}
		</td>
		<td align="right">
			{!!$op->disc_price>0 ? $op->disc_price : $op->price!!}
		</td>
		<td align="right">
			{!!($op->disc_price>0 ? $op->disc_price : $op->price) * $op->quantity !!}
		</td>
	</tr>
	
	<?php 
	$toplam+= ($op->disc_price>0 ? $op->disc_price : $op->price) * $op->quantity ;
	$teslimat += $op->delivery_price;
	?>
	
@endforeach
	<tr><td colspan="6">&nbsp;</td></tr>
	<tr><th colspan="6">GENEL TOPLAMLAR</th></tr>
	<tr>
		<th>TESLİMAT TOPLAMI</th>
		<td align="right"><?=$teslimat;?></td>
		
		
		<th>TOPLAM</th>
		<td align="right"><?=$toplam;?></td>
	</tr>
</table>