@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">RAPORLAR</div>

                <div class="panel-body">

                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'target' => '_blank' ]) !!}

                        <div class="form-group">
                            {!! Form::label('rapor', 'Rapor Cinsi', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('rapor',$raporlar, null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('rapor') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('tarih1', 'İlk Tarih', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('tarih1',date("Y-m-d"), ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('tarih1') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('tarih2', 'İkinci Tarih', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('tarih2',date("Y-m-d"), ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('tarih2') }}</small>

                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-4">
                                <input type="text" class="form-control" size="5" name="komisyon" placeholder="Komisyon">

                            </div>
                        </div>

                        <div class="btn-group pull-right">

                            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                            {!! Form::submit("GÖNDER", ['class' => 'btn btn-success']) !!}
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
