@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <kurye-page></kurye-page>
        </div>
    </div>

@endsection

@section("scripts")
<script type="text/javascript">
var api_link = '<?=config("site.api_link");?>';
var pusher_channel = '<?=env("PUSHER_CHANNEL");?>';
var user = {id:<?=$user->id;?>,name:'<?=$user->name;?>',sube_id:<?=$user->sube_id;?>,onay:<?=$user->can("siparis-onay")?1:0;?>,indirim:<?=$user->can("indirim-yapabilir")?1:0;?>,urun:<?=$user->can("urun-ekleyebilir")?1:0;?>,iade:<?=$user->can("iade-yapabilir")?1:0;?>};
var tarih1 = '<?=date("01-m-Y");?>';
var tarih2 = '<?=date("d-m-Y");?>';
</script>
<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
@endsection
