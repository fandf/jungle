
require('./bootstrap');

import VueLocalStorage from 'vue-localstorage'

/* import DisableAutocomplete from 'vue-disable-autocomplete';
Vue.use(DisableAutocomplete); */
Vue.use(VueLocalStorage)
import vSelect from 'vue-select'
window.Cropper = require('cropperjs');
window.Cropper = 'default' in window.Cropper ? window.Cropper['default'] : window.Cropper;
/* Vue.use(require('vue-pusher'), {
    api_key: 'f23b2ccb86c823b707b8',
    options: {
        cluster: 'eu',
        encrypted: true,
    }
}); */
Vue.component('v-select', vSelect)
Vue.component('OnayPage', require('./pages/OnayPage.vue'));
Vue.component('HazirlikPage', require('./pages/HazirlikPage.vue'));
Vue.component('UretimPage', require('./pages/UretimPage.vue'));
Vue.component('UretilenPage', require('./pages/UretilenPage.vue'));
Vue.component('TransferPage', require('./pages/TransferPage.vue'));
Vue.component('TransferPanel', require('./pages/TransferPanel.vue'));
Vue.component('DagitimdaPage', require('./pages/DagitimdaPage.vue'));
Vue.component('CikislarPage', require('./pages/CikislarPage.vue'));
Vue.component('CikislarPanel', require('./pages/CikislarPanel.vue'));
Vue.component('FaturaPage', require('./pages/FaturaPage.vue'));
Vue.component('TumPage', require('./pages/TumPage.vue'));
Vue.component('KuryePage', require('./pages/KuryePage.vue'));
Vue.component('CariPage', require('./pages/CariPage.vue'));
Vue.component('OrderPage', require('./pages/OrderPage.vue'));
Vue.component('BayiGirisPage', require('./pages/BayiGirisPage.vue'));
Vue.component('UrunPage', require('./pages/UrunPage.vue'));
Vue.component('TopPage', require('./pages/TopPage.vue'));
Vue.component('HataliPage', require('./pages/HataliPage.vue'));
Vue.component('AnalizPage', require('./pages/AnalizPage.vue'));

const app = new Vue({
    el: '#app'
});
