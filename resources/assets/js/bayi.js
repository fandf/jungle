require('./bootstrap');

window.Resumable = require('./resumable');

import VueLocalStorage from 'vue-localstorage'
import vSelect from 'vue-select'
/* import VueResumable from 'vue-resumable'
Vue.use(VueResumable) */
Vue.use(VueLocalStorage)
Vue.component('v-select', vSelect)

Vue.component('BayiPage', require('./pages/BayiPage.vue'));
Vue.component('TumPage', require('./pages/BayiTumPage.vue'));
Vue.component('UrunPage', require('./pages/BayiUrunPage.vue'));
Vue.component('BaskiUpload', require('./components/BaskiUpload.vue'));

const app = new Vue({
    el: '#app'
});
