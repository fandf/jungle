<?php
return [
    "kartlar"	=> [
        0	=>	 "Lütfen Seçiniz..",
        14	=>	"Seni Seviyorum",
        1	=>	"Doğum günün kutlu olsun",
        2	=>	"Canım Annem",
        3	=>	"Tebrikler",
        4	=>	"Özel bir hediyeniz var",
        5	=>	"Başarılar",
        6	=>	"Seni seviyorum",
        8	=>	"Hoşgeldin bebek",
        10	=>	"Afiyet olsun",
        11	=>	"Canım Babam",
        12	=>	"Canım Öğretmenim",
        13	=>	"Hoşgeldin yeni yıl",
        14	=>	"Seni Seviyorum",
        15	=>	"Tebrikler",
        16	=>	"Özür Dilerim"
    ],
    "nedenler"	=>	[
        "bireysel"=>"BİREYSEL",
        "kurumsal"=>"KURUMSAL"
    ],
    'labels'	=> [
        0	=>	"label-islem",
        1	=>	"label-islem",
        2	=>	"label-uretildi",
        3	=>	"label-danger",
        4	=>	"label-success",
        5	=>	"label-iade",
        6	=>	"label-iptal",
        7	=>	"label-info",
        8	=>	"label-warning",
        9	=>	"label-warning",
        10	=>	"label-iade"
    ],
    'durumlar'	=> [
        0   =>  'Yeni Sipariş',
        1   =>  'Hazırlıkta',
        2   =>  'Baskıda',
        8   =>  'Basılmış',
        9   =>  'Transferde',
        7   =>  'Barkodlandı',
        3   =>  'Hazırlandı',
        4   =>  'Teslim Edildi',
        5   =>  'Ürün Çıkmadı',
        6   =>  'İptal Edildi',
        10  =>  'Ürün Hatalı Çıktı',
    ],
    'colors'    => [
        0   =>  "#FF5733",
        1   =>  "#FDD017",//levent gül kahve
        2   =>  "#FBBBB9",//ümraniye altın
        3   =>  "#79BAEC",//gök mavi
        4   =>  "#43C6DB",//turkuaz
        5   =>  "#F778A1",//soluk menekşe
        6   =>  "#5CB3FF",//metalik mavi
        7   =>  "#57E964" //bahar yeşili
    ],
    'payments'  => [
        'kredi'  =>  'Kredi Kartı',
        'havale'    =>  'Havale',
        'kapida'    =>  'Kapıda',
        'subede'    => 'Şubede',
        'cari'  =>  "Cari"
        //'ipara' =>  'İpara',
        //'iyzico' =>  'Iyzico',
        //'hepsipay'  =>  'HepsiPay',
        //'bonusapp'  =>  "BonusApp ile Ödeme",
        //'bankakart' =>  "Banka Kartı ile Ödeme",
        //'turkodeme' =>  "Türködeme EasyCard ile Ödeme",
        //'multinet'  =>  "Multinet İle Ödeme",
        //'compay'    =>  "Compay ile Ödeme",
    ],
    'brands' => [
        'jungle' => "jungle",
        'jungle show' => "Jungle Show",
        'otantik' => "Otantik",
        'new otantik' => "New Otantik",
        'art' => "Art",
        'else' => "Else",
        'flash' => "Flash",
        'show erkek' => "Show Erkek",
        'show kız' => "Show Kız",
        'welsoft' => "Welsoft",
    ],
    'kanallar' => [
        "whatsapp"=>"Whatsapp",
        "cari"=>"Cari",
        "email"=>"Eposta"
    ],
];
