<?php 
return [
	"hafta"	=> [
		1=>"Pazartesi",
		2=>"Salı",
		3=>"Çarşamba",
		4=>"Perşembe",
		5=>"Cuma",
		6=>"Cumartesi",
		7=>"Pazar"
	],
	"saatler"	=> [
		'9'=>'09:00-14:00',
		'12'=>'12:00-18:00',
		'17'=>'17:00-21:00'
	],
	"aylar"	=>	[
		'1' => 'Ocak',
		'2' => 'Şubat',
		'3' => 'Mart',
		'4' => 'Nisan',
		'5' => 'Mayıs',
		'6' => 'Haziran',
		'7' => 'Temmuz',
		'8' => 'Ağustos',
		'9' => 'Eylül',
		'10' => 'Ekim',
		'11' => 'Kasım',
		'12' => 'Aralık'
	]
];
?>