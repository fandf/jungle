<?php

return [

    "callcenter" => env("CALLCENTER","0224"),
    "whatsapp" => env("WHATSAPP","0555"),
    "company"   => env("COMPANY","Jungle"),
    "email" => env("SITE_EMAIL","info@junglehali.com.tr"),
    "name" => "Jungle Halı",
    "firma" => "Jungle Halı",
    "adres" => "Beşevler Nilüfer/İstanbul",
    "posta_kodu" => "16550",
    "vergi_no" => "",
    "vergi_dairesi" => "",
    "api_link" => env("API_LINK")
];
