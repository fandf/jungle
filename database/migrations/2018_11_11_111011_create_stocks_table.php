<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("STOKNO");
            $table->string("MALINCINSI");
            $table->string("STOKKODU");
            $table->integer("ANABIRIM");
            $table->integer("BIRIMEX");
            $table->integer("ALTSEVIYE");
            $table->integer("KRITIKSEVIYE");
            $table->integer("USTSEVIYE");
            $table->boolean("DEPOSEVIYESI");
            $table->string("URETICI");
            $table->decimal("AYLIKVADE",8,2);
            $table->integer("SERINO");
            $table->integer("DEPO");
            $table->integer("STOKGRUBU");
            $table->integer("GARANTI");
            $table->decimal("PRIM",8,2);
            $table->boolean("IPTAL");
            $table->integer("STOKTIPI");
            $table->integer("STOKTAKIP");
            $table->integer("TEMINYERI");
            $table->integer("RAFOMRU");
            $table->string("RESIM");
            $table->decimal("KALAN",8,2);
            $table->decimal("REZERV",8,2);
            $table->integer("SATISKOSULU");
            $table->string("KOD1");
            $table->string("KOD2");
            $table->string("KOD3");
            $table->string("KOD4");
            $table->string("KOD5");
            $table->string("KOD6");
            $table->string("KOD7");
            $table->string("KOD8");
            $table->string("KOD9");
            $table->string("KOD10");
            $table->integer("TAKSITSAYISI");
            $table->text("ISTIHBARAT");
            $table->boolean("FIYATYOK");
            $table->boolean("DELETED");
            $table->decimal("ALISFIYATI",8,2);
            $table->datetime("ALISFIYATIDEGISMETARIHI");
            $table->decimal("ESKIALISFIYATI",8,2);
            $table->datetime("SONALISTARIHI");
            $table->datetime("SONSATISTARIHI");
            $table->datetime("KARTINACILMATARIHI");
            $table->integer("DEVIRIND");
            $table->decimal("MALIYET",8,2);
            $table->integer("KDVGRUBU");
            $table->boolean("AKTIF");
            $table->integer("ISCILIKIND");
            $table->integer("ISCILIKBIRIMIND");
            $table->string("ISCILIKACIKLAMA");
            $table->integer("STATUS");
            $table->decimal("DALISFIYATI",8,2);
            $table->string("APB");
            $table->decimal("OIV",8,2);
            $table->decimal("KARORANI",8,2);
            $table->decimal("OTV",8,2);
            $table->decimal("ISK",8,2);
            $table->text("STOKGRUPTANIMI");
            $table->decimal("ISKSATISFIYATI2",8,2);
            $table->decimal("ISKSATISFIYATI3",8,2);
            $table->integer("ALISKDVORANI");
            $table->decimal("ALISISKORANI",8,2);
            $table->boolean("SIPARISALINMASIN");
            $table->boolean("SIPARISVERILMESIN");
            $table->string("P1");
            $table->string("P2");
            $table->string("P3");
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
